package com.iflytek.cyber.iot.show.core.utils;

import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class FileUtils {


    public static boolean writeResponseBodyToDisk(String str) {

        try {
            InputStream in_withcode = new ByteArrayInputStream(str.getBytes("UTF-8"));

            // todo change the file location/name according to your needs
//            File filedrir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/android/");
//            if (!filedrir.exists()){
//                filedrir.mkdir();
//            }

            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/android/directive.txt");
            if (!file.exists()) {
                file.createNewFile();
            }
//            if (file.exists()) {
//                file.delete();
//            }
            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSizeDownloaded = 0;

                inputStream = in_withcode;
                outputStream = new FileOutputStream(file);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    public static void saveToSDCard(String content) {
//        Log.i("IflyosClient_1", content);
//        String fileName = "/android/log.txt";
//        FileOutputStream fos = null;
//
//        try {
//            //Environment.getExternalStorageDirectory()获取sd卡的路径
//            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + fileName);
//            if (!file.exists()) {
//                file.createNewFile();
//            }
//
//            fos = new FileOutputStream(file);
//
//            fos.write(content.getBytes());
//            fos.write("--------------\n".getBytes());
////            fos.flush();
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (fos != null) {
//                    fos.close();
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
    }


}
