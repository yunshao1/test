package com.iflytek.cyber.iot.show.core.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.response.RepairResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.ServiceBean;
import com.iflytek.cyber.iot.show.core.utils.GlideUtil;

import java.util.List;

public class HomeRepairAdapter extends RecyclerView.Adapter<HomeRepairAdapter.VHodler> {

    private Context context;
    private List<RepairResponse.ListBean> list;


    public HomeRepairAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<RepairResponse.ListBean> list) {
        this.list = list;
        notifyDataSetChanged();

    }


    @NonNull
    @Override
    public HomeRepairAdapter.VHodler onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        HomeRepairAdapter.VHodler vHodler = null;
        vHodler = new HomeRepairAdapter.VHodler(LayoutInflater.from(context).inflate(R.layout.item_fragment_home_reapar, viewGroup, false));
        return vHodler;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeRepairAdapter.VHodler vHodler, final int position) {
        vHodler.tv_title.setText(list.get(position).getName());
        vHodler.tv_subtitle.setText(list.get(position).getRemark());
        vHodler.ll_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(list.get(position).getName()
                            + "-" + list.get(position).getRemark(), list.get(position).getId()+"");
                }
            }
        });
        GlideUtil.loadImage(context, list.get(position).getImg(), 0, vHodler.iv_cover);
        if (position==0) {
            vHodler.ll_bg.setBackgroundResource(R.drawable.shape_grad_8px_color_5dbaf6);
        }
        if (position==1) {
            vHodler.ll_bg.setBackgroundResource(R.drawable.shape_grad_8px_color_5fb8b6e);
        }
        if (position==2) {
            vHodler.ll_bg.setBackgroundResource(R.drawable.shape_grad_8px_color_acc964);
        }
        if (position==3) {
            vHodler.ll_bg.setBackgroundResource(R.drawable.shape_grad_8px_color_f3d965);
        }
        if (position==4) {
            vHodler.ll_bg.setBackgroundResource(R.drawable.shape_grad_8px_color_4bccf6);
        }
        if (position==5) {
            vHodler.ll_bg.setBackgroundResource(R.drawable.shape_grad_8px_color_5fb8b6e);
        }
        if (position==6) {
            vHodler.ll_bg.setBackgroundResource(R.drawable.shape_grad_8px_color_acc964);
        }
        if (position==7) {
            vHodler.ll_bg.setBackgroundResource(R.drawable.shape_grad_8px_color_57d2bd);
        }
        if (position==8) {
            vHodler.ll_bg.setBackgroundResource(R.drawable.shape_grad_8px_color_dc98f2);
        }
        if (position==9) {
            vHodler.ll_bg.setBackgroundResource(R.drawable.shape_grad_8px_color_57d2bd);
        }

    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    public class VHodler extends RecyclerView.ViewHolder {
        public TextView tv_title;
        public ImageView iv_cover;
        public TextView tv_subtitle;
        public LinearLayout ll_bg;

        public VHodler(@NonNull View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            iv_cover = itemView.findViewById(R.id.image);
            tv_subtitle = itemView.findViewById(R.id.tv_subtitle);
            ll_bg = itemView.findViewById(R.id.ll_bg);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(String title,String id);
    }

    public OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
