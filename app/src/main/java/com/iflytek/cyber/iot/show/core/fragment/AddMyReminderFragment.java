package com.iflytek.cyber.iot.show.core.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.listener.OnTimeSelectListener;
import com.bigkoo.pickerview.view.TimePickerView;
import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.CreatRemindResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.ReminderResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.UploadResponse;
import com.iflytek.cyber.iot.show.core.impl.SpeechRecognizer.SpeechRecognizerHandler;
import com.iflytek.cyber.iot.show.core.utils.Constant;
import com.iflytek.cyber.iot.show.core.utils.DateUtils;
import com.iflytek.cyber.iot.show.core.utils.DialogUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.navigation.fragment.NavHostFragment;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * 添加我的提醒
 */
public class AddMyReminderFragment extends BaseFragment implements View.OnClickListener, View.OnTouchListener {
    LinearLayout ll_finsh;

    private View view;
    private LinearLayout ll_week;
    private LinearLayout ll_time;

    private TextView tv_save, tv_min;
    private ImageView im_recode, im_play, im_delete;
    private TextView tv_chouse_weeks;
    private TextView tv_play,tv_delete;


    private MediaRecorder recorder;
    private File tempFile;
    private File SDPathDir;
    private boolean isSDCardExit; // 判断SDCard是否存在
    private String voiceUrl = "";
    private String min = "";
    private String hour = "";
    private Date date;
    private List<String> weeks = new ArrayList<>();
    private boolean isdestry = false;

    private String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/android/voice.mp3";


//    private String

    private PopupWindow popupWindow;
    private MediaPlayer mediaPlayer = new MediaPlayer();
    private static int REQUEST_PERMISSION_CODE = 1;
    private String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_add_my_reminder, container, false);
            initView();
        }

        return view;
    }

    @Override
    public void onActivityCreated(@org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setData("试试，“蓝小飞，每天早上八点提醒我散步”");
    }


    private void setData(String tryTxt) {
        setHideBottomBar(true);
        setTipText(tryTxt, R.style.TextWhiteFont);
    }


    private void initView() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), PERMISSIONS_STORAGE, REQUEST_PERMISSION_CODE);
            }
        }
        isSDCardExit = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
        if (isSDCardExit) {
            SDPathDir = Environment.getExternalStorageDirectory();
        }
        ll_finsh = view.findViewById(R.id.ll_finsh);
        ll_time = view.findViewById(R.id.ll_time);
        ll_week = view.findViewById(R.id.ll_week);
        tv_save = view.findViewById(R.id.tv_save);
        tv_min = view.findViewById(R.id.tv_min);
        tv_chouse_weeks = view.findViewById(R.id.tv_chouse_weeks);

        im_recode = view.findViewById(R.id.im_recode);
        im_play = view.findViewById(R.id.im_play);
        im_delete = view.findViewById(R.id.im_delete);

        tv_play = view.findViewById(R.id.tv_play);
        tv_delete = view.findViewById(R.id.tv_delete);

        im_play.setOnClickListener(this);
        im_delete.setOnClickListener(this);
        im_recode.setOnTouchListener(this);


        ll_time.setOnClickListener(v -> {
            getTimePick();
        });
        ll_week.setOnClickListener(v -> {
            DialogUtils.showChouseWeekPop(getActivity(), view,
                    week -> {
                        weeks = week;
                        setWeeks(weeks);
                        if ("只看当日".equals(tv_chouse_weeks.getText().toString())) {
                            weeks.clear();
                        }
                    });

        });
        ll_finsh.setOnClickListener(v -> NavHostFragment.findNavController(AddMyReminderFragment.this).navigateUp());
        tv_save.setOnClickListener(v -> {

            savaData();

        });

    }

    private void getTimePick() {
        Calendar selectedDate = Calendar.getInstance();
        Calendar startDate = Calendar.getInstance();
        //startDate.set(2013,1,1);
        Calendar endDate = Calendar.getInstance();
        //endDate.set(2020,1,1);

        //正确设置方式 原因：注意事项有说明
        startDate.set(2018, 0, 1);
        endDate.set(2050, 11, 31);

        TimePickerView pvTime = new TimePickerBuilder(getActivity(), new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date dates, View v) {//选中事件回调
//                tvTime.setText(getTime(date));
                date = dates;
                min = DateUtils.getMinute(dates) + "";
                hour = DateUtils.getHour(dates) + "";
                tv_min.setText(DateUtils.getRemDate(date));
            }
        })
                .setType(new boolean[]{false, true, true, true, true, false})// 默认全部显示
                .setCancelText("取消")//取消按钮文字
                .setSubmitText("确定")//确认按钮文字
                .setContentTextSize(25)//滚轮文字大小
                .setTitleSize(25)//标题文字大小
                .setSubCalSize(25)
                .setTitleText("")//标题文字
                .setOutSideCancelable(false)//点击屏幕，点在控件外部范围时，是否取消显示
                .isCyclic(true)//是否循环滚动
                .setTitleColor(Color.BLACK)//标题文字颜色
                .setSubmitColor(0xff2CB98B)//确定按钮文字颜色
                .setCancelColor(0xff666666)//取消按钮文字颜色
                .setTextColorCenter(0xff2CB98B)
                .setTextXOffset(0, 10, 10, 10, 10, 0)
                .setTitleBgColor(Color.WHITE)//标题背景颜色 Night mode
                .setBgColor(Color.WHITE)//滚轮背景颜色 Night mode
                .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
//                .setRangDate(startDate, endDate)//起始终止年月日设定
                .setLabel("年", "月", "日", "", "", "")//默认设置为年月日时分秒
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                .isDialog(false)//是否显示为对话框样式
                .build();
        pvTime.show();
    }

    private void setWeeks(List<String> list) {
         String[] lists = {"只看当日","每周一", "每周二", "每周三", "每周四", "每周五", "每周六", "每周日"};

        if (list == null || list.size() == 0)
            return;
        if (list.size()==7){
            tv_chouse_weeks.setText("每天");
        }else {
            String we="";
            for (int i=0;i<list.size();i++){
                if (i!=list.size()-1){
                    we = we + lists[Integer.parseInt(list.get(i))] + "、";
                }else {

                    we = we + lists[Integer.parseInt(list.get(i))];
                }
            }
            tv_chouse_weeks.setText(we);
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        isdestry = false;
    }

    private boolean savaData() {
        if (TextUtils.isEmpty(min)) {
            Toast.makeText(getActivity(), "请先添加首次生效时间", Toast.LENGTH_SHORT).show();
            return false;
        }
//        if (weeks.size() == 0) {
//            Toast.makeText(getActivity(), "请先添加重复时间", Toast.LENGTH_SHORT).show();
//            return false;
//        }
        if (TextUtils.isEmpty(voiceUrl)) {
            Toast.makeText(getActivity(), "请先添加语音提醒内容", Toast.LENGTH_SHORT).show();
            return false;
        }
        String weekstr = getweek();
        Map<String, Object> map = new HashMap<>();
        map.put("text", "");
        map.put("audioUrl", voiceUrl);
        map.put("audioSeconds", mediaPlayer.getDuration() / 1000);
        map.put("hour", hour);
        map.put("minute", min);
        map.put("bellType", "1");//1铃声1 2铃声2
        map.put("type", "1");//2安全提醒1用药 提醒
        map.put("week", weekstr);
        if (TextUtils.isEmpty(weekstr)) {
            map.put("date", DateUtils.getStrDate(date));
        } else {
            map.put("date", "");
        }

        NetHandler.Companion.getInst().creatRemind(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CreatRemindResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(CreatRemindResponse uploadResponse) {
                        if (!isdestry) {
                            NavHostFragment.findNavController(AddMyReminderFragment.this).navigateUp();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return true;
    }

    private String getweek() {
        String str = "";
        for (int i = 0; i < weeks.size(); i++) {
            if (i == weeks.size() - 1) {
                str = str + weeks.get(i);
            } else {
                str = str + weeks.get(i) + ",";
            }

        }
        return str;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.im_play:
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                } else {
                    mediaPlayer.start();
                }

                break;
            case R.id.im_delete:
                voiceUrl = "";
                im_play.setVisibility(View.GONE);
                im_delete.setVisibility(View.GONE);
                tv_delete.setVisibility(View.GONE);
                tv_play.setVisibility(View.GONE);
                im_recode.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isdestry = true;
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer = null;
        }
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer = null;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.im_recode:
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    popupWindow = DialogUtils.showToalkPop(getActivity(), view);
                    if (SpeechRecognizerHandler.speechRecognizerHandler != null) {
                        if (SpeechRecognizerHandler.speechRecognizerHandler.isOpen()) {
                            SpeechRecognizerHandler.speechRecognizerHandler.stopAudioInput();
                        }
                    }
                    initRecorder();
                    startRecorder();
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (popupWindow != null && popupWindow.isShowing()) {
                        popupWindow.dismiss();
                        stopRecorder();
                        if (SpeechRecognizerHandler.speechRecognizerHandler != null) {
                            if (!SpeechRecognizerHandler.speechRecognizerHandler.isOpen()) {
                                SpeechRecognizerHandler.speechRecognizerHandler.startAudioInput();
                            }
                        }
                        final File file = new File(filePath);
                        new Thread() {
                            @Override
                            public void run() {
                                super.run();
                                up(file);
                            }
                        }.start();

                    }
                }
                break;
        }

        return true;
    }

    private void up(File file) {
        NetHandler.Companion.getInst().uploadVoice(file)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UploadResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(UploadResponse uploadResponse) {
                        if (uploadResponse != null && uploadResponse.getResult() != null) {
                            if (!TextUtils.isEmpty(uploadResponse.getResult().getUrl())) {
                                im_play.setVisibility(View.VISIBLE);
                                im_delete.setVisibility(View.VISIBLE);
                                tv_delete.setVisibility(View.VISIBLE);
                                tv_play.setVisibility(View.VISIBLE);
                                im_recode.setVisibility(View.GONE);
                                voiceUrl = uploadResponse.getResult().getUrl();
                                try {
                                    mediaPlayer = new MediaPlayer();
                                    mediaPlayer.setDataSource(filePath);
                                    mediaPlayer.prepare();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * 准备录音
     */
    private void initRecorder() {
        recorder = new MediaRecorder();
        /* 设置音频源*/
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        /* 设置输出格式*/
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        /* 设置音频编码器，注意这里，设置成AAC，不然苹果手机播放不了*/
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
//        try {
//            /* 创建一个临时文件，用来存放录音*/
//            tempFile = File.createTempFile("tempFile", ".WAV", SDPathDir);
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        File file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        /* 设置录音文件*/
        recorder.setOutputFile(file.getAbsolutePath());
    }

    /**
     * 开始录音
     */
    private void startRecorder() {
        if (!isSDCardExit) {
            return;
        }
        try {
            recorder.prepare();
            recorder.start();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 停止录音
     */
    private void stopRecorder() {
        if (recorder != null) {
            try {
                recorder.stop();
            } catch (Exception e) {
                // TODO 如果当前java状态和jni里面的状态不一致，
                //e.printStackTrace();
                recorder = null;
                recorder = new MediaRecorder();
            }
            recorder.release();
            recorder = null;
        }
    }

}
