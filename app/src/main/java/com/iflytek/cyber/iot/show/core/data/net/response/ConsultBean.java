package com.iflytek.cyber.iot.show.core.data.net.response;

import java.util.List;

public class ConsultBean {


    private String message;
    private List<String> tryTxt;
    private List<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getTryTxt() {
        return tryTxt;
    }

    public void setTryTxt(List<String> tryTxt) {
        this.tryTxt = tryTxt;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {

        private String name;
        private String fromId;
        private String messageType;
        private String message;
        private String imageUrl;
        private LinkMessageBean linkMessage;
        private String time;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFromId() {
            return fromId;
        }

        public void setFromId(String fromId) {
            this.fromId = fromId;
        }

        public String getMessageType() {
            return messageType;
        }

        public void setMessageType(String messageType) {
            this.messageType = messageType;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public LinkMessageBean getLinkMessage() {
            return linkMessage;
        }

        public void setLinkMessage(LinkMessageBean linkMessage) {
            this.linkMessage = linkMessage;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }
    }

    public static class LinkMessageBean {


        private String id;
        private String name;
        private String shopName;
        private String imageUrl;
        private String price;
        private boolean isInsurance;
        private String drugClassify;
        private String indication;
        private String specification;
        private String readTxt;
        private List<String> tryTxt;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getShopName() {
            return shopName;
        }

        public void setShopName(String shopName) {
            this.shopName = shopName;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public boolean isIsInsurance() {
            return isInsurance;
        }

        public void setIsInsurance(boolean isInsurance) {
            this.isInsurance = isInsurance;
        }

        public String getDrugClassify() {
            return drugClassify;
        }

        public void setDrugClassify(String drugClassify) {
            this.drugClassify = drugClassify;
        }

        public String getIndication() {
            return indication;
        }

        public void setIndication(String indication) {
            this.indication = indication;
        }

        public String getSpecification() {
            return specification;
        }

        public void setSpecification(String specification) {
            this.specification = specification;
        }

        public String getReadTxt() {
            return readTxt;
        }

        public void setReadTxt(String readTxt) {
            this.readTxt = readTxt;
        }

        public List<String> getTryTxt() {
            return tryTxt;
        }

        public void setTryTxt(List<String> tryTxt) {
            this.tryTxt = tryTxt;
        }
    }
}
