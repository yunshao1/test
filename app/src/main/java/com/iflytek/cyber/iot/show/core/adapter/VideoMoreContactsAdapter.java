package com.iflytek.cyber.iot.show.core.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.response.ChatGroupBean;

import java.util.ArrayList;
import java.util.List;

public class VideoMoreContactsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private int type = 0;
    private int[] images = new int[]{R.drawable.icon_video_p1, R.drawable.icon_video_p2, R.drawable.icon_video_p3, R.drawable.icon_video_p4, R.drawable.icon_video_p5};
    private List<ChatGroupBean.GroupsBean> titles = new ArrayList<ChatGroupBean.GroupsBean>();
    private String[] times = new String[]{"12:23", "昨天", "昨天", "昨天"};

    public VideoMoreContactsAdapter(Context context) {
        this.context = context;
    }

    public void setType(int type) {
        this.type = type;
        notifyDataSetChanged();
    }

    public void setDataList(List<ChatGroupBean.GroupsBean> list) {
        this.titles.clear();
        titles.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder vHodler = null;
        vHodler = new VideoMoreContactsAdapter.VHodler(LayoutInflater.from(context).inflate(R.layout.item_video, viewGroup, false));


        return vHodler;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vHodler, final int position) {
            ((VHodler) vHodler).tv_name.setText(titles.get(position).groupName);
            int number = position + 1;
            ((VHodler) vHodler).tv_number.setText(number + "");
            ((VHodler) vHodler).image.setImageResource(images[position]);
            ((VHodler) vHodler).ll_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemclick(titles.get(position));
                }
            });
    }

    public interface OnItemClickListener {
        void onItemclick(ChatGroupBean.GroupsBean bean);
    }

    private OnItemClickListener mListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    @Override
    public int getItemCount() {
        return titles.size();
    }

    public class VHodler extends RecyclerView.ViewHolder {
        public TextView tv_number;
        public ImageView image;
        public TextView tv_name;
        public LinearLayout ll_container;

        public VHodler(@NonNull View itemView) {
            super(itemView);
            tv_number = itemView.findViewById(R.id.tv_number);
            ll_container = itemView.findViewById(R.id.ll_container);
            image = itemView.findViewById(R.id.image);
            tv_name = itemView.findViewById(R.id.tv_name);

        }
    }



}
