package com.iflytek.cyber.iot.show.core.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.response.HealthReportBean;
import com.iflytek.cyber.iot.show.core.utils.DateUtils;
import com.iflytek.cyber.iot.show.core.utils.GlideUtil;

import java.util.ArrayList;
import java.util.List;

public class HealthReportAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<HealthReportBean.ResultBean> list = new ArrayList<>();
    private int type = 0;//0、1、2、3、4、5、6、7


    public HealthReportAdapter(Context context) {
        this.context = context;
    }

    public int getType() {
        return type;
    }

    public HealthReportAdapter setType(int type) {
        this.type = type;
        return this;
    }

    public void setData(List<HealthReportBean.ResultBean> list) {
        this.list = list;
        notifyDataSetChanged();

    }

    public List<HealthReportBean.ResultBean> getList() {
        return list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        RecyclerView.ViewHolder vHodler = null;
        View view = null;
        switch (type) {
            case 0:
                view = LayoutInflater.from(context).inflate(R.layout.item_health_report_item, viewGroup, false);
                RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(RecyclerView.LayoutParams.WRAP_CONTENT,
                        RecyclerView.LayoutParams.MATCH_PARENT);
                params.leftMargin =90;
                view.setLayoutParams(params);
                vHodler = new HealthReportAdapter.VHodler(view);
                break;
        }

        return vHodler;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vHodler, final int position) {
        ((VHodler) vHodler).tv_content.setText(list.get(position).getSummary());
        ((VHodler) vHodler).tv_time.setText(list.get(position).getTimePassed());

        GlideUtil.loadImage(context, list.get(position).getImageUrl(), 0, ((VHodler) vHodler).iv_cover);

        ((VHodler) vHodler).ll_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener!=null){
                    onItemClickListener.onItemClick(list.get(position));
                }
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return type;
    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    public class VHodler extends RecyclerView.ViewHolder {
        public ImageView iv_cover;
        public TextView tv_content;
        public TextView tv_time;
        public LinearLayout ll_top;

        public VHodler(@NonNull View itemView) {
            super(itemView);
            iv_cover = itemView.findViewById(R.id.iv_cover);
            tv_content = itemView.findViewById(R.id.tv_content);
            tv_time = itemView.findViewById(R.id.tv_time);
            ll_top = itemView.findViewById(R.id.ll_top);

        }
    }
    public interface  OnItemClickListener{
        void onItemClick(HealthReportBean.ResultBean bean);
    }
    public  OnItemClickListener onItemClickListener;
    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }
}
