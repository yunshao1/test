package com.iflytek.cyber.iot.show.core.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.TRTCMainActivity;
import com.iflytek.cyber.iot.show.core.adapter.VideoAdapter;
import com.iflytek.cyber.iot.show.core.adapter.VideoMoreContactsAdapter;
import com.iflytek.cyber.iot.show.core.chat.manager.VideoManager;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.BaseResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.ChatGroupBean;
import com.iflytek.cyber.iot.show.core.impl.SpeechRecognizer.SpeechRecognizerHandler;
import com.iflytek.cyber.iot.show.core.utils.Constant;
import com.iflytek.cyber.iot.show.core.utils.RoomUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.navigation.fragment.NavHostFragment;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class VideoMoreContactsFragment extends Fragment {
    LinearLayout ll_finsh;
    private View view;
    private VideoMoreContactsAdapter adapter;
    private RecyclerView recly_view;
    private TextView txl;
    private TextView thjl;
    private final static int REQ_PERMISSION_CODE = 0x1000;
    private SpeechRecognizerHandler instance;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private ProgressDialog progressDialog;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_video_more_contacts, container, false);
            initView();
        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        load();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    private void load() {
        Disposable disposable = NetHandler.Companion.getInst().fetchGroupIdByType(Constant.FRIEND_TYPE_ADDRESS_BOOK)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<BaseResponse<ChatGroupBean>>() {
                           @Override
                           public void accept(BaseResponse<ChatGroupBean> baseResponse) throws Exception {
                               if (baseResponse.isSuccess() && baseResponse.result.groups != null && baseResponse.result.groups.size() > 0) {
                                   adapter.setDataList(baseResponse.result.groups);
                               }
                           }
                       }, new Consumer<Throwable>() {
                           @Override
                           public void accept(Throwable throwable) throws Exception {
                               Toast.makeText(getActivity(),throwable.getMessage()==null?"接口请求失败":throwable.getMessage(),Toast.LENGTH_LONG).show();

                           }
                       }
                );

        mCompositeDisposable.add(disposable);
    }

    public void showDialog(){
        progressDialog.show();
        progressDialog.setContentView( R.layout.progressdialog_layout);
    }
    public void closeDialog(){
        progressDialog.dismiss();
    }

    private void initView() {
        progressDialog = new ProgressDialog(getActivity(),R.style.CustomProgressDialog);
        progressDialog.setCanceledOnTouchOutside(false);
        ll_finsh = this.view.findViewById(R.id.ll_finsh);
        recly_view = view.findViewById(R.id.recycler);
        adapter = new VideoMoreContactsAdapter(getActivity());
        adapter.setOnItemClickListener(new VideoMoreContactsAdapter.OnItemClickListener() {
            @Override
            public void onItemclick(ChatGroupBean.GroupsBean bean) {
                instance.stopAudioInput();
                showDialog();
                HashMap<String, String> map = new HashMap<>();
                map.put("groupName",bean.groupName);
                map.put("video_status", "0");
                map.put("toName", bean.groupName);
                VideoManager.getInstance().initiateVideoCall(getActivity(), RoomUtils.getRoomId(), bean.notifyId, map);
            }
        });
        recly_view.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        recly_view.setAdapter(adapter);
        ll_finsh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TRTCMainActivity activity = (TRTCMainActivity) getActivity();
                activity.closeFragment();
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        closeDialog();
    }

}
