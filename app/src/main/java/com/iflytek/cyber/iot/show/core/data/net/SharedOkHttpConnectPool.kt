package com.iflytek.cyber.iot.show.core.data.net

import okhttp3.ConnectionPool
import java.util.concurrent.TimeUnit

/**
 * Created by Kaka on 19/1/8.
 */
object SharedOkHttpConnectPool {
    @Volatile
    private var sInst: ConnectionPool? = null

    val inst: ConnectionPool?
        get() {
            if (sInst == null) {
                synchronized(SharedOkHttpConnectPool::class.java) {
                    if (sInst == null) {
                        sInst = ConnectionPool(20, 5L, TimeUnit.MINUTES)
                    }
                }
            }
            return sInst
        }
}
