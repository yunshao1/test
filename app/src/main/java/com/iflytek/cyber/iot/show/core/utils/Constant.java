package com.iflytek.cyber.iot.show.core.utils;

import android.app.Activity;
import android.os.Environment;
import android.util.ArrayMap;

import com.iflytek.cyber.iot.show.core.data.net.response.ReminderResponse;

import java.util.List;

public class Constant {

    public static final String API_BASE_URL = "http://speaker.api.shfusion.com";


    public static final String URL_SQYY = "http://elderlyweixin.shfusion.com/Interface/Android/CommunityBooking";
    public static final String URL_BUSHU = "http://elderlyweixin.shfusion.com/Interface/Android/HealthData";
    public static final String URL_XINLV = "http://elderlyweixin.shfusion.com/Interface/Android/HeartRate";
    public static final String URL_XUEYA = "http://elderlyweixin.shfusion.com/Interface/Android/BloodPressure";
    public static final String URL_XUETANG = "http://elderlyweixin.shfusion.com/Interface/Android/BloodSugar";

    public static final String CONFIG_ELDERID = "12";
//    public static final String DEVICE_ID = "3333333";
//    public static final String DEVICE_ID = "4444444";
    public static final String DEVICE_ID = "5555555";
    public static final String VIDEO_STATUS = "video_status";

    public static final int APP_ID = 1400230639;

    public static List<ReminderResponse.ListBean> rmindList;

    public static ArrayMap<String, String> rmindMap = new ArrayMap<>();

    public static Activity activity;

    public static final int FRIEND_TYPE_ADDRESS_BOOK = 0;
    public static final int FRIEND_TYPE_ONLINE_ASK = 1;
    public static final int FRIEND_TYPE_PENSION_ADVISERS = 2;

    public static final String CHAT_RECORD_VOICE_BASE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
    public static final String CHAT_RECORD_VOICE_PATH_NAME = "voice.mp3";
    public static final String CHAT_RECORD_VOICE_PATH = CHAT_RECORD_VOICE_BASE_PATH + CHAT_RECORD_VOICE_PATH_NAME;

}
