package com.iflytek.cyber.iot.show.core.data.net.response;

import java.util.List;

public class AskHealthBean {


    private String message;
    private String tryTxt;
    private String readTxt;
    private List<ListBean> list;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTryTxt() {
        return tryTxt;
    }

    public void setTryTxt(String tryTxt) {
        this.tryTxt = tryTxt;
    }

    public String getReadTxt() {
        return readTxt;
    }

    public void setReadTxt(String readTxt) {
        this.readTxt = readTxt;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * name : 高血压
         * sort : 1
         * tryTxt : ["试试，\u201c蓝小飞，什么是高血压\u201d","试试，\u201c蓝小飞，什么因素会引起高血压\u201d","试试，\u201c蓝小飞，高血压有危害什么\u201d","试试，\u201c蓝小飞，高血压有哪些症状\u201d"]
         */

        private String name;
        private int sort;
        private List<String> tryTxt;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getSort() {
            return sort;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }

        public List<String> getTryTxt() {
            return tryTxt;
        }

        public void setTryTxt(List<String> tryTxt) {
            this.tryTxt = tryTxt;
        }
    }
}
