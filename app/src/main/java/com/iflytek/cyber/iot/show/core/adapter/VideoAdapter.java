package com.iflytek.cyber.iot.show.core.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.response.ChatGroupBean;
import com.iflytek.cyber.iot.show.core.data.net.response.ChatGroupBean2;
import com.iflytek.cyber.iot.show.core.data.net.response.VideoUserBean;

import java.util.ArrayList;
import java.util.List;

public class VideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private int type = 0;
//    private int[] images = new int[]{R.drawable.icon_video_p1, R.drawable.icon_video_p2, R.drawable.icon_video_p3, R.drawable.icon_video_p4, R.drawable.icon_video_p5};
    private List<ChatGroupBean.GroupsBean> titles = new ArrayList<ChatGroupBean.GroupsBean>();
    private String[] times = new String[]{"12:23", "昨天", "昨天", "昨天"};
    public VideoAdapter(Context context) {
        this.context = context;
//        titles.add(new VideoUserBean("张三","Web_trtc_01","eJxlj11PgzAYhe-5FU1vZ1wpK2Mmu2C46AzELU4BbxqkxTWEj5XyafzvKi6RxPf2eXLOeT80AAA8uk-XURwXda6o6ksOwQ2ACF79wbIUjEaKGpL9g7wrheQ0ShSXI9QJIRihqSMYz5VIxMXw*RtVUsUU6ROpYikdm35TFghhA5nGaqqI9xF624Ozu2vrl4eYbImfZiQzh*Uta-fnXTCsjK5O**ekPVuBfZ-aXmSLzcKpdTnMrCDF7qvPTvNj6IbFoxX2jaxOs7m5SQzudKrxDuv1pFKJjF-eMrG5tAiZDmq4rESRjwJGOtG-J-8c1D61L9JIX34_"));
//        titles.add(new VideoUserBean("李华","Web_trtc_02","eJxlj8FOg0AQQO98BeGqscvCruCNLm1EwbSWWullg7DUsRFwWVrE*O8qNpHEub6XmTcfmq7rRhyuLtIsq9pScfVeC0O-0g1knP-Buoacp4pbMv8HRVeDFDwtlJADNAkhGKGxA7koFRRwMjbiiSupMo7wSGryPR8u-W6xEcIWopY7VmA3wGiWsGDJFl0DHY0d70YlfjUJpQ-LhQyYeGCmpwRd99P2ejZ-tKNj8OxF8f3UKuLscHbnmvtwhSeJvXWdfovaFz*87Tdx8ub2bL2bj6sUvIrTWxTTS4eQcdBByAaqchAwMon5nfwzhvapfQFCEl41"));
//        titles.add(new VideoUserBean("张华名","Web_trtc_03","eJxlj11PgzAYhe-5FQ23GFNgLZuJF3PDDHQOw8aiNw0fhRUZq6Uwxey-T3GJTXxvnyfnnPdLAwDo68fwOk7TQ1tLIj851cEN0KF*9Qc5ZxmJJbFF9g-SD84EJXEuqRigiRCyIFQdltFaspxdjC1NiBQyJdBWpCZ7I0PTb8oIQsuG2J6oCisGuHQ3M*95tg6mht*3O4w282Q7XvZw8sBfYeUviqNTtyUqOlku7voRnXpF2DYdCmicRMG8dI08WB3DxKmiyLh-f1rtfIa597JP86pyb5VKyfb08ha2sDNGSB3UUdGwQz0IFjSR*T3553TtpJ0Bth1fiw__"));
//        titles.add(new VideoUserBean("张翠山","Web_trtc_04","eJxlj0trg0AUhff*isGtpY6jo22hixCUSLXEPExxM-i4sZNQHXQUS8l-b2sDGejdfh-nnPulIYT0XbS9z8uyHRrJ5KcAHT0hHet3NygEr1gumd1V-yBMgnfA8qOEboYWpZRgrDq8gkbyI78aByiY7GTJsKNIfXVmc9NfioMxsbFrP6oKr2cY*-tl6DvGlCW0MLNgqMF892A71UMabOKXlbDeytE4RF4frrPgtOD*wrTrJB2WUTWcNuO*7UnopoaMXyESSTaRdEXdIonX4EznZ6VS8g*4vuUS13ugVB00QtfztpkFgi1q-Uz*PV27aN9bll6d"));
    }

    public void setType(int type) {
        this.type = type;
        notifyDataSetChanged();
    }

    public void setDataList(List<ChatGroupBean.GroupsBean> list) {
        this.titles.clear();
        titles.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder vHodler = null;
        if (type == 0) {
            vHodler = new VideoAdapter.VHodler(LayoutInflater.from(context).inflate(R.layout.item_video, viewGroup, false));

        } else {
            vHodler = new VideoAdapter.VHodler1(LayoutInflater.from(context).inflate(R.layout.item_video1, viewGroup, false));

        }

        return vHodler;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vHodler, final int position) {
        if (vHodler instanceof VHodler) {
            ((VHodler) vHodler).tv_name.setText(titles.get(position).groupName);
            int number = position + 1;
            ((VHodler) vHodler).tv_number.setText(number + "");
            Glide.with(context).load(titles.get(position).img).into(((VHodler) vHodler).image);
            ((VHodler) vHodler).ll_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemclick(titles.get(position));
                }
            });
        } else {
            ((VHodler1) vHodler).tv_name.setText(titles.get(position).groupName);
            int number = position + 1;
            ((VHodler1) vHodler).tv_number.setText(number + "");
            Glide.with(context).load(titles.get(position).img).into(((VHodler1) vHodler).image);
//            ((VHodler1) vHodler).image.setImageResource(images[position]);
            if (position < times.length) {
                ((VHodler1) vHodler).tv_time.setText(times[position]);
            } else {
                ((VHodler1) vHodler).tv_time.setText(times[times.length - 1]);
            }
        }
    }

    public interface OnItemClickListener{
        void onItemclick(ChatGroupBean.GroupsBean bean);
    }

    private OnItemClickListener mListener;
    public void setOnItemClickListener(OnItemClickListener listener){
        this.mListener= listener;
    }
    @Override
    public int getItemCount() {
        return titles.size();
    }

    public class VHodler extends RecyclerView.ViewHolder {
        public TextView tv_number;
        public ImageView image;
        public TextView tv_name;
        public LinearLayout ll_container;

        public VHodler(@NonNull View itemView) {
            super(itemView);
            tv_number = itemView.findViewById(R.id.tv_number);
            ll_container = itemView.findViewById(R.id.ll_container);
            image = itemView.findViewById(R.id.image);
            tv_name = itemView.findViewById(R.id.tv_name);

        }
    }

    public class VHodler1 extends RecyclerView.ViewHolder {
        public TextView tv_number;
        public ImageView image;
        public TextView tv_name;
        public TextView tv_time;

        public VHodler1(@NonNull View itemView) {
            super(itemView);
            tv_number = itemView.findViewById(R.id.tv_number);
            image = itemView.findViewById(R.id.image);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_time = itemView.findViewById(R.id.tv_time);
        }
    }


}
