package com.iflytek.cyber.iot.show.core.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.adapter.AdAdapter;
import com.iflytek.cyber.iot.show.core.adapter.ServiceAdapter;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.AdBeanResonse;
import com.iflytek.cyber.iot.show.core.data.net.response.ServiceNewBean;
import com.iflytek.cyber.iot.show.core.utils.DialogUtils;

import androidx.navigation.fragment.NavHostFragment;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ServiceFragment extends BaseFragment {
    LinearLayout ll_finsh;




    RecyclerView recly_view;
    RecyclerView recly_view_ad;
    ServiceAdapter adapter;
    AdAdapter adAdapter;


    private View view;
    private String apiUrl = "";


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_service, container, false);
            initView();
            loadData();
        }

        return view;
    }

    @Override
    public void onActivityCreated(@org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHideBottomBar(true);
        setTipText("试试，“蓝小飞，我要看旅游服务”", R.style.TextBlackFont);
    }

    private void initView() {

        ll_finsh = view.findViewById(R.id.ll_finsh);

        ll_finsh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(ServiceFragment.this).navigateUp();

            }
        });
        recly_view = view.findViewById(R.id.recycler);
        adapter = new ServiceAdapter(getActivity());
        recly_view.setLayoutManager(new GridLayoutManager(getActivity(), 5));
        recly_view.setAdapter(adapter);
        adapter.setOnItemClickListener(new ServiceAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                if (TextUtils.isEmpty(apiUrl))
                    return;
                if (adapter.getList() != null && adapter.getList().size() > 0) {
                    ServiceNewBean.DataBean.PanelBean.PlugBean plugBean = adapter.getList().get(pos);
                    if (plugBean.isEnable()) {
                        if ("1".equals(plugBean.getOpenType())) {
                            Bundle bundle4 = new Bundle();
                            bundle4.putString("title", adapter.getList().get(pos).getName());
                            bundle4.putString("code", adapter.getList().get(pos).getCode());
                            bundle4.putString("apiUrl", apiUrl);
                            bundle4.putString("typeCode", adapter.getList().get(pos).getOpenType());
                            NavHostFragment.findNavController(ServiceFragment.this).navigate(R.id.action_to_mutritious_fragment, bundle4);

                        }
                        if ("2".equals(plugBean.getOpenType())) {
                            Bundle bundle4 = new Bundle();
                            bundle4.putString("title", adapter.getList().get(pos).getName());
                            bundle4.putString("code", adapter.getList().get(pos).getCode());
                            bundle4.putString("apiUrl", apiUrl);
                            bundle4.putString("typeCode", adapter.getList().get(pos).getOpenType());
                            NavHostFragment.findNavController(ServiceFragment.this).navigate(R.id.action_to_home_repair_fragment, bundle4);

                        }
                        if ("3".equals(plugBean.getOpenType())) {
                            Bundle bundle4 = new Bundle();
                            bundle4.putString("title", adapter.getList().get(pos).getName());
                            bundle4.putString("code", adapter.getList().get(pos).getCode());
                            bundle4.putString("apiUrl", apiUrl);
                            NavHostFragment.findNavController(ServiceFragment.this).navigate(R.id.action_to_pharmacy_fragment, bundle4);
                        }
                        if ("0".equals(plugBean.getOpenType())) {
                            Bundle bundle4 = new Bundle();
                            bundle4.putString("title", adapter.getList().get(pos).getName());
                            bundle4.putString("code", adapter.getList().get(pos).getCode());
                            bundle4.putString("apiUrl", apiUrl);
                            bundle4.putString("typeCode", adapter.getList().get(pos).getOpenType());
                            NavHostFragment.findNavController(ServiceFragment.this).navigate(R.id.action_to_service_detail_fragment, bundle4);

                        }

                    } else {
                        DialogUtils.showMyToast(getActivity(), "暂未开放\n敬请期待");
                    }

                }


            }
        });



        recly_view_ad = view.findViewById(R.id.recly_view_ad);
        recly_view_ad.setLayoutManager(new LinearLayoutManager(getActivity()));
        adAdapter = new AdAdapter(getActivity());
        recly_view_ad.setAdapter(adAdapter);
        adAdapter.setOnItemClickListener(new AdAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                if (adAdapter.getList()!=null&&adAdapter.getList().size()>0){
                    Bundle bundle4 = new Bundle();
                    bundle4.putString("title", adAdapter.getList().get(pos).getTitle());
                    bundle4.putString("url", adAdapter.getList().get(pos).getLinkUrl());
                    NavHostFragment.findNavController(ServiceFragment.this).navigate(R.id.action_to_webview_fragment, bundle4);
                }

            }
        });



    }

    private void loadData() {
        NetHandler.Companion.getInst().getPage("VoiceBox","P-001")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ServiceNewBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(ServiceNewBean listResponse) {
                        if (listResponse != null) {
                            if (listResponse.getSuccess() == 0) {
                                    adapter.setData(listResponse.getData().getPanel().get(getPos(listResponse)).getPlug());
                            }

                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    private int getPos(ServiceNewBean listResponse) {
        if (listResponse == null || listResponse.getData() == null
                || listResponse.getData().getPanel() == null
                || listResponse.getData().getPanel().size() == 0)
            return -1;
        int pos = -1;
        int size = listResponse.getData().getPanel().size();
        for (int i = 0; i < size; i++) {
            if ("M-001".equals(listResponse.getData().getPanel().get(i).getCode())) {
                pos = i;
                apiUrl = listResponse.getData().getPanel().get(i).getApiUrl();
            }
            if ("M-002".equals(listResponse.getData().getPanel().get(i).getCode())) {
                loadAd("M-002", listResponse.getData().getPanel().get(i).getApiUrl());
            }
        }
        return pos;

    }

    private void loadAd(String code, String url) {
        if (TextUtils.isEmpty(url))
            return;
        NetHandler.Companion.getInst().getAd(url, code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AdBeanResonse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(AdBeanResonse listResponse) {
                        if (listResponse != null && listResponse.getList() != null) {
                            adAdapter.setData(listResponse.getList());

                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
