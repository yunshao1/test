package com.iflytek.cyber.iot.show.core.data.net.response;

/**
 * @author cloud.wang
 * @date 2019/6/2
 * @Description: TODO
 */
public class ActionResponse {

    /**
     * result : {}
     * tryTxt : ["string"]
     * message : string
     */
    private Object list;
    private String tryTxt;
    private String message;
    private String readTxt;

    public Object isList() {
        return list;
    }

    public void setList(Object list) {
        this.list = list;
    }

    public String getReadTxt() {
        return readTxt;
    }

    public void setReadTxt(String readTxt) {
        this.readTxt = readTxt;
    }


    public void setTryTxt(String tryTxt) {
        this.tryTxt = tryTxt;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getTryTxt() {
        return tryTxt;
    }

    public String getMessage() {
        return message;
    }

}
