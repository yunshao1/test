package com.iflytek.cyber.iot.show.core.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.response.PersonListMessageResponse;
import com.iflytek.cyber.iot.show.core.utils.GlideUtil;

import java.util.List;

public class RelativesMessagePersonListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<PersonListMessageResponse.ListBean.FamilyMessagesBean> list;

    public List<PersonListMessageResponse.ListBean.FamilyMessagesBean> getList() {
        return list;
    }

    public RelativesMessagePersonListAdapter(Context context) {
        this.context = context;
    }


    public void setDataList(List<PersonListMessageResponse.ListBean.FamilyMessagesBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder vHodler = null;
        vHodler = new VHodler(LayoutInflater.from(context).inflate(R.layout.item_person_list_message, viewGroup, false));

        return vHodler;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vHodler, final int position) {
        ((VHodler) vHodler).tv_name.setText(list.get(position).getName() + "-" + list.get(position).getNickName());
        GlideUtil.loadImage(context, list.get(position).getImg(), 0, ((VHodler) vHodler).image);
        int number = position + 1;
        ((VHodler) vHodler).tv_number.setText(number + "");
        if (list.get(position).getUnreadCount() > 0) {
            ((VHodler) vHodler).tv_message_number.setVisibility(View.VISIBLE);
        } else {
            ((VHodler) vHodler).tv_message_number.setVisibility(View.GONE);
        }
        ((VHodler) vHodler).tv_message_number.setText(list.get(position).getUnreadCount()+"");
        ((VHodler) vHodler).ll_container.setOnClickListener(view -> {
            mListener.onItemclick(position);
        });
        if (list.get(position).isIsAudio()) {
            ((VHodler) vHodler).tv_message.setText("[语音消息]");

        } else {
            ((VHodler) vHodler).tv_message.setText(list.get(position).getMessage());
        }
    }

    public interface OnItemClickListener {
        void onItemclick(int position);
    }

    private OnItemClickListener mListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        else
            return list.size();
    }

    public class VHodler extends RecyclerView.ViewHolder {
        public TextView tv_number;
        public ImageView image;
        public TextView tv_name;
        public TextView tv_message;
        public LinearLayout ll_container;
        public TextView tv_message_number;

        public VHodler(@NonNull View itemView) {
            super(itemView);
            tv_number = itemView.findViewById(R.id.tv_number);
            ll_container = itemView.findViewById(R.id.ll_container);
            image = itemView.findViewById(R.id.image);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_message_number = itemView.findViewById(R.id.tv_message_number);
            tv_message = itemView.findViewById(R.id.tv_message);
        }
    }


}
