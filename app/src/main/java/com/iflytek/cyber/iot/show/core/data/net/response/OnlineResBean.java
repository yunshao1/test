package com.iflytek.cyber.iot.show.core.data.net.response;

import java.util.List;

public class OnlineResBean {


    private String message;
    private ResultBean result;
    private List<?> tryTxt;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public List<?> getTryTxt() {
        return tryTxt;
    }

    public void setTryTxt(List<?> tryTxt) {
        this.tryTxt = tryTxt;
    }

    public static class ResultBean {
        /**
         * name : 张翠华
         * fromId : 1
         * messageType : 1
         * message : http://resource.shfusion.com/voice/consult/2019-06-09/aa672ac19741-aa56-2b43-df33-f4421dcd.wav
         * imageUrl : http://resource.shfusion.com/image/neighbor/consult/我.png
         * linkMessage : null
         * time : 2019-06-09T19:55:33.4638534+08:00
         */

        private String name;
        private String fromId;
        private String messageType;
        private String message;
        private String imageUrl;
        private Object linkMessage;
        private String time;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFromId() {
            return fromId;
        }

        public void setFromId(String fromId) {
            this.fromId = fromId;
        }

        public String getMessageType() {
            return messageType;
        }

        public void setMessageType(String messageType) {
            this.messageType = messageType;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public Object getLinkMessage() {
            return linkMessage;
        }

        public void setLinkMessage(Object linkMessage) {
            this.linkMessage = linkMessage;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }
    }
}
