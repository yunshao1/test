package com.iflytek.cyber.iot.show.core.fragment;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.adapter.RelativesMessageAdapter;
import com.iflytek.cyber.iot.show.core.chat.ChatMessage;
import com.iflytek.cyber.iot.show.core.data.local.LoginSession;
import com.iflytek.cyber.iot.show.core.utils.GlideUtil;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.List;

/**
 * @author cloud.wang
 * @date 2019-08-03
 * @Description: TODO
 */
public class OnlineAskAdapter extends MultiItemTypeAdapter<ChatMessage> {

    public OnlineAskAdapter(Context context, List<ChatMessage> dataList) {
        super(context, dataList);

        addItemViewDelegate(new LeftMessageDelegate());
        addItemViewDelegate(new RightMessageDelegate());
    }


    class LeftMessageDelegate implements ItemViewDelegate<ChatMessage> {

        @Override
        public int getItemViewLayoutId() {
            return R.layout.item_message_left;
        }

        @Override
        public boolean isForViewType(ChatMessage item, int position) {
            return !TextUtils.equals(item.getFromId(), LoginSession.INSTANCE.getSUserId());
        }

        @Override
        public void convert(ViewHolder holder, ChatMessage chatMessage, int position) {
            //txt   文本   img   图片   audio   音频   video   视频
            if (TextUtils.equals("audio", chatMessage.getMsgType())) {
                holder.getView(R.id.read_txt).setVisibility(View.GONE);
                holder.getView(R.id.image_laba_left).setVisibility(View.VISIBLE);
                holder.getView(R.id.content).setVisibility(View.GONE);
                holder.setText(R.id.tv_video_seconds, "");
            } else {
//                else if (TextUtils.equals("txt", chatMessage.getMsgType())) {
                holder.getView(R.id.content).setVisibility(View.VISIBLE);
                holder.getView(R.id.read_txt).setVisibility(View.GONE);
                holder.getView(R.id.image_laba_left).setVisibility(View.GONE);
                holder.setText(R.id.tv_video_seconds, "");
                holder.setText(R.id.content, chatMessage.getDetailMessage() == null
                        ? chatMessage.getMsgContent() : chatMessage.getDetailMessage().getMsgContent());
            }

            GlideUtil.load(mContext, chatMessage.getDetailMessage().getIconImg(), 0, holder.getView(R.id.image_head_left));
            holder.setImageResource(R.id.image_laba_left, R.drawable.icon_laba_left);
        }
    }

    class RightMessageDelegate implements ItemViewDelegate<ChatMessage> {

        @Override
        public int getItemViewLayoutId() {
            return R.layout.item_message_right;
        }

        @Override
        public boolean isForViewType(ChatMessage item, int position) {
            return TextUtils.equals(item.getFromId(), LoginSession.INSTANCE.getSUserId());
        }

        @Override
        public void convert(ViewHolder holder, ChatMessage chatMessage, int position) {
            GlideUtil.load(mContext, chatMessage.getDetailMessage().getIconImg(), 0, holder.getView(R.id.image_head_left));
            holder.setImageResource(R.id.image_laba_left, R.drawable.icon_laba_right);
            if (!TextUtils.equals("audio", chatMessage.getMsgType())) {//文字
                holder.getView(R.id.content).setVisibility(View.VISIBLE);
                holder.getView(R.id.image_laba_left).setVisibility(View.GONE);
                holder.setText(R.id.content, chatMessage.getDetailMessage() == null
                        ? chatMessage.getMsgContent() : chatMessage.getDetailMessage().getMsgContent());
                holder.setText(R.id.tv_video_right_seconds, "");
            } else {
                //文字内容
                holder.getView(R.id.content).setVisibility(View.GONE);
                //喇叭
                holder.getView(R.id.image_laba_left).setVisibility(View.VISIBLE);
                //音频多少秒
                holder.setText(R.id.tv_video_right_seconds, "");
            }
        }
    }
}
