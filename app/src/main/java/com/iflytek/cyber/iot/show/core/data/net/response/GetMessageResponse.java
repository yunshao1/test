package com.iflytek.cyber.iot.show.core.data.net.response;

import java.util.List;

public class GetMessageResponse {

    private String message;
    private String tryTxt;
    private String readTxt;
    private List<ListBean> list;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTryTxt() {
        return tryTxt;
    }

    public void setTryTxt(String tryTxt) {
        this.tryTxt = tryTxt;
    }

    public String getReadTxt() {
        return readTxt;
    }

    public void setReadTxt(String readTxt) {
        this.readTxt = readTxt;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * fromId : 6190
         * fromType : Wristband
         * toId : 619001
         * msgContent :
         * audioUrl : http://common.api.shfusion.com/upload/msg/c32505e44b204a379f300d9c4298d19d.mp3
         * audioSecond : 1520
         * dateTime : 2019-07-27T16:48:38.003+08:00
         * id : aa972412506b-be97-d949-19c1-dde6cede
         * isRead : false
         * isAudio : true
         * isMine : true
         * fromUserImg : http://resource.shfusion.com/image/common/elder/user.jpg
         */

        private String fromId;
        private String fromType;
        private String toId;
        private String msgContent;
        private String audioUrl;
        private int audioSecond;
        private String dateTime;
        private String id;
        private boolean isRead;
        private boolean isAudio;
        private boolean isMine;
        private String fromUserImg;

        public String getFromId() {
            return fromId;
        }

        public void setFromId(String fromId) {
            this.fromId = fromId;
        }

        public String getFromType() {
            return fromType;
        }

        public void setFromType(String fromType) {
            this.fromType = fromType;
        }

        public String getToId() {
            return toId;
        }

        public void setToId(String toId) {
            this.toId = toId;
        }

        public String getMsgContent() {
            return msgContent;
        }

        public void setMsgContent(String msgContent) {
            this.msgContent = msgContent;
        }

        public String getAudioUrl() {
            return audioUrl;
        }

        public void setAudioUrl(String audioUrl) {
            this.audioUrl = audioUrl;
        }

        public int getAudioSecond() {
            return audioSecond;
        }

        public void setAudioSecond(int audioSecond) {
            this.audioSecond = audioSecond;
        }

        public String getDateTime() {
            return dateTime;
        }

        public void setDateTime(String dateTime) {
            this.dateTime = dateTime;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public boolean isIsRead() {
            return isRead;
        }

        public void setIsRead(boolean isRead) {
            this.isRead = isRead;
        }

        public boolean isIsAudio() {
            return isAudio;
        }

        public void setIsAudio(boolean isAudio) {
            this.isAudio = isAudio;
        }

        public boolean isIsMine() {
            return isMine;
        }

        public void setIsMine(boolean isMine) {
            this.isMine = isMine;
        }

        public String getFromUserImg() {
            return fromUserImg;
        }

        public void setFromUserImg(String fromUserImg) {
            this.fromUserImg = fromUserImg;
        }
    }
}
