package com.iflytek.cyber.iot.show.core.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.adapter.AskHealthAdapter;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.AskHealthBean;

import androidx.navigation.fragment.NavHostFragment;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * 问健康
 */
public class AskHealthFragment extends BaseFragment {
    LinearLayout ll_finsh;
    private RecyclerView recyclerView;
    private AskHealthAdapter adapter;
    private TextView tv_content;

    private View view;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_ask_health, container, false);
            initView();
            load();
        }

        return view;
    }

    private void load() {


        NetHandler.Companion.getInst().getAskHealthByGrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AskHealthBean>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(AskHealthBean listResponse) {
                        if ("success".equals(listResponse.getMessage())) {
                            setData(listResponse);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void setData(AskHealthBean listResponse) {
        if (listResponse == null)
            return;
        adapter.setData(listResponse.getList());
        String text = "";
        if (listResponse.getList() == null || listResponse.getList().get(0).getTryTxt() == null) {
            return;
        }
        int size = listResponse.getList().get(0).getTryTxt().size();
        for (int i = 0; i < size; i++) {
            if (i != size - 1) {
                text =text+ listResponse.getList().get(0).getTryTxt().get(i) + "\n";
            } else {
                text =text+ listResponse.getList().get(0).getTryTxt().get(i);
            }
        }
        tv_content.setText(text);

    }


    private void initView() {
        ll_finsh = view.findViewById(R.id.ll_finsh);
        recyclerView = view.findViewById(R.id.recyclerView);
        tv_content = view.findViewById(R.id.tv_content);
        adapter = new AskHealthAdapter(getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        ll_finsh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(AskHealthFragment.this).navigateUp();

            }
        });
        adapter.setOnItemClickListener(new AskHealthAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(AskHealthBean.ListBean bean) {
                if (bean == null || bean.getTryTxt() == null)
                    return;
                int size = bean.getTryTxt().size();
                String text = "";
                for (int i = 0; i < size; i++) {
                    if (i != size - 1) {
                        text = text + bean.getTryTxt().get(i) + "\n";
                    } else {
                        text = text + bean.getTryTxt().get(i);
                    }
                }
                tv_content.setText(text);
            }
        });
    }

}
