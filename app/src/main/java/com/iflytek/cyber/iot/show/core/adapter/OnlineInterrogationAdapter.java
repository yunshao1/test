package com.iflytek.cyber.iot.show.core.adapter;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.response.ConsultBean;
import com.iflytek.cyber.iot.show.core.utils.GlideUtil;

import java.io.IOException;
import java.util.List;

public class OnlineInterrogationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ConsultBean.ResultBean> list;
    private Context context;


    public OnlineInterrogationAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<ConsultBean.ResultBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }
    public void addData(ConsultBean.ResultBean bean) {
        this.list.add(bean);
        notifyDataSetChanged();
    }


    @Override
    public int getItemViewType(int position) {
        if (list != null && list.size() > 0) {
            if ("1".equals(list.get(position).getFromId())) {
                position = 1;
            } else {
                if ("2".equals(list.get(position).getMessageType())) {
                    position = 2;
                } else {
                    position = 0;
                }
            }

        } else {
            position = -1;
        }
        return position;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        RecyclerView.ViewHolder vHodler = null;
        switch (position) {
            case 0:
                vHodler = new OnlineInterrogationAdapter.VHodlerLeft(LayoutInflater.from(context).inflate(R.layout.item_online_left, viewGroup, false));
                break;
            case 1:
                vHodler = new OnlineInterrogationAdapter.VHodlerRight(LayoutInflater.from(context).inflate(R.layout.item_online_right, viewGroup, false));
                break;
            case 2:
                vHodler = new OnlineInterrogationAdapter.VHodlerCenter(LayoutInflater.from(context).inflate(R.layout.item_online_center, viewGroup, false));
                break;
        }
        return vHodler;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder vHodler, final int position) {
        if (vHodler instanceof OnlineInterrogationAdapter.VHodlerLeft) {
            if (list.get(position).getMessageType().equals("0")) {//文字
                ((VHodlerLeft) vHodler).content.setVisibility(View.VISIBLE);
                ((VHodlerLeft) vHodler).image_laba_left.setVisibility(View.GONE);
                ((VHodlerLeft) vHodler).content.setText(list.get(position).getMessage().toString());
            } else {
                if (list.get(position).getMessageType().equals("0"))
                    ((VHodlerLeft) vHodler).image_laba_left.setVisibility(View.VISIBLE);
                ((VHodlerLeft) vHodler).content.setVisibility(View.GONE);
            }
            if ((list.get(position).getMessageType().equals("1"))) {//音频
                ((VHodlerLeft) vHodler).ll_bg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onItemClickListener != null) {
                            onItemClickListener.onItemClick(list.get(position).getMessage().toString());
                        }
                    }
                });
            }
            GlideUtil.load(context, list.get(position).getImageUrl(), 0, ((VHodlerLeft) vHodler).image_head_left);
            ((VHodlerLeft) vHodler).image_laba_left.setImageResource(R.drawable.icon_laba_left);
        } else if (vHodler instanceof OnlineInterrogationAdapter.VHodlerRight) {
            GlideUtil.load(context, list.get(position).getImageUrl(), 0, ((VHodlerRight) vHodler).image_head_left);
            ((VHodlerRight) vHodler).image_laba_left.setImageResource(R.drawable.icon_laba_right);
            if (list.get(position).getMessageType().equals("0")) {//文字
                ((VHodlerRight) vHodler).content.setVisibility(View.VISIBLE);
                ((VHodlerRight) vHodler).image_laba_left.setVisibility(View.GONE);
                ((VHodlerRight) vHodler).content.setText(list.get(position).getMessage().toString());
            } else {
                ((VHodlerRight) vHodler).image_laba_left.setVisibility(View.VISIBLE);
                ((VHodlerRight) vHodler).content.setVisibility(View.GONE);
            }
            if ((list.get(position).getMessageType().equals("1"))) {//音频
                ((VHodlerRight) vHodler).ll_bg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onItemClickListener != null) {
                            onItemClickListener.onItemClick(list.get(position).getMessage().toString());
                        }
                    }
                });
            }
        } else {
            ((VHodlerCenter) vHodler).tv_bot_title.setText(list.get(position).getLinkMessage().getName());
            ((VHodlerCenter) vHodler).tv_bot_price.setText(list.get(position).getLinkMessage().getPrice());
            ((VHodlerCenter) vHodler).tv_bot_default.setText(list.get(position).getLinkMessage().getDrugClassify());
            GlideUtil.load(context, list.get(position).getLinkMessage().getImageUrl(), 0, ((VHodlerCenter) vHodler).sub_image);
            GlideUtil.load(context, list.get(position).getImageUrl(), 0, ((VHodlerCenter) vHodler).im_head);
        }

    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        else
            return list.size();

    }

    public class VHodlerLeft extends RecyclerView.ViewHolder {
        public ImageView image_laba_left;
        public ImageView image_head_left;
        public TextView content;
        public LinearLayout ll_bg;

        public VHodlerLeft(@NonNull View itemView) {
            super(itemView);
            content = itemView.findViewById(R.id.content);
            image_laba_left = itemView.findViewById(R.id.image_laba_left);
            image_head_left = itemView.findViewById(R.id.image_head_left);
            ll_bg = itemView.findViewById(R.id.ll_bg);


        }
    }

    public class VHodlerCenter extends RecyclerView.ViewHolder {
        public LinearLayout ll_bot;
        public ImageView sub_image;
        public TextView tv_bot_title;
        public TextView tv_bot_default;
        public TextView tv_bot_price;
        public ImageView im_head;


        public VHodlerCenter(@NonNull View itemView) {
            super(itemView);
            ll_bot = itemView.findViewById(R.id.ll_bot);
            sub_image = itemView.findViewById(R.id.sub_image);
            tv_bot_title = itemView.findViewById(R.id.tv_bot_title);
            tv_bot_default = itemView.findViewById(R.id.tv_bot_default);
            tv_bot_price = itemView.findViewById(R.id.tv_bot_price);
            im_head = itemView.findViewById(R.id.im_head);
        }
    }

    public class VHodlerRight extends RecyclerView.ViewHolder {
        public ImageView image_laba_left;
        public ImageView image_head_left;
        public TextView content;
        public LinearLayout ll_bg;

        public VHodlerRight(@NonNull View itemView) {
            super(itemView);
            content = itemView.findViewById(R.id.content);
            image_laba_left = itemView.findViewById(R.id.image_laba_left);
            image_head_left = itemView.findViewById(R.id.image_head_left);
            ll_bg = itemView.findViewById(R.id.ll_bg);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(String url);
    }

    public OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
