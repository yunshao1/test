package com.iflytek.cyber.iot.show.core.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.adapter.ServiceAllAdapter;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.ActionResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.ServiceBean;
import com.iflytek.cyber.iot.show.core.utils.DateUtils;
import com.iflytek.cyber.iot.show.core.widget.recyclerview.AlphaTransformer;
import com.iflytek.cyber.iot.show.core.widget.recyclerview.FlingRecycleView;
import com.iflytek.cyber.iot.show.core.widget.recyclerview.GalleryLayoutManager;
import com.iflytek.cyber.iot.show.core.widget.recyclerview.LinearItemDecoration;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import androidx.navigation.fragment.NavHostFragment;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * 服务成点击详情页面
 */
public class ServiceDetailFragment extends BaseFragment {
    LinearLayout ll_finsh;
    private FlingRecycleView recyclerView;
    private ServiceAllAdapter adapter;
    private String code = "";
    private String title = "";
    private String apiUrl;

    private TextView tv_title;

    private View view;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_service_detail, container, false);
            initView();
        }
        return view;
    }

    @Override
    public void onActivityCreated(@org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void load(String keyword) {
        Map<String, String> map = new HashMap<>();
        map.put("categoryCode", code);
        map.put("pageSize", "20");
        map.put("pageNo", "1");
        map.put("keyword", keyword);
        NetHandler.Companion.getInst().getService2(apiUrl,map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ServiceBean>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(ServiceBean listResponse) {
                        if (listResponse != null) {
                            setData(listResponse, "");
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }



    private void setData(ServiceBean listResponse, String keyword) {
        if (!TextUtils.isEmpty(keyword)) {
            playVoice("以下是你要找的内容");
        } else {
            if (listResponse.getTryTxt() != null  ) {
                setHideBottomBar(true);
                setTipText(listResponse.getTryTxt(), R.style.TextWhiteFont);
            }
        }

        if (listResponse.getResult() != null && listResponse.getResult().size() > 0) {
            adapter.setData(listResponse.getResult());
        } else {
            if (!TextUtils.isEmpty(keyword)) {
                playVoice("找不到你要的内容");
            }
        }
    }


    private void initView() {
        code = getArguments().getString("code");
        title = getArguments().getString("title");
        apiUrl = getArguments().getString("apiUrl");
        ll_finsh = view.findViewById(R.id.ll_finsh);
        tv_title = view.findViewById(R.id.tv_title);
        tv_title.setText(title);
        recyclerView = view.findViewById(R.id.recyclerView);

        load("");
        recyclerView.setFlingAble(false);
        GalleryLayoutManager layoutManager = new GalleryLayoutManager(GalleryLayoutManager.HORIZONTAL);
        layoutManager.attach(recyclerView, 0);
        layoutManager.setOnItemSelectedListener(new GalleryLayoutManager.OnItemSelectedListener() {
            @Override
            public void onItemSelected(RecyclerView recyclerView, View item, int position) {
                Log.i("Cloud", "onItemSelected====" + position);
                mSelectBean = adapter.getList().get(position);
                if (mSelectBean.getTryTxt() != null && mSelectBean.getTryTxt().size() > 0) {
                    setHideBottomBar(true);
                    setTipText(mSelectBean.getTryTxt().get(0), R.style.TextWhiteFont);
                }
                playVoice(mSelectBean.getReadTxt());
            }
        });
        layoutManager.setItemTransformer(new AlphaTransformer());
        LinearItemDecoration divider = new LinearItemDecoration.Builder(getActivity())
                .setOrientation(LinearLayoutManager.HORIZONTAL)
                .setSpan(60f)
                .setColorResource(android.R.color.transparent)
                .setShowLastLine(true)
                .build();
        recyclerView.addItemDecoration(divider);
        adapter = new ServiceAllAdapter(getActivity());
        recyclerView.setAdapter(adapter);
        ll_finsh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(ServiceDetailFragment.this).navigateUp();

            }
        });
    }

    private ServiceBean.ResultBean mSelectBean;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    /**
     * @param input 下单或者安排
     */
    public void submitAction(final String input) {
        if (mSelectBean == null) {
            return;
        }
        Map<String,String>map = new ArrayMap<>();
        map.put("serviceId",mSelectBean.getId());
        map.put("typeCode",code);
        map.put("goodProperty","");//配餐传早餐中餐午餐
        map.put("date",DateUtils.getStrDate(new Date()));
        map.put("content",title);
        map.put("actionName","下单");
        mCompositeDisposable.add(NetHandler.Companion.getInst().submitAction2(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ActionResponse>() {
                    @Override
                    public void accept(ActionResponse actionResponse) throws Exception {
                        if ("success".equals(actionResponse.getMessage())) {
                            playVoice("下单成功");
                            Bundle bundle = new Bundle();
                            bundle.putString("title", input);
                            bundle.putString("content", "好的，下单成功");
                            NavHostFragment.findNavController(ServiceDetailFragment.this).navigate(R.id.apply_fragment, bundle);
                        } else {
                            Log.i("Cloud", actionResponse.getMessage());
//                            Toast.makeText(getLauncher(), "报名失败，"+ actionResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.i("Cloud", "onError::"+ throwable.getMessage());
//                        Toast.makeText(getLauncher(), "网络请求失败，导致报名失败，请重新"+ throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }));
    }

    private void playVoice(String speakText) {
        if (!TextUtils.isEmpty(speakText)) {
            NetHandler.Companion.getInst().playVoice(speakText);
        }
    }

}
