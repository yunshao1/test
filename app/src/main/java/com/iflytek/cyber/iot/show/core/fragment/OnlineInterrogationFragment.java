package com.iflytek.cyber.iot.show.core.fragment;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.navigation.fragment.NavHostFragment;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.chat.ChatDetailMessage;
import com.iflytek.cyber.iot.show.core.chat.ChatMessage;
import com.iflytek.cyber.iot.show.core.chat.ChatResponse;
import com.iflytek.cyber.iot.show.core.chat.SessionParamBean;
import com.iflytek.cyber.iot.show.core.chat.event.ChatEvent;
import com.iflytek.cyber.iot.show.core.chat.manager.VideoManager;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.BaseResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.ChatGroupBean;
import com.iflytek.cyber.iot.show.core.data.net.response.JoinGroupResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.UploadResponse;
import com.iflytek.cyber.iot.show.core.impl.SpeechRecognizer.SpeechRecognizerHandler;
import com.iflytek.cyber.iot.show.core.utils.Constant;
import com.iflytek.cyber.iot.show.core.utils.DialogUtils;
import com.iflytek.cyber.iot.show.core.utils.RoomUtils;
import com.iflytek.cyber.iot.show.core.utils.SharedPreUtils;
import com.iflytek.cyber.iot.show.core.utils.scheduler.TaskScheduler;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static com.iflytek.cyber.iot.show.core.chat.event.ChatEvent.EVENT_CREATE_MESSAGE_SESSION;

/**
 * 在线问诊
 */
public class OnlineInterrogationFragment extends BaseFragment implements View.OnTouchListener, View.OnClickListener {

    private View rootView;
    private TextView tvSpeak;
    private RecyclerView recycler;

    private List<ChatMessage> mMessageList;
    private OnlineAskAdapter mAdapter;
    private MediaPlayer mediaPlayer = new MediaPlayer();
    private PopupWindow popupWindow;
    private MediaRecorder recorder;
    private boolean isSDCardExit; // 判断SDCard是否存在
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private ChatGroupBean.GroupsBean mGroupBean;
    private String mGroupId;
    private boolean hasReceiveEvent = false;

    private boolean isOnlineAsk = true;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_online_interogation, container, false);

        initView(view);

        isSDCardExit = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);

        EventBus.getDefault().register(this);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!SpeechRecognizerHandler.speechRecognizerHandler.isOpen()) {
            //打开讯飞的语音类，防止讯飞无法唤醒
            SpeechRecognizerHandler.speechRecognizerHandler.startAudioInput();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mediaPlayer != null) {
            try {
                mediaPlayer.stop();
            } catch (Exception e) {
                // TODO 如果当前java状态和jni里面的状态不一致，
                //e.printStackTrace();
                mediaPlayer = null;
                mediaPlayer = new MediaPlayer();
            }
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        EventBus.getDefault().unregister(this);

        if (!SpeechRecognizerHandler.speechRecognizerHandler.isOpen()) {
            //打开讯飞的语音类，防止讯飞无法唤醒
            SpeechRecognizerHandler.speechRecognizerHandler.startAudioInput();
        }
    }

    private void initView(View view) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            isOnlineAsk = bundle.getBoolean("is_online_ask", true);
        }

        rootView = view.findViewById(R.id.root_view);
        ImageView ivBack = view.findViewById(R.id.iv_back);
        ivBack.setOnClickListener(this);
        TextView tvTitle = view.findViewById(R.id.tv_title);
        if (!isOnlineAsk) {
            tvTitle.setText("养老顾问");
        }
        tvTitle.setOnClickListener(this);
        ImageView ivVideo = view.findViewById(R.id.iv_video);
        ivVideo.setOnClickListener(this);
        tvSpeak = view.findViewById(R.id.tv_speak);
        tvSpeak.setOnTouchListener(this);
        recycler = (RecyclerView) view.findViewById(R.id.recycler);

        if (mMessageList == null) {
            mMessageList = new ArrayList<>();
        }
        mAdapter = new OnlineAskAdapter(getActivity(), mMessageList);
        recycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recycler.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                if (!TextUtils.equals("audio", mMessageList.get(position).getMsgType())) {
                    return;
                }

                final String url = mMessageList.get(position).getDetailMessage() == null
                        ? mMessageList.get(position).getMsgContent()
                        : mMessageList.get(position).getDetailMessage().getMsgContent();
                TaskScheduler.execute(new Runnable() {
                    @Override
                    public void run() {
                        if (mediaPlayer != null) {
                            try {
                                mediaPlayer.stop();
                            } catch (IllegalStateException e) {
                                mediaPlayer = null;

                            }
                            try {
                                mediaPlayer = new MediaPlayer();
                                mediaPlayer.setDataSource(url);
                                mediaPlayer.prepare();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            try {
                                mediaPlayer = new MediaPlayer();
                                mediaPlayer.setDataSource(url);
                                mediaPlayer.prepare();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        mediaPlayer.start();
                    }
                });
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });

        load();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_back || v.getId() == R.id.tv_title) {
            NavHostFragment.findNavController(OnlineInterrogationFragment.this).navigateUp();
        } else if (v.getId() == R.id.iv_video) {
            if (SpeechRecognizerHandler.speechRecognizerHandler.isOpen()) {
                //关闭讯飞的语音类，防止音频通道占用
                SpeechRecognizerHandler.speechRecognizerHandler.stopAudioInput();
            }
            //视频通话
            HashMap<String, String> map = new HashMap<>();
            map.put("groupName",mGroupBean.groupName);
            map.put("active","true");
            map.put("img",mGroupBean.img);
            map.put("video_status", "0");
            map.put("toName", mGroupBean.groupName);
            VideoManager.getInstance().initiateVideoCall(getLauncher(), RoomUtils.getRoomId(), mGroupBean.notifyId, map);
        } else if (v.getId() == R.id.ll_more_people) {

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void handleEvent(ChatEvent chatEvent) {
        if (chatEvent.getMessageType() == ChatEvent.EVENT_FINISH_CONNECTION) {
            hasReceiveEvent = true;
            //拉取groupId
            if (!TextUtils.isEmpty(mGroupId)) {
                Log.i("ChatService", "handleEvent  " + mGroupId + "之前的消息列表是否有长度：" + mMessageList.size());
                if (mMessageList != null) {
                    mMessageList.clear();
                }

                //加入组
                joinGroup();
            }
            //拉取历史消息
        } else if (chatEvent.getMessageType() == ChatEvent.EVENT_MESSAGE) {
            if (chatEvent.getMessageContent() instanceof ChatResponse) {
                ChatResponse response = (ChatResponse) chatEvent.getMessageContent();
                if (response == null) {
                    return;
                }

                for (ChatMessage message: response.message) {
                    if (isOnlineAsk && message.getGroupType() == 1) {
                        mMessageList.add(message);
                    } else if (!isOnlineAsk && message.getGroupType() == 2) {
                        mMessageList.add(message);
                    }
                }

                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                    recycler.scrollToPosition(mAdapter.getItemCount()-1);
                }
            }
        }
    }

    private void load() {
        int type = isOnlineAsk ? Constant.FRIEND_TYPE_ONLINE_ASK : Constant.FRIEND_TYPE_PENSION_ADVISERS;
        Disposable disposable = NetHandler.Companion.getInst().fetchGroupIdByType(type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<BaseResponse<ChatGroupBean>>() {
                       @Override
                       public void accept(BaseResponse<ChatGroupBean> baseResponse) throws Exception {
                           if (baseResponse.isSuccess() && baseResponse.result.groups != null && baseResponse.result.groups.size() > 0) {
                               mGroupId = baseResponse.result.groups.get(0).groupId;
                               mGroupBean = baseResponse.result.groups.get(0);
                               if (hasReceiveEvent) {
                                   Log.i("Cloud", "fetchGroupIdByType  " + mGroupId);
                                   joinGroup();
                               }
                           }
                       }
                    }, new Consumer<Throwable>() {
                       @Override
                       public void accept(Throwable throwable) throws Exception {

                       }
                    }
                );

        mCompositeDisposable.add(disposable);
    }

    private void joinGroup() {
        SessionParamBean paramBean = new SessionParamBean(mGroupId + "");
        paramBean.setGroupType(isOnlineAsk ? 1 : 2);
        Disposable disposable = NetHandler.Companion.getInst().joinGroup(paramBean)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<BaseResponse<JoinGroupResponse>>() {
                               @Override
                               public void accept(BaseResponse<JoinGroupResponse> joinGroupResponse) throws Exception {
                                   Log.i("Cloud", "JoinGroupResponse  " + joinGroupResponse.toString());
                                   EventBus.getDefault().post(new ChatEvent(EVENT_CREATE_MESSAGE_SESSION, paramBean));
                               }
                           }, new Consumer<Throwable>() {
                               @Override
                               public void accept(Throwable throwable) throws Exception {
                                   Log.i("Cloud", "JoinGroupResponse failed " + throwable.getMessage());
                               }
                           }
                );
        mCompositeDisposable.add(disposable);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.tv_speak:
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    popupWindow = DialogUtils.showToalkPop(getActivity(), rootView);

                    if (SpeechRecognizerHandler.speechRecognizerHandler.isOpen()) {
                        //关闭讯飞的语音类，防止音频通道占用
                        SpeechRecognizerHandler.speechRecognizerHandler.stopAudioInput();
                    }

                    initRecorder();
                    startRecorder();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (popupWindow != null && popupWindow.isShowing()) {
                        popupWindow.dismiss();
                        stopRecorder();

                        if (!SpeechRecognizerHandler.speechRecognizerHandler.isOpen()) {
                            //打开讯飞的语音类，防止讯飞无法唤醒
                            SpeechRecognizerHandler.speechRecognizerHandler.startAudioInput();
                        }

                        sendVoiceMessage();
                    }
                }

                break;
        }

        return true;
    }

    /**
     * 准备录音
     */
    private void initRecorder() {
        recorder = new MediaRecorder();
        /* 设置音频源*/
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        /* 设置输出格式*/
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        /* 设置音频编码器，注意这里，设置成AAC，不然苹果手机播放不了*/
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
//        try {
//            /* 创建一个临时文件，用来存放录音*/
//            tempFile = File.createTempFile("tempFile", ".WAV", SDPathDir);
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        File file = new File(Constant.CHAT_RECORD_VOICE_PATH);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* 设置录音文件*/
        recorder.setOutputFile(file.getAbsolutePath());
    }

    /**
     * 开始录音
     */
    private void startRecorder() {
        if (!isSDCardExit) {
            return;
        }
        try {
            recorder.prepare();
            recorder.start();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 停止录音
     */
    private void stopRecorder() {
        if (recorder != null) {
            try {
                recorder.stop();
            } catch (Exception e) {
                // TODO 如果当前java状态和jni里面的状态不一致，
                //e.printStackTrace();
                recorder = null;
                recorder = new MediaRecorder();
            }
            recorder.release();
            recorder = null;
        }
    }

    private void sendVoiceMessage() {
        Disposable disposable = Observable.just(new File(Constant.CHAT_RECORD_VOICE_PATH))
                .subscribeOn(Schedulers.io())
                .flatMap(new Function<File, ObservableSource<UploadResponse>>() {
                    @Override
                    public ObservableSource<UploadResponse> apply(File file) throws Exception {
                        return NetHandler.Companion.getInst().uploadVoice(file);
                    }
                })
                .flatMap(new Function<UploadResponse, ObservableSource<BaseResponse<JoinGroupResponse>>>() {
                    @Override
                    public ObservableSource<BaseResponse<JoinGroupResponse>> apply(UploadResponse uploadResponse) throws Exception {
                        if (uploadResponse.isSuccess()) {
                            SessionParamBean paramBean = new SessionParamBean(mGroupId + "");
                            paramBean.setGroupType(isOnlineAsk ? 1 : 2);
                            paramBean.setGroup(mGroupId + "");
                            paramBean.setMsgType("audio");
                            ChatDetailMessage detailMessage = new ChatDetailMessage();
                            detailMessage.setMsgType("audio");
                            detailMessage.setMsgContent(uploadResponse.getResult().getUrl());
                            detailMessage.setIconImg(SharedPreUtils.getInstance(getLauncher()).getValue(SharedPreUtils.USER_AVATAR, ""));
                            detailMessage.setNickname(SharedPreUtils.getInstance(getLauncher()).getValue(SharedPreUtils.USER_REAL_NAME, ""));

                            return NetHandler.Companion.getInst().sendMessage(paramBean, detailMessage);
                        }

                        return Observable.error(new Throwable("上传音频失败！"));
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<BaseResponse<JoinGroupResponse>>() {
                    @Override
                    public void accept(BaseResponse<JoinGroupResponse> joinGroupResponse) throws Exception {
                        Log.i("Cloud", "发送成功！！"+ joinGroupResponse.toString());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.e("Cloud", "发送成功！！"+ throwable.getMessage());
                    }
                });

        mCompositeDisposable.add(disposable);
    }

}


