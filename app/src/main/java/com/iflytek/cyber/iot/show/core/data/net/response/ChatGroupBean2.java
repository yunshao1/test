package com.iflytek.cyber.iot.show.core.data.net.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author cloud.wang
 * @date 2019-07-27
 * @Description: TODO
 */
public class ChatGroupBean2 implements Serializable {

        /**
         * groupId : 4
         * id : 555567aa-1355-4b98-ba5b-400dde8c1e3b
         * name : 高小音
         */

        //用于视频唤醒，发送通知消息
        @SerializedName("id")
        public String notifyId;

        @SerializedName("groupId")
        public int groupId;

        @SerializedName("name")
        public String groupName;

        @SerializedName("img")
        public String img;

}
