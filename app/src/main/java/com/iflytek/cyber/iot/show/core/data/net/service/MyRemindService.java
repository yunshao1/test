package com.iflytek.cyber.iot.show.core.data.net.service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.CloseReminder;
import com.iflytek.cyber.iot.show.core.data.net.response.ReminderResponse;
import com.iflytek.cyber.iot.show.core.utils.Constant;
import com.iflytek.cyber.iot.show.core.utils.DateUtils;
import com.iflytek.cyber.iot.show.core.utils.DialogUtils;
import com.iflytek.cyber.iot.show.core.utils.SharedPreUtils;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MyRemindService extends Service {

    private Handler mHandler = new Handler(Looper.getMainLooper());
    private MyMinderRunable runable = new MyMinderRunable();
    private boolean isRun = true;
    private MediaPlayer mediaPlayer;
    private View view;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;

    }

    @Override
    public void onCreate() {
        super.onCreate();
        isRun = true;
        if (Constant.rmindList == null) {
            load();
            mHandler.postDelayed(runable, 5000);

        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRun = false;
        mHandler.removeCallbacksAndMessages(null);
    }

    private class MyMinderRunable implements Runnable {
        @Override
        public void run() {
            if (isRun) {
                if (Constant.rmindList == null) {
                    load();
                } else {
                    //不同一天则清楚数据
                    String time = SharedPreUtils.getInstance(getApplicationContext()).getValue("time", "");
                    if (time.equals(DateUtils.getStrDate(new Date()))) {

                    } else {
                        Constant.rmindMap.clear();
                        SharedPreUtils.getInstance(getApplicationContext()).saveValue("time", DateUtils.getStrDate(new Date()));

                    }
                    openDilaog(Constant.rmindList);
                }
                mHandler.postDelayed(runable, 5000);
            }

        }
    }

    /**
     * 如果在其删除后立马又请求了接口可能会出现一个提醒弹出多次（如果这个请求不在同一分钟则没事）
     *
     * @param rmindList
     */
    private void openDilaog(List<ReminderResponse.ListBean> rmindList) {
        if (rmindList == null)
            return;
        int size = rmindList.size();
        for (int i = size - 1; i >= 0; i--) {
            ReminderResponse.ListBean listBean = rmindList.get(i);
            if (listBean.isOne()) {//是否只提示一次
                if (DateUtils.isSameDay(new Date(), DateUtils.timeToDate(listBean.getDate()))) {
                    if (listBean.getHour() == DateUtils.getHour(new Date())
                            && listBean.getMinute() == DateUtils.getMinute(new Date())) {
                        String url = "";
                        int type = 0;
                        if (!TextUtils.isEmpty(listBean.getAudioUrl())) {
                            url = listBean.getAudioUrl();
                            type = 1;
                        } else {
                            type = 0;
                            url = listBean.getText();
                        }
                        String id = Constant.rmindMap.get(listBean.getId());
                        if (TextUtils.isEmpty(id)) {
                            playItem(url, type);
                            view = DialogUtils.openMyminderDialog(Constant.activity, listBean, this::playItem);
                            closeRemind(listBean.getId());
                            Constant.rmindMap.put(listBean.getId(), listBean.getId());
                        }
                        rmindList.remove(i);
                    }
                }
            } else {
                if (DateUtils.isHaveTheDay(new Date(), listBean.getWeek())) {
                    if (listBean.getHour() == DateUtils.getHour(new Date())
                            && listBean.getMinute() == DateUtils.getMinute(new Date())) {
                        int type = 0;
                        String url = "";
                        if (!TextUtils.isEmpty(listBean.getAudioUrl())) {
                            url = listBean.getAudioUrl();
                            type = 1;
                        } else {
                            type = 0;
                            url = listBean.getText();
                        }
                        String id = Constant.rmindMap.get(listBean.getId());
                        if (TextUtils.isEmpty(id)) {
                            playItem(url, type);
                            view = DialogUtils.openMyminderDialog(Constant.activity, listBean, this::playItem);
                            Constant.rmindMap.put(listBean.getId(), listBean.getId());
                        }
                        rmindList.remove(i);
                    }

                }


            }


        }
    }

    /**
     * @param url
     * @param type 1 是语音
     */
    private void playItem(String url, int type) {
        if (type == 1) {
            if (view != null) {
                ImageView image = view.findViewById(R.id.image);
                image.setImageResource(R.drawable.ic_my_reminder_pouse);
            }
            if (mediaPlayer != null) {
                try {
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.stop();
                        mediaPlayer.release();
                        mediaPlayer = null;
                    }
                } catch (IllegalStateException e) {
                    mediaPlayer = null;

                }
                try {
                    if (mediaPlayer != null) {
                        mediaPlayer.release();
                        mediaPlayer = null;
                    }
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource(url);
                    mediaPlayer.prepare();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource(url);
                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (mediaPlayer != null) {
                mediaPlayer.start();
                mediaPlayer.setOnCompletionListener(mp -> {

                    if (view != null) {
                        ImageView image = view.findViewById(R.id.image);
                        image.setImageResource(R.drawable.ic_my_reminder_paly);
                    }
                });
            }
        } else {
            playVoice(url);
        }


    }

    /**
     * 关闭提醒一次的
     *
     * @param id
     */
    private void closeRemind(String id) {
        NetHandler.Companion.getInst().closeReminder(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CloseReminder>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(CloseReminder listResponse) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }
    private void playVoice(String speakText) {
        if (!TextUtils.isEmpty(speakText)) {
            NetHandler.Companion.getInst().playVoice(speakText);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        isRun = true;
        return super.onStartCommand(intent, flags, startId);
    }

    public void load() {
        NetHandler.Companion.getInst().getReminder()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ReminderResponse>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(ReminderResponse listResponse) {
                        if (listResponse != null && listResponse.getList() != null) {
                            Constant.rmindList = listResponse.getList();
//                            view = DialogUtils.openMyminderDialog(Constant.activity, listResponse.getList().get(0),null);

                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
