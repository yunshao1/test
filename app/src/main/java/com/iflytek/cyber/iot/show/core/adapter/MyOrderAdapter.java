package com.iflytek.cyber.iot.show.core.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.response.MyOrederBean;
import com.iflytek.cyber.iot.show.core.utils.GlideUtil;

import java.util.List;

public class MyOrderAdapter extends RecyclerView.Adapter<MyOrderAdapter.VHodler> {

    private Context context;
    private List<MyOrederBean.ListBean> list;


    public MyOrderAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<MyOrederBean.ListBean> list) {
        this.list = list;
        notifyDataSetChanged();

    }


    @NonNull
    @Override
    public MyOrderAdapter.VHodler onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        MyOrderAdapter.VHodler vHodler = null;
        vHodler = new MyOrderAdapter.VHodler(LayoutInflater.from(context).inflate(R.layout.item_fragment_my_order, viewGroup, false));
        return vHodler;
    }

    @Override
    public void onBindViewHolder(@NonNull MyOrderAdapter.VHodler vHodler, final int position) {
        vHodler.tv_name.setText(list.get(position).getElderName());
        vHodler.tv_subtitle.setText(list.get(position).getTimeTxt());
        vHodler.tv_title.setText(list.get(position).getSaleName());
        if ("WaitExecute".equals(list.get(position).getStatusDisplay())) {
            vHodler.defult.setImageResource(R.drawable.ic_view1);
        } else if ("WaitEvaluate".equals(list.get(position).getStatusShow())) {
            vHodler.defult.setImageResource(R.drawable.ic_view2);
        } else if ("WaitAccept".equals(list.get(position).getStatusShow())) {
            vHodler.defult.setImageResource(R.drawable.ic_view3);
        } else {
            vHodler.defult.setImageResource(R.drawable.ic_view4);
        }
        //
        GlideUtil.loadImage(context, list.get(position).getUserImg(), 0, vHodler.iv_head);
        GlideUtil.loadImage(context, list.get(position).getSalePicture(), R.drawable.icon_error_image, vHodler.iv_cover);


    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    public class VHodler extends RecyclerView.ViewHolder {
        public TextView tv_title;
        public ImageView iv_cover;
        public ImageView iv_head;
        public TextView tv_subtitle;
        public TextView tv_name;
        public ImageView defult;

        public VHodler(@NonNull View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            iv_cover = itemView.findViewById(R.id.iv_cover);
            iv_head = itemView.findViewById(R.id.iv_head);
            tv_subtitle = itemView.findViewById(R.id.tv_subtitle);
            tv_name = itemView.findViewById(R.id.tv_name);
            defult = itemView.findViewById(R.id.defult);
        }
    }

}
