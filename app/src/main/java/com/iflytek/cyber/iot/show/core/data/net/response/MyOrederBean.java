package com.iflytek.cyber.iot.show.core.data.net.response;

import java.util.List;

public class MyOrederBean {


    private String message;
    private String tryTxt;
    private String readTxt;
    private List<ListBean> list;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTryTxt() {
        return tryTxt;
    }

    public void setTryTxt(String tryTxt) {
        this.tryTxt = tryTxt;
    }

    public String getReadTxt() {
        return readTxt;
    }

    public void setReadTxt(String readTxt) {
        this.readTxt = readTxt;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {

        private String id;
        private String orderCode;
        private String elderNo;
        private String elderName;
        private int orderAmount;
        private int payableAmount;
        private int paymentAmount;
        private String userName;
        private String status;
        private String createDate;
        private String statusShow;
        private String saleName;
        private String salePicture;
        private String timeTxt;
        private String userImg;
        private String statusDisplay;
        private List<?> list;

        public String getStatusDisplay() {
            return statusDisplay;
        }

        public void setStatusDisplay(String statusDisplay) {
            this.statusDisplay = statusDisplay;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOrderCode() {
            return orderCode;
        }

        public void setOrderCode(String orderCode) {
            this.orderCode = orderCode;
        }

        public String getElderNo() {
            return elderNo;
        }

        public void setElderNo(String elderNo) {
            this.elderNo = elderNo;
        }

        public String getElderName() {
            return elderName;
        }

        public void setElderName(String elderName) {
            this.elderName = elderName;
        }

        public int getOrderAmount() {
            return orderAmount;
        }

        public void setOrderAmount(int orderAmount) {
            this.orderAmount = orderAmount;
        }

        public int getPayableAmount() {
            return payableAmount;
        }

        public void setPayableAmount(int payableAmount) {
            this.payableAmount = payableAmount;
        }

        public int getPaymentAmount() {
            return paymentAmount;
        }

        public void setPaymentAmount(int paymentAmount) {
            this.paymentAmount = paymentAmount;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getStatusShow() {
            return statusShow;
        }

        public void setStatusShow(String statusShow) {
            this.statusShow = statusShow;
        }

        public String getSaleName() {
            return saleName;
        }

        public void setSaleName(String saleName) {
            this.saleName = saleName;
        }

        public String getSalePicture() {
            return salePicture;
        }

        public void setSalePicture(String salePicture) {
            this.salePicture = salePicture;
        }

        public String getTimeTxt() {
            return timeTxt;
        }

        public void setTimeTxt(String timeTxt) {
            this.timeTxt = timeTxt;
        }

        public String getUserImg() {
            return userImg;
        }

        public void setUserImg(String userImg) {
            this.userImg = userImg;
        }

        public List<?> getList() {
            return list;
        }

        public void setList(List<?> list) {
            this.list = list;
        }
    }
}
