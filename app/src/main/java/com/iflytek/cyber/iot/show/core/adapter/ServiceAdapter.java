package com.iflytek.cyber.iot.show.core.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.response.ServiceNewBean;
import com.iflytek.cyber.iot.show.core.utils.GlideUtil;

import java.util.ArrayList;
import java.util.List;

public class ServiceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<ServiceNewBean.DataBean.PanelBean.PlugBean> list = new ArrayList<>();
    private int type = 0;//0、1、2、3、4、5、6、7


    public ServiceAdapter(Context context) {
        this.context = context;
    }

    public int getType() {
        return type;
    }

    public ServiceAdapter setType(int type) {
        this.type = type;
        return this;
    }

    public void setData(List<ServiceNewBean.DataBean.PanelBean.PlugBean> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();

    }

    public List<ServiceNewBean.DataBean.PanelBean.PlugBean> getList() {
        return list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        RecyclerView.ViewHolder vHodler = null;
        View view = LayoutInflater.from(context).inflate(R.layout.item_service, viewGroup, false);
        vHodler = new ServiceAdapter.VHodler(view);
        return vHodler;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vHodler, final int position) {
        ((VHodler) vHodler).tv_name.setText(list.get(position).getName());
        GlideUtil.loadImage(context, list.get(position).getImg(), R.drawable.shape_grad_8px_color_4bccf6, ((VHodler) vHodler).image);
        ((VHodler) vHodler).ll_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(position);
                }
            }
        });


    }

    @Override
    public int getItemViewType(int position) {
        return type;
    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    public class VHodler extends RecyclerView.ViewHolder {
        public RelativeLayout ll_parent;
        public ImageView image;
        public TextView tv_name;

        public VHodler(@NonNull View itemView) {
            super(itemView);
            ll_parent = itemView.findViewById(R.id.ll_parent);
            image = itemView.findViewById(R.id.image);
            tv_name = itemView.findViewById(R.id.tv_name);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }

    public OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

}
