package com.iflytek.cyber.iot.show.core.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * 缓存类
 * @author lzj
 *
 */
public class SharedPreUtils {

	public final static String USER_TOKEN = "token";
	public final static String USER_ID = "id";
	public final static String USER_REAL_NAME = "user_real_name";
	public final static String USER_AVATAR = "user_avatar";
	public final static String LOGIN_NAME = "LOGIN_NAME";
	public final static String LOGIN_PWD = "LOGIN_PWD";

	public final static String CACHE = "user_info.sp";

	public final static String USER_SIG = "userSig";


	private SharedPreferences sp;

	static private SharedPreUtils instance;

	static public SharedPreUtils getInstance(Context context) {
		if (instance == null)
			instance = new SharedPreUtils(context);
		return instance;
	}

	private SharedPreUtils(Context context) {
		sp = context.getSharedPreferences(CACHE, Context.MODE_PRIVATE);
	}
	
	/**
	 * SharedPreferences通过key取值
	 * @param key
	 * @param dfValue 默认值
	 * @return
	 */
	public String getValue(String key, String dfValue){
		String value = sp.getString(key, dfValue);
		if ("null".equals(value)) {
			value = dfValue;
		}
		return value;
	}

	/**
	 * SharedPreferences通过key存value
	 * @param key
	 * @param value
	 */
	public void saveValue(String key, String value){
		Editor editor = sp.edit();
		editor.putString(key, value);
		editor.commit();
	}

	/**
	 * SharedPreferences通过key取值
	 * @param key
	 * @param dfValue 默认值
	 * @return
	 */
	public boolean getValue(String key, boolean dfValue){
		boolean value = sp.getBoolean(key, dfValue);
		return value;
	}

	/**
	 * SharedPreferences通过key存value
	 * @param key
	 * @param value
	 */
	public void saveValue(String key, boolean value){
		Editor editor = sp.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}
	/**
	 * 清空所有数据
	 */
	public void clearAllData(){
		Editor editor = sp.edit();
		editor.clear();
		editor.commit();
	}
}
