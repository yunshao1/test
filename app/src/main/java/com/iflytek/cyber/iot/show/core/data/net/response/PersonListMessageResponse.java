package com.iflytek.cyber.iot.show.core.data.net.response;

import java.util.List;

public class PersonListMessageResponse {

    private String message;
    private ListBean list;
    private String tryTxt;
    private String readTxt;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ListBean getList() {
        return list;
    }

    public void setList(ListBean list) {
        this.list = list;
    }

    public String getTryTxt() {
        return tryTxt;
    }

    public void setTryTxt(String tryTxt) {
        this.tryTxt = tryTxt;
    }

    public String getReadTxt() {
        return readTxt;
    }

    public void setReadTxt(String readTxt) {
        this.readTxt = readTxt;
    }

    public static class ListBean {
        private List<FamilyMessagesBean> familyMessages;

        public List<FamilyMessagesBean> getFamilyMessages() {
            return familyMessages;
        }

        public void setFamilyMessages(List<FamilyMessagesBean> familyMessages) {
            this.familyMessages = familyMessages;
        }

        public static class FamilyMessagesBean {

            private String name;
            private String img;
            private String nickName;
            private String message;
            private int unreadCount;
            private boolean isAudio;
            private String accountId;

            public String getAccountId() {
                return accountId;
            }

            public void setAccountId(String accountId) {
                this.accountId = accountId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public String getNickName() {
                return nickName;
            }

            public void setNickName(String nickName) {
                this.nickName = nickName;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public int getUnreadCount() {
                return unreadCount;
            }

            public void setUnreadCount(int unreadCount) {
                this.unreadCount = unreadCount;
            }

            public boolean isIsAudio() {
                return isAudio;
            }

            public void setIsAudio(boolean isAudio) {
                this.isAudio = isAudio;
            }
        }
    }
}
