package com.iflytek.cyber.iot.show.core.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.response.ClassifyBean;

import java.util.List;

public class PharmacyAdapter extends RecyclerView.Adapter<PharmacyAdapter.VHodler> {

    private Context context;
    private List<ClassifyBean.ListBeanX> list;


    public PharmacyAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<ClassifyBean.ListBeanX> list) {
        this.list = list;
        notifyDataSetChanged();


    }

    public List<ClassifyBean.ListBeanX> getList() {
        return list;
    }

    @NonNull
    @Override
    public PharmacyAdapter.VHodler onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        PharmacyAdapter.VHodler vHodler = null;
        vHodler = new PharmacyAdapter.VHodler(LayoutInflater.from(context).inflate(R.layout.item_fragment_pharmacy, viewGroup, false));
        return vHodler;
    }

    @Override
    public void onBindViewHolder(@NonNull PharmacyAdapter.VHodler vHodler, final int position) {
        vHodler.tv_name.setText(list.get(position).getName());
        if (list.get(position).getList() != null) {
            int size = list.get(position).getList().size();
            int spanCount;
            if (size > 0) {
                if (size % 2 == 0) {
                    spanCount = size / 2;
                } else {
                    spanCount = size / 2 + 1;
                }
            vHodler.recly_view.setLayoutManager(new GridLayoutManager(context, spanCount));
                PharmacySecondAdapter adapter = new PharmacySecondAdapter(context, list.get(position).getList());
            adapter.setOnItemClickListener(new PharmacySecondAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(ClassifyBean.ListBeanX.ListBean bean) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(bean);
                    }
                }
            });
            vHodler.recly_view.setAdapter(adapter);
            vHodler.recly_view.setNestedScrollingEnabled(false);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    public class VHodler extends RecyclerView.ViewHolder {
        public TextView tv_name;
        public RecyclerView recly_view;

        public VHodler(@NonNull View itemView) {
            super(itemView);
            recly_view = itemView.findViewById(R.id.recycler);
            tv_name = itemView.findViewById(R.id.tv_name);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(ClassifyBean.ListBeanX.ListBean bean);
    }

    public OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

}
