package com.iflytek.cyber.iot.show.core.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.adapter.HomeRepairAdapter;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.ActionResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.RepairResponse;
import com.iflytek.cyber.iot.show.core.utils.DateUtils;
import com.iflytek.cyber.iot.show.core.utils.NavigateUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import androidx.navigation.fragment.NavHostFragment;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * 居家维修
 */
public class HomeRepairFragment extends BaseFragment {
    LinearLayout ll_finsh;
    private RecyclerView recyclerView;
    private HomeRepairAdapter adapter;

    private View view;
    private String code = "";
    private String title = "";
    private String apiUrl;
    private String serviceId="";

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            view = inflater.inflate(R.layout.fragment_home_repair, container, false);
            initView();
        return view;
    }

    private void load() {
        Map<String, String> map = new HashMap<>();
        map.put("categoryCode", code);
        map.put("pageSize", "20");
        map.put("pageNo", "1");
        map.put("keyword", "");
        NetHandler.Companion.getInst().getService4(apiUrl, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RepairResponse>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(RepairResponse listResponse) {
                        if (listResponse != null) {
                            setData(listResponse);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void setData(RepairResponse listResponse) {
        if (listResponse == null || listResponse.getList() == null)
            return;
        serviceId = listResponse.getList().get(0).getId()+"";
        int spanCount = listResponse.getList().size() / 2;
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), spanCount));
        adapter.setData(listResponse.getList());
        if (listResponse.getTryTxt() != null) {
            setHideBottomBar(true);
            setTipText(listResponse.getTryTxt(), R.style.TextBlackFontAndBg);
        }
    }


    private void initView() {
        code = getArguments().getString("code");
        title = getArguments().getString("title");
        apiUrl = getArguments().getString("apiUrl");
        ll_finsh = view.findViewById(R.id.ll_finsh);
        recyclerView = view.findViewById(R.id.recycler);
        adapter = new HomeRepairAdapter(getActivity());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(adapter);
        load();
        adapter.setOnItemClickListener(new HomeRepairAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String title,String id) {
                serviceId = id;
                submitAction("");
                Bundle bundle = new Bundle();
                bundle.putString("title", title);
                bundle.putString("content", "好的，预约成功\n稍后会有师傅联系您的");
                NavHostFragment.findNavController(HomeRepairFragment.this).navigate(R.id.action_to_apply_fragment, bundle);
            }
        });
        ll_finsh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(HomeRepairFragment.this).navigateUp();

            }
        });
    }

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    public void submitAction(final String input) {
        Map<String,String>map = new ArrayMap<>();
        map.put("serviceId",serviceId);
        map.put("typeCode",code);
        map.put("goodProperty","");//配餐传早餐中餐午餐
        map.put("date",DateUtils.getStrDate(new Date()));
        map.put("content",getArguments().getString("title"));
        map.put("actionName","下单");
        mCompositeDisposable.add(NetHandler.Companion.getInst().submitAction2(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ActionResponse>() {
                    @Override
                    public void accept(ActionResponse actionResponse) throws Exception {
                        if ("success".equals(actionResponse.getMessage())) {
                            NavigateUtil.INSTANCE.playVoice("下单成功");
                            if (!TextUtils.isEmpty(input)) {
                                Bundle bundle = new Bundle();
                                bundle.putString("title", input);
                                bundle.putString("content", "好的，下单成功");
                                NavHostFragment.findNavController(HomeRepairFragment.this).navigate(R.id.apply_fragment, bundle);
                            }
                        } else {
                            Log.i("Cloud", actionResponse.getMessage());
//                            Toast.makeText(getLauncher(), "报名失败，"+ actionResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.i("Cloud", "onError::"+ throwable.getMessage());
//                        Toast.makeText(getLauncher(), "网络请求失败，导致报名失败，请重新"+ throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }));
    }

}
