package com.iflytek.cyber.iot.show.core.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.ServiceBean;

import androidx.navigation.fragment.NavHostFragment;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * 答复页面
 */
public class ReplyFrament extends BaseFragment {
    LinearLayout ll_finsh;

    private View view;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_replay, container, false);
            initView();
            load();
        }

        return view;
    }

    private void load() {

        NetHandler.Companion.getInst().getService("7", "", 10, 1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ServiceBean>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(ServiceBean listResponse) {
                        if (listResponse != null) {
                            setData(listResponse);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void setData(ServiceBean listResponse) {
        if (listResponse == null || listResponse.getResult() == null)
            return;

        if (listResponse.getTryTxt() != null) {
//            setTipText(listResponse.getTryTxt().get(0), R.style.TextWhiteFont);
        }
    }


    private void initView() {
        ll_finsh = view.findViewById(R.id.ll_finsh);
        ll_finsh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(ReplyFrament.this).navigateUp();

            }
        });
    }

}
