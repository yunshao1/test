package com.iflytek.cyber.iot.show.core.data.net.response;

import java.util.List;

public class ReminderResponse {


    private String message;
    private String tryTxt;
    private String readTxt;
    private List<ListBean> list;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTryTxt() {
        return tryTxt;
    }

    public void setTryTxt(String tryTxt) {
        this.tryTxt = tryTxt;
    }

    public String getReadTxt() {
        return readTxt;
    }

    public void setReadTxt(String readTxt) {
        this.readTxt = readTxt;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {

        private String id;
        private String text;
        private String audioUrl;
        private int audioSeconds;
        private int hour;
        private int minute;
        private String bellType;
        private String type;
        private String createTime;
        private String week;
        private String date;
        private boolean isPlaying;
        private boolean isOne;//是否只提示一次
        private boolean isSend;

        public boolean isSend() {
            return isSend;
        }

        public void setSend(boolean send) {
            isSend = send;
        }

        public boolean isPlaying() {
            return isPlaying;
        }

        public boolean IsPlaying() {
            return isPlaying;
        }

        public void setIsPlaying(boolean isPlaying) {
            this.isPlaying = isPlaying;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public boolean isOne() {
            return isOne;
        }

        public void setOne(boolean one) {
            isOne = one;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getAudioUrl() {
            return audioUrl;
        }

        public void setAudioUrl(String audioUrl) {
            this.audioUrl = audioUrl;
        }

        public int getAudioSeconds() {
            return audioSeconds;
        }

        public void setAudioSeconds(int audioSeconds) {
            this.audioSeconds = audioSeconds;
        }

        public int getHour() {
            return hour;
        }

        public void setHour(int hour) {
            this.hour = hour;
        }

        public int getMinute() {
            return minute;
        }

        public void setMinute(int minute) {
            this.minute = minute;
        }

        public String getBellType() {
            return bellType;
        }

        public void setBellType(String bellType) {
            this.bellType = bellType;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getWeek() {
            return week;
        }

        public void setWeek(String week) {
            this.week = week;
        }
    }
}
