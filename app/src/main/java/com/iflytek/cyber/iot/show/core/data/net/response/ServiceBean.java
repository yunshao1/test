package com.iflytek.cyber.iot.show.core.data.net.response;

import java.util.List;

public class ServiceBean {


    private String message;
    private String tryTxt;
    private List<ResultBean> list;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTryTxt() {
        return tryTxt;
    }

    public void setTryTxt(String tryTxt) {
        this.tryTxt = tryTxt;
    }

    public List<ResultBean> getResult() {
        return list;
    }

    public void setResult(List<ResultBean> result) {
        this.list = result;
    }

    public static class ResultBean {
        /**
         * tryTxt : ["试试，\u201c小乐，帮我预定这个服务\u201d"]
         * id : Diet_2
         * title : 虾仁小馄饨套餐
         * content : null
         * imageUrl : null
         * price : ￥15
         * serviceType : 健康膳食
         * serviceTypeId : 9
         * readTxt : null
         */

        private String id;
        private String name;
        private String content;
        private String picture;
        private String price;
        private String serviceType;
        private String serviceTypeId;
        private String readTxt = "";
        private List<String> tryTxt;
        private String sellerName;
        private String intro;

        public String getIntro() {
            return intro;
        }

        public ResultBean setIntro(String intro) {
            this.intro = intro;
            return this;
        }

        public String getSellerName() {
            return sellerName;
        }

        public void setSellerName(String sellerName) {
            this.sellerName = sellerName;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return name;
        }

        public void setTitle(String title) {
            this.name = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getImageUrl() {
            return picture;
        }

        public void setImageUrl(String imageUrl) {
            this.picture = imageUrl;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getServiceType() {
            return serviceType;
        }

        public void setServiceType(String serviceType) {
            this.serviceType = serviceType;
        }

        public String getServiceTypeId() {
            return serviceTypeId;
        }

        public void setServiceTypeId(String serviceTypeId) {
            this.serviceTypeId = serviceTypeId;
        }

        public String getReadTxt() {
            return readTxt;
        }

        public void setReadTxt(String readTxt) {
            this.readTxt = readTxt;
        }

        public List<String> getTryTxt() {
            return tryTxt;
        }

        public void setTryTxt(List<String> tryTxt) {
            this.tryTxt = tryTxt;
        }
    }
}
