package com.iflytek.cyber.iot.show.core.data.net;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.iflytek.cyber.iot.show.core.data.local.LoginSession;
import com.iflytek.cyber.iot.show.core.data.net.response.LoginBean;
import com.iflytek.cyber.iot.show.core.utils.AppUtil;
import com.iflytek.cyber.iot.show.core.utils.SharedPreUtils;
import com.iflytek.cyber.iot.show.core.utils.scheduler.TaskScheduler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class HeadInterceptor implements Interceptor {
    @NonNull
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        String token = LoginSession.INSTANCE.getSUserToken();
        if (TextUtils.isEmpty(token)) {
            token = SharedPreUtils.getInstance(AppUtil.getApp())
                    .getValue(SharedPreUtils.USER_TOKEN, "");
        }
        Request originalRequest = chain.request();
        Request authorised = originalRequest.newBuilder()
                .addHeader("authorization", "Bearer " + token)
                .build();

        Log.e("OkHttp", "Token== " + LoginSession.INSTANCE.getSUserToken());

        Response originalResponse = chain.proceed(authorised);
        //可以对response进行解析，并重定向比如再次请求

        if (originalResponse.code() == 401) {//如果是401，说明Token过期
            ResponseBody responseBody = originalResponse.body();
            if (responseBody != null) {
                responseBody.close();//关闭 释放原有的资源
            }

            Request loginRequest = loginRequest();  //获取登陆的Request
            Response loginResponse = chain.proceed(loginRequest);//执行登陆，获取新的SessionId
            if (loginResponse.isSuccessful()) {//登陆成功
                ResponseBody body = loginResponse.body();
                if (body != null) {
                    String responseStr = body.string();
                    try {
                        LoginBean response = new Gson().fromJson(responseStr, LoginBean.class);
                        if (response.getList() != null && response.getList().getToken() != null) {
                            SharedPreUtils instance = SharedPreUtils.getInstance(AppUtil.getApp());
                            instance.saveValue(SharedPreUtils.USER_TOKEN, response.getList().getToken());
                            instance.saveValue(SharedPreUtils.USER_ID, response.getList().getId());
                            instance.saveValue(SharedPreUtils.USER_REAL_NAME, response.getList().getRealName());
                            instance.saveValue(SharedPreUtils.USER_AVATAR, response.getList().getImg());
                            instance.saveValue(SharedPreUtils.USER_SIG, response.getList().getUserSig());

                            LoginSession.INSTANCE.setSUserId(response.getList().getId());
                            LoginSession.INSTANCE.setSUserToken(response.getList().getToken());
                            LoginSession.INSTANCE.setSUserSig(response.getList().getUserSig());
                            LoginSession.INSTANCE.setSUserAvatar(response.getList().getImg());
                            LoginSession.INSTANCE.setSUserName(response.getList().getRealName());

                            Request newRequest = originalRequest.newBuilder()
                                    .addHeader("authorization", "Bearer " + LoginSession.INSTANCE.getSUserToken())
                                    .build();

                            return chain.proceed(newRequest);//重新执行
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        body.close();//释放登陆成功的资源
                    }
                }
            }

//            TaskScheduler.runOnUIThread(new Runnable() {
//                @Override
//                public void run() {
//                    LoginActivity.start(AppUtil.getApp());
//                }
//            });
            //跳转到LauncherFragment
        }

        return originalResponse;
    }

    private Request loginRequest() {
        //解析参数
        Map<String, String> params = new HashMap<>();
        params.put("userName", SharedPreUtils.getInstance(AppUtil.getApp()).getValue(SharedPreUtils.LOGIN_NAME, ""));
        params.put("pwd", SharedPreUtils.getInstance(AppUtil.getApp()).getValue(SharedPreUtils.LOGIN_PWD, ""));
        params.put("appCode", "VoiceBox");

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), new Gson().toJson(params));

        Log.e("OkHttp", "重新登录中...");

        return new Request.Builder()
                .url(NetConfig.API_CHAT_BASE_URL + "/api/Account/Login")
                .method("POST", body).build();
    }
}
