package com.iflytek.cyber.iot.show.core.data.local

/**
 * @author cloud.wang
 * @date 2019-08-03
 * @Description: TODO
 */
object LoginSession {

    var sUserId = ""
    var sUserToken = ""
    var sUserName = ""
    var sUserAvatar = ""
    var sUserSig = ""

}