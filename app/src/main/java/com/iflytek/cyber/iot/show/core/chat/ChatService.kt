package com.iflytek.cyber.iot.show.core.chat

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.iflytek.cyber.iot.show.core.chat.event.ChatEvent
import com.iflytek.cyber.iot.show.core.chat.event.ChatEvent.Companion.EVENT_DISCONNECTION
import com.iflytek.cyber.iot.show.core.chat.event.ChatEvent.Companion.EVENT_FINISH_CONNECTION
import com.iflytek.cyber.iot.show.core.chat.event.ChatEvent.Companion.EVENT_MESSAGE
import com.iflytek.cyber.iot.show.core.chat.event.ChatEvent.Companion.EVENT_NOTIFY
import com.iflytek.cyber.iot.show.core.data.net.NetConfig
import com.iflytek.cyber.iot.show.core.utils.SharedPreUtils
import com.iflytek.cyber.iot.show.core.utils.ToastUtil
import com.iflytek.cyber.iot.show.core.utils.scheduler.TaskScheduler
import com.microsoft.signalr.HubConnection
import com.microsoft.signalr.HubConnectionBuilder
import com.microsoft.signalr.HubConnectionState
import io.reactivex.Single
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * @author cloud.wang
 * @date 2019-07-13
 * @Description: TODO
 */
class ChatService: Service() {

    private var mConnection: HubConnection? = null

    companion object {
        private const val TAG = "ChatService"
    }

    override fun onCreate() {
        super.onCreate()

        EventBus.getDefault().register(this)
        //可用本地广播的形式，和其他组建通信
//        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
    }

    override fun onBind(intent: Intent): IBinder? {
        return binder
    }

    private val binder = EngineBinder()

    open inner class EngineBinder : Binder() {
        fun getService(): ChatService {
            return this@ChatService
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.i(TAG, "onStartCommand: 启动Service $flags startId: $startId")

        TaskScheduler.execute {
            startConnection()
        }

        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        super.onDestroy()

        Log.i(TAG, "stopConnection: 停止Service")
        mConnection?.stop()
        EventBus.getDefault().unregister(this)
    }

    private fun startConnection() {
        Log.i(TAG, "startConnection: 启动Service")
        val token = SharedPreUtils.getInstance(applicationContext).getValue(SharedPreUtils.USER_TOKEN, "")
        if (token.isNullOrEmpty()) {
            TaskScheduler.runOnUIThread {
                ToastUtil.showLongToastCenter("Token为空，请退出重新登录～")
            }

            return
        }

        if (mConnection != null && mConnection?.connectionState == HubConnectionState.CONNECTED) {
            return
        }

        if (mConnection == null) {
            Log.i(TAG, "startConnection: 初始化Connection")
            mConnection = HubConnectionBuilder.create("${NetConfig.API_CHAT_SERVER_URL}?access_token=$token")
                    .withAccessTokenProvider( Single.defer {
                        Single.just(token)
                    }).build()
        }

        mConnection?.on("OnGroupMessages", { message -> parseMessageResponse(message) }, ChatResponse::class.java)
        mConnection?.on("OnUserMessages", { message -> parseNotifyResponse(message) }, ChatResponse::class.java)
        mConnection?.onClosed { exception ->
//            Toast.makeText(MyApplication.APP, "SingleR服务器断开连接：${exception.message}", Toast.LENGTH_LONG).show()
            EventBus.getDefault().post(ChatEvent(EVENT_DISCONNECTION, null))
        }

        mConnection?.start()?.blockingAwait()

        if (mConnection?.connectionState == HubConnectionState.CONNECTED) {
            Log.i(TAG, "startConnection: 启动成功-----${mConnection?.keepAliveInterval}")
            EventBus.getDefault().postSticky(ChatEvent(EVENT_FINISH_CONNECTION, null))
        } else {
            Log.i(TAG, "startConnection: 启动失败-----")
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun handleEvent(chatEvent: ChatEvent) {
        if (chatEvent.messageType == ChatEvent.EVENT_CREATE_MESSAGE_SESSION) {
            if (chatEvent.messageContent is SessionParamBean) {
                Log.i(TAG, "EVENT_CREATE_MESSAGE_SESSION  ${chatEvent.messageContent}")
                createMessageSession(chatEvent.messageContent.groupId,
                        chatEvent.messageContent.appKey, chatEvent.messageContent.groupType)
            }
        } else if (chatEvent.messageType == ChatEvent.EVENT_CREATE_NOTIFY_SESSION) {
            if (chatEvent.messageContent is SessionParamBean) {
                Log.i(TAG, "EVENT_CREATE_NOTIFY_SESSION  ${chatEvent.messageContent}")
                createListenerSession(chatEvent.messageContent.groupId,
                        chatEvent.messageContent.appKey, chatEvent.messageContent.groupType)
            }
        }
    }

    /**
     * 创建一个聊天会话
     * @param group
     * @param appKey  写死：Elderly.SOS
     * @param groupType 写死：2
     */
    private fun createMessageSession(group: String, appKey: String, groupType: Int) {
        if (mConnection?.connectionState != HubConnectionState.CONNECTED) {
            startConnection()
        }

        mConnection?.send("GetGroupMessages", group, appKey, groupType)
    }

    /**
     * 创建一个监听会话
     * @param group
     * @param appKey  写死：Elderly.SOS
     * @param groupType 写死：2
     */
    private fun createListenerSession(group: String, appKey: String, groupType: Int) {
        if (mConnection?.connectionState != HubConnectionState.CONNECTED) {
            startConnection()
        }

        mConnection?.send("GetUserMessage")
    }

    fun getHubConnection(): HubConnection? {
        return mConnection
    }

    private fun parseMessageResponse(response: ChatResponse) {
        if (response.message == null || response.message.size < 1) {
            Log.i(TAG, "New Message: ${response.message}")
            return
        }

        for (msg in response.message) {
            Log.i(TAG, "New Message: $msg")
        }

        EventBus.getDefault().post(ChatEvent(EVENT_MESSAGE, response))
    }

    private fun parseNotifyResponse(response: ChatResponse) {
        if (response.message == null || response.message.size < 1) {
            Log.i(TAG, "New Notify Message: ${response.message}")
            return
        }

        for (msg in response.message) {
            Log.i(TAG, "New Notify Message: $msg")
        }

        EventBus.getDefault().post(ChatEvent(EVENT_NOTIFY, response))
    }
}