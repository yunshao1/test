package com.iflytek.cyber.iot.show.core.data.net.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Response<T> implements Serializable {

    @SerializedName("message")
    public String message;

    @SerializedName("list")
    public T list;

    @SerializedName("tryTxt")
    public String tryTxt;

    @Override
    public String toString() {
        return "Response{" +
                "message='" + message + '\'' +
                ", list=" + list +
                ", tryTxt=" + tryTxt +
                '}';
    }
}
