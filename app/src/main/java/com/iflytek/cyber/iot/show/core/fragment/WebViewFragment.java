package com.iflytek.cyber.iot.show.core.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;

import androidx.navigation.fragment.NavHostFragment;

public class WebViewFragment extends BaseFragment {

    private View view;
    private TextView tv_web_name;
    LinearLayout ll_finsh;
    private String url = "";
    private String title = "";

    WebView web_view;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_webview, container, false);
            initView();
            initWeb();
        }

        return view;
    }

    private void initView() {
        url = getArguments().getString("url");
        title = getArguments().getString("title");
        tv_web_name = view.findViewById(R.id.tv_web_name);
        ll_finsh = view.findViewById(R.id.ll_finsh);
        web_view = view.findViewById(R.id.web_view);
        tv_web_name.setText(title);
        ll_finsh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(WebViewFragment.this).navigateUp();

            }
        });
    }

    @JavascriptInterface
    private void initWeb() {
        web_view.getSettings().setDatabaseEnabled(true);
        web_view.getSettings().setDomStorageEnabled(true);
        web_view.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        WebSettings mWebSettings = web_view.getSettings();

        mWebSettings.setGeolocationEnabled(true);
        mWebSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        mWebSettings.setDatabaseEnabled(true);
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setUseWideViewPort(true);
        mWebSettings.setLoadWithOverviewMode(true);
        mWebSettings.setDomStorageEnabled(true);
        mWebSettings.setAllowFileAccess(true);
        mWebSettings.setSupportZoom(false);
        mWebSettings.setAllowContentAccess(true);

        //支持屏幕缩放
//		mWebSettings.setSupportZoom(true);
//		mWebSettings.setBuiltInZoomControls(true);

//设置可以弹出web页面的弹框
        web_view.setWebChromeClient(new WebChromeClient());
        //设置支持Javascript。
        web_view.getSettings().setJavaScriptEnabled(true);
        //开启js跳转方法
        //加载需要显示的网页
        web_view.setVisibility(View.VISIBLE);
        web_view.loadUrl(url);
        web_view.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //view.loadUrl(url); //使用此句代码会出现重定向后第一次无法返回主页的问题
                return false;
//	            2、返回: return true; webview处理url是根据程序来执行的。
//	            3、返回: return false; webview处理url是在webview内部执行。
            }

            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                if (errorCode == -2 || errorCode == -8
                        || errorCode == -11 || errorCode == -6) {
                }
                super.onReceivedError(view, errorCode, description, failingUrl);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
            }


            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (web_view != null) {
            web_view.onPause();
            web_view.destroy();
            web_view = null;
        }
    }
}
