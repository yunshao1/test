package com.iflytek.cyber.iot.show.core.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.navigation.fragment.NavHostFragment;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.adapter.RelativesMessagePersonListAdapter;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.PersonListMessageResponse;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * 亲属留言列表
 */
public class RelativesMessagePersonListFragment extends BaseFragment {
    LinearLayout ll_finsh;
    private RecyclerView recyclerView;
    private RelativesMessagePersonListAdapter adapter;

    private TextView tv_title;

    private View view;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            view = inflater.inflate(R.layout.fragment_list_persion_message, container, false);
            initView();
        return view;
    }

    @Override
    public void onActivityCreated(@org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void load() {
        NetHandler.Companion.getInst().getFamilyMessages()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PersonListMessageResponse>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(PersonListMessageResponse listResponse) {
                        if (listResponse != null) {
                            setData(listResponse);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    private void setData(PersonListMessageResponse listResponse) {
        if (listResponse == null || listResponse.getList() == null) {
            return;
        }
        if (listResponse.getTryTxt() != null) {
            setHideBottomBar(true);
            setTipText(listResponse.getTryTxt(), R.style.TextBlackFontAndBg);
        }
        adapter.setDataList(listResponse.getList().getFamilyMessages());
    }


    private void initView() {
        ll_finsh = view.findViewById(R.id.ll_finsh);
        tv_title = view.findViewById(R.id.tv_title);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        adapter = new RelativesMessagePersonListAdapter(getActivity());
        recyclerView.setAdapter(adapter);
        load();
        adapter.setOnItemClickListener(position -> {
            Bundle bundle = new Bundle();
            bundle.putString("toId", adapter.getList().get(position).getAccountId());
            bundle.putString("name", adapter.getList().get(position).getName());
            bundle.putString("img", adapter.getList().get(position).getImg());
            NavHostFragment.findNavController(RelativesMessagePersonListFragment.this).navigate(R.id.action_to_relatives_message_fragment, bundle);

        });
        ll_finsh.setOnClickListener(v -> NavHostFragment.findNavController(RelativesMessagePersonListFragment.this).navigateUp());
    }


}
