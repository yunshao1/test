package com.iflytek.cyber.iot.show.core.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.navigation.fragment.NavHostFragment;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.adapter.PharmacyDetailAdapter;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.ActionResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.PharmacyDetailResponse;
import com.iflytek.cyber.iot.show.core.utils.DateUtils;
import com.iflytek.cyber.iot.show.core.widget.recyclerview.AlphaTransformer;
import com.iflytek.cyber.iot.show.core.widget.recyclerview.FlingRecycleView;
import com.iflytek.cyber.iot.show.core.widget.recyclerview.GalleryLayoutManager;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * 周边药房detail
 */
public class PharmacyDetailFragment extends BaseFragment {

    TextView mBackTv;

    FlingRecycleView mRecycler;

    private View mContentView;
    private PharmacyDetailAdapter mAdapter;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private String type = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mContentView == null) {
            mContentView = inflater.inflate(R.layout.fragment_pharmacy_detail, container, false);
            initView();
        }

        return mContentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void initView() {
        mBackTv = mContentView.findViewById(R.id.tv_back);
        mBackTv.setText(getArguments().getString("title"));
        mBackTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(PharmacyDetailFragment.this).navigateUp();
            }
        });

        mRecycler = mContentView.findViewById(R.id.recycler);
        mRecycler.setFlingAble(false);
        GalleryLayoutManager layoutManager = new GalleryLayoutManager(GalleryLayoutManager.HORIZONTAL);
        layoutManager.attach(mRecycler, 0);
        layoutManager.setOnItemSelectedListener(new GalleryLayoutManager.OnItemSelectedListener() {
            @Override
            public void onItemSelected(RecyclerView recyclerView, View item, int position) {
                Log.i("Cloud", "onItemSelected====" + position);
                mSelectBean = mAdapter.getList().get(position);
                playVoice(mSelectBean.getReadTxt());
            }
        });
        layoutManager.setItemTransformer(new AlphaTransformer());
        mAdapter = new PharmacyDetailAdapter();
        mRecycler.setAdapter(mAdapter);
        load("");

    }

    public void load(final String keyword) {
        type = getArguments().getString("type");
        Map<String, String> map = new HashMap<>();
        map.put("typeCode", type);
        map.put("pageSize", "20");
        map.put("pageNo", "1");
        map.put("keyword", keyword);
        NetHandler.Companion.getInst().getPhDetail(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PharmacyDetailResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mCompositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(PharmacyDetailResponse communityBeans) {
                        if (communityBeans != null && communityBeans.getList() != null) {
                            setData(communityBeans, keyword);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void setTryTxt(String tryTxt) {
        if (TextUtils.isEmpty(tryTxt))
            return;
            setHideBottomBar(true);
        setTipText(tryTxt, R.style.TextWhiteFont);
    }

    private void setData(PharmacyDetailResponse listResponse, String keyword) {
        if (!TextUtils.isEmpty(keyword)) {
            playVoice("以下是你要找的内容");
        }

        if (listResponse != null) {
            setTryTxt(listResponse.getTryTxt());
            mAdapter.updateList(listResponse.getList());
        } else {
            if (!TextUtils.isEmpty(keyword)) {
                playVoice("找不到你要的内容");
            }
        }
    }

    private PharmacyDetailResponse.ListBean mSelectBean;

    public void submitAction(final String input) {
        if (mSelectBean == null) {
            return;
        }
        Map<String,String>map = new ArrayMap<>();
        map.put("serviceId",mSelectBean.getId());
        map.put("typeCode",type);
        map.put("goodProperty","");//配餐传早餐中餐午餐
        map.put("date",DateUtils.getStrDate(new Date()));
        map.put("content",getArguments().getString("title"));
        map.put("actionName","下单");
        mCompositeDisposable.add(NetHandler.Companion.getInst().submitAction2(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ActionResponse>() {
                    @Override
                    public void accept(ActionResponse actionResponse) throws Exception {
                        if ("success".equals(actionResponse.getMessage())) {
                            playVoice("稍后会有医师联系您的");
                            Bundle bundle = new Bundle();
                            bundle.putString("title", input);
                            bundle.putString("content", "好的，稍后会有医师联系您的");
                            NavHostFragment.findNavController(PharmacyDetailFragment.this).navigate(R.id.apply_fragment, bundle);
                        } else {
                            Log.i("Cloud", actionResponse.getMessage());
//                            Toast.makeText(getLauncher(), "报名失败，"+ actionResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.i("Cloud", "onError::"+ throwable.getMessage());
//                        Toast.makeText(getLauncher(), "网络请求失败，导致报名失败，请重新"+ throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    private void playVoice(String speakText) {
        if (!TextUtils.isEmpty(speakText)) {
            NetHandler.Companion.getInst().playVoice(speakText);
        }
    }
}
