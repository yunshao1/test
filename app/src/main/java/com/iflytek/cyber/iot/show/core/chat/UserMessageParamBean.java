package com.iflytek.cyber.iot.show.core.chat;

/**
 * @author cloud.wang
 * @date 2019-07-27
 * @Description: TODO
 */
public class UserMessageParamBean {
//    {
//        "type": 1,
//            "toId": "6190",
//            "toName": "刘已久",
//            "sendTimeSeconds": 0,
//            "msgContent": "string",
//            "msgType": "txt"
//    }

    private int type = 1;

    private String toId;

    private String toName;

    private String sendTimeSeconds;

    private String msgType;

    private String msgContent;

    public UserMessageParamBean() {}

    public UserMessageParamBean(int type, String toId, String toName, String msgType) {
        this.type = type;
        this.toId = toId;
        this.toName = toName;
        this.msgType = msgType;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public String getSendTimeSeconds() {
        return sendTimeSeconds;
    }

    public void setSendTimeSeconds(String sendTimeSeconds) {
        this.sendTimeSeconds = sendTimeSeconds;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    @Override
    public String toString() {
        return "UserMessageParamBean{" +
                "type=" + type +
                ", toId='" + toId + '\'' +
                ", toName=" + toName +
                ", sendTimeSeconds='" + sendTimeSeconds + '\'' +
                ", msgType='" + msgType + '\'' +
                ", msgContent='" + msgContent + '\'' +
                '}';
    }
}
