package com.iflytek.cyber.iot.show.core.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.adapter.NutritiousMealsAdapter;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.ActionResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.NutritiousMealsResonse;
import com.iflytek.cyber.iot.show.core.data.net.response.ServiceBean;
import com.iflytek.cyber.iot.show.core.utils.DateUtils;
import com.iflytek.cyber.iot.show.core.utils.NavigateUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import androidx.navigation.fragment.NavHostFragment;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * 营养配餐
 */
public class NutritiousMealsFragment extends BaseFragment {
    LinearLayout ll_finsh;
    private RecyclerView recyclerView;
    private NutritiousMealsAdapter adapter;

    private View view;
    private String code = "";
    private String title = "";
    private String apiUrl;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_nutritious_meals, container, false);
            initView();
        }

        return view;
    }

    @Override
    public void onActivityCreated(@org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void load(final String keyword) {
        Map<String, String> map = new HashMap<>();
        map.put("categoryCode", code);
        map.put("pageSize", "20");
        map.put("pageNo", "1");
        map.put("keyword", keyword);
        NetHandler.Companion.getInst().getService3(apiUrl,map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NutritiousMealsResonse>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(NutritiousMealsResonse listResponse) {
                        if (listResponse!=null) {
                            setData(listResponse, keyword);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void setData(NutritiousMealsResonse listResponse, String keyword) {
        if (!TextUtils.isEmpty(keyword)) {
            NavigateUtil.INSTANCE.playVoice("以下是你要找的内容");
        }

        if (listResponse.getList() != null && listResponse.getList().size() > 0) {
            if (TextUtils.isEmpty(keyword)) {
                if (listResponse.getTryTxt() != null) {
                    setHideBottomBar(true);
                    setTipText(listResponse.getTryTxt(), R.style.TextWhiteFont);
                }
            }
            adapter.setData(listResponse.getList());
            mSelectBean = listResponse.getList().get(0);
        } else {
            if (!TextUtils.isEmpty(keyword)) {
                NavigateUtil.INSTANCE.playVoice("找不到你要的内容");
            }
        }
    }


    private void initView() {
        code = getArguments().getString("code");
        title = getArguments().getString("title");
        apiUrl = getArguments().getString("apiUrl");
        ll_finsh = view.findViewById(R.id.ll_finsh);
        recyclerView = view.findViewById(R.id.recyclerView);
        adapter = new NutritiousMealsAdapter(getActivity(), getFragmentManager());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        ll_finsh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(NutritiousMealsFragment.this).navigateUp();

            }
        });
        load("");
    }

    private NutritiousMealsResonse.ListBean mSelectBean;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    public void submitAction(final String input) {
        if (mSelectBean == null) {
            return;
        }
        Map<String,String>map = new ArrayMap<>();
        map.put("serviceId",mSelectBean.getId());
        map.put("typeCode",code);
        map.put("goodProperty","");//配餐传早餐中餐午餐
        map.put("date",DateUtils.getStrDate(new Date()));
        map.put("content",getArguments().getString("title"));
        map.put("actionName","下单");
        mCompositeDisposable.add(NetHandler.Companion.getInst().submitAction2(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ActionResponse>() {
                    @Override
                    public void accept(ActionResponse actionResponse) throws Exception {
                        if ("success".equals(actionResponse.getMessage())) {
                            NavigateUtil.INSTANCE.playVoice("下单成功");
                            Bundle bundle = new Bundle();
                            bundle.putString("title", input);
                            bundle.putString("content", "好的，下单成功");
                            NavHostFragment.findNavController(NutritiousMealsFragment.this).navigate(R.id.apply_fragment, bundle);
                        } else {
                            Log.i("Cloud", actionResponse.getMessage());
//                            Toast.makeText(getLauncher(), "报名失败，"+ actionResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.i("Cloud", "onError::"+ throwable.getMessage());
//                        Toast.makeText(getLauncher(), "网络请求失败，导致报名失败，请重新"+ throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }));
    }

}
