package com.iflytek.cyber.iot.show.core.utils;

import android.text.TextUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {
    public static final SimpleDateFormat fullFormatSdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private final static long minute = 60 * 1000;// 1分钟
    private final static long hour = 60 * minute;// 1小时
    private final static long day = 24 * hour;// 1天
    private final static long month = 31 * day;// 月
    private final static long year = 12 * month;// 年

    public static String getTime(String pattern) {
//        "yyyy-MM-dd HH:mm:ss"
        SimpleDateFormat df = new SimpleDateFormat(pattern);//设置日期格式
        return df.format(new Date());
    }

    public static String getWeekOfDate(Date date) {
        String[] weekDays = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return weekDays[w];
    }

    public static String getWeekOfInt(Date date) {
        String[] weekDays = {"1", "2", "3", "4", "5", "6", "7"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return weekDays[w];
    }

    public static String getWeekOfIntDate(Date date, String hasDay) {
        if (TextUtils.isEmpty(hasDay) ) {
            return "";
        }
        String[] weekDays = {"一", "二", "三", "四", "五", "六", "日"};

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        w = w % 7;

        if (!hasDay.contains(",")) {
            if (Integer.parseInt(hasDay) == w) {
                return "今天 周" + weekDays[Integer.parseInt(hasDay) - 1];
            } else if (Integer.parseInt(hasDay) - w == 1) {
                return "明天 周" + weekDays[Integer.parseInt(hasDay) - 1];
            } else {
                return "周" + weekDays[Integer.parseInt(hasDay) - 1];
            }
        } else {
            String[] arry = hasDay.split(",");
            String week = "每周";
            for (int i = 0; i < arry.length; i++) {
                if (i == arry.length - 1) {
                    week = week + weekDays[Integer.parseInt(arry[i]) - 1];
                } else {
                    week = week + weekDays[Integer.parseInt(arry[i]) - 1] + "、";
                }

            }
            if (arry.length == 7) {
                week = "每天";
            }
            return week;
        }
    }

    public static String getDate(String time) {
        Date date0 = null;
        try {
            try {
                if (time.contains("GMT")) {
                    date0 = new Date(time);
                } else {
                    date0 = fullFormatSdf.parse(time.replace("Z", "").replace("T", " "));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return getStrDate(date0);
    }

    public static String getReturnDate(String time) {
        Date date0 = null;
        try {
            try {
                if (time.contains("GMT")) {
                    date0 = new Date(time);
                } else {
                    date0 = fullFormatSdf.parse(time.replace("Z", "").replace("T", " "));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return getTimeFormatText(date0);
    }

    public static String getStrDate(Date date) {
        if (null == date)
            return "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str = sdf.format(date);
        return str;
    }

    public static String getStrDates(Date date) {
        if (null == date)
            return "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String str = sdf.format(date);
        return str;
    }

    public static String getRemDate(Date date) {
        if (null == date)
            return "";
        SimpleDateFormat sdf = new SimpleDateFormat("MM月dd日 HH:mm");
        String str = sdf.format(date);
        return str;
    }

    /**
     * 返回文字描述的日期
     *
     * @param date
     * @return
     */
    public static String getTimeFormatText(Date date) {
        if (date == null) {
            return null;
        }
        long diff = new Date().getTime() - date.getTime();
        long r = 0;
        if (diff > year) {
            r = (diff / year);
            return r + "年前";
        }
        if (diff > month) {
            r = (diff / month);
            return r + "个月前";
        }
        if (diff > day) {
            r = (diff / day);
            return r + "天前";
        }
        if (diff > hour) {
            r = (diff / hour);
            return r + "个小时前";
        }
        if (diff > minute) {
            r = (diff / minute);
            return r + "分钟前";
        }
        return "刚刚";
    }

    public static boolean isSameDay(Date day1, Date day2) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String ds1 = sdf.format(day1);
        String ds2 = sdf.format(day2);
        if (ds1.equals(ds2)) {
            return true;
        } else {
            return false;
        }
    }

    public static Date timeToDate(String time) {
        if (TextUtils.isEmpty(time))
            return null;
        Date date = null;
        SimpleDateFormat mTFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());

        try {
            date = mTFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 功能描述：返回小时
     *
     * @param date 日期
     * @return 返回小时
     */
    public static int getHour(Date date) {

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(date);

        return calendar.get(Calendar.HOUR_OF_DAY);

    }

    /**
     * 功能描述：返回分
     *
     * @param date 日期
     * @return 返回分钟
     */
    public static int getMinute(Date date) {

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(date);

        return calendar.get(Calendar.MINUTE);
    }

    /**
     * 判断今天在不在所传的周里面（weeks）
     *
     * @param date
     * @param hasDay
     * @return
     */
    public static boolean isHaveTheDay(Date date, String hasDay) {
        boolean ishava;
        if (TextUtils.isEmpty(hasDay) /*&& !hasDay.contains(",")*/) {
            return false;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0) {
            w = 0;
        }
        w = w % 7;
        if (w == 0) {
            w = 7;
        }
        if (hasDay.contains(w + "")) {
//        if ((w + "").equals(hasDay)) {
            ishava = true;
        } else {
            ishava = false;
        }

        return ishava;
    }

    /**
     * C#返回的时间：
     * 2019-08-25T16:24:29.6102898+08:00
     */
    public static long date2Timestamp(String oldDateStr) {
        try {
            String newDateStr = oldDateStr.replace("T", " ");
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            Date date = format.parse(newDateStr);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }

//        System.out.println("年: " + calendar.get(Calendar.YEAR));
//        System.out.println("月: " + (calendar.get(Calendar.MONTH) + 1) + "");
//        System.out.println("日: " + calendar.get(Calendar.DAY_OF_MONTH));
//        System.out.println("时: " + calendar.get(Calendar.HOUR_OF_DAY));
//        System.out.println("分: " + calendar.get(Calendar.MINUTE));
//        System.out.println("秒: " + calendar.get(Calendar.SECOND));
//        System.out.println("当前时间毫秒数：" + calendar.getTimeInMillis());
//        System.out.println(calendar.getTime());
    }

}
