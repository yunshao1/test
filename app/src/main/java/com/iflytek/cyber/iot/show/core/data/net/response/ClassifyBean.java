package com.iflytek.cyber.iot.show.core.data.net.response;

import java.util.ArrayList;
import java.util.List;

public class ClassifyBean {


    private String message;
    private String tryTxt;
    private String readTxt;
    private List<ListBeanX> list;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTryTxt() {
        return tryTxt;
    }

    public void setTryTxt(String tryTxt) {
        this.tryTxt = tryTxt;
    }

    public String getReadTxt() {
        return readTxt;
    }

    public void setReadTxt(String readTxt) {
        this.readTxt = readTxt;
    }

    public List<ListBeanX> getList() {
        return list;
    }

    public void setList(List<ListBeanX> list) {
        this.list = list;
    }

    public static class ListBeanX {

        private int id;
        private String code;
        private String name;
        private int parentId;
        private boolean isRoot;
        private String createDate;
        private int display;
        private List<ListBean> list;
        private List<?> orgs;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public boolean isIsRoot() {
            return isRoot;
        }

        public void setIsRoot(boolean isRoot) {
            this.isRoot = isRoot;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public int getDisplay() {
            return display;
        }

        public void setDisplay(int display) {
            this.display = display;
        }

        public List<ListBean> getList() {
            if (list == null) {
                list = new ArrayList<>();
            }
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public List<?> getOrgs() {
            return orgs;
        }

        public void setOrgs(List<?> orgs) {
            this.orgs = orgs;
        }

        public static class ListBean {
            /**
             * id : 7
             * code : SP_7
             * name : 降压药
             * parentId : 1
             * isRoot : false
             * createDate : 2019-07-12T00:00:00+08:00
             * display : 1
             * list : []
             * orgs : []
             */

            private String id;
            private String code;
            private String name;
            private String img;
            private int parentId;
            private boolean isRoot;
            private String createDate;
            private int display;
            private List<?> list;
            private List<?> orgs;

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getParentId() {
                return parentId;
            }

            public void setParentId(int parentId) {
                this.parentId = parentId;
            }

            public boolean isIsRoot() {
                return isRoot;
            }

            public void setIsRoot(boolean isRoot) {
                this.isRoot = isRoot;
            }

            public String getCreateDate() {
                return createDate;
            }

            public void setCreateDate(String createDate) {
                this.createDate = createDate;
            }

            public int getDisplay() {
                return display;
            }

            public void setDisplay(int display) {
                this.display = display;
            }

            public List<?> getList() {
                return list;
            }

            public void setList(List<?> list) {
                this.list = list;
            }

            public List<?> getOrgs() {
                return orgs;
            }

            public void setOrgs(List<?> orgs) {
                this.orgs = orgs;
            }
        }
    }
}
