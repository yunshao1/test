package com.iflytek.cyber.iot.show.core.data.net.response;

import java.util.List;

public class NutritiousMealsResonse {


    private String message;
    private String tryTxt;
    private String readTxt;
    private List<ListBean> list;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTryTxt() {
        return tryTxt;
    }

    public void setTryTxt(String tryTxt) {
        this.tryTxt = tryTxt;
    }

    public String getReadTxt() {
        return readTxt;
    }

    public void setReadTxt(String readTxt) {
        this.readTxt = readTxt;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {

        private String id;
        private String code;
        private String name;
        private String typeCode;
        private String typeName;
        private String price;
        private int timesNum;
        private String picture;
        private String createDate;
        private String sellerName;
        private PropertiesBean properties;
        private List<?> packages;
        private List<?> images;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTypeCode() {
            return typeCode;
        }

        public void setTypeCode(String typeCode) {
            this.typeCode = typeCode;
        }

        public String getTypeName() {
            return typeName;
        }

        public void setTypeName(String typeName) {
            this.typeName = typeName;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public int getTimesNum() {
            return timesNum;
        }

        public void setTimesNum(int timesNum) {
            this.timesNum = timesNum;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getSellerName() {
            return sellerName;
        }

        public void setSellerName(String sellerName) {
            this.sellerName = sellerName;
        }

        public PropertiesBean getProperties() {
            return properties;
        }

        public void setProperties(PropertiesBean properties) {
            this.properties = properties;
        }

        public List<?> getPackages() {
            return packages;
        }

        public void setPackages(List<?> packages) {
            this.packages = packages;
        }

        public List<?> getImages() {
            return images;
        }

        public void setImages(List<?> images) {
            this.images = images;
        }

        public static class PropertiesBean {
            private List<String> 用餐时间;
            private List<String> 餐别;

            public List<String> get用餐时间() {
                return 用餐时间;
            }

            public void set用餐时间(List<String> 用餐时间) {
                this.用餐时间 = 用餐时间;
            }

            public List<String> get餐别() {
                return 餐别;
            }

            public void set餐别(List<String> 餐别) {
                this.餐别 = 餐别;
            }
        }
    }
}
