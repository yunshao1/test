package com.iflytek.cyber.iot.show.core.data.net.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CommunityBean implements Serializable {

    @SerializedName("id")
    public String id = "";

    @SerializedName("orgType")
    public String orgType = "";

    @SerializedName("title")
    public String title;

    @SerializedName("content")
    public String content = "";

    @SerializedName("imageUrl")
    public String imageUrl;

    @SerializedName("readTxt")
    public String readTxt = "";

    @SerializedName("type")
    public String type = "";

    @SerializedName("bottom_Txt")
    public String bottomTxt = "";



    @Override
    public String toString() {
        return "CommunityBean{" +
                "id='" + id + '\'' +
                ", orgType='" + orgType + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", readTxt='" + readTxt + '\'' +
                ", type='" + type + '\'' +
                ", bottomTxt='" + bottomTxt + '\'' +
                '}';
    }
}
