package com.iflytek.cyber.iot.show.core.utils;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Window;
import android.view.WindowManager;

import java.lang.reflect.Field;

public class ScreenUtil {

    /**
     * 将dp转换成px
     *
     * @param dp
     * @return
     */
    public static int dip2px(Context context, float dp) {
        return (int) (dp * context.getResources().getDisplayMetrics().density);
    }

//    public static int dpToPxInt(float dp) {
//        return (int) (dip2px(dp) + 0.5f);
//    }

    /**
     * 将px转换成dp
     *
     * @param px
     * @return
     */
    public static float pxToDp(Context context, float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

//    public static int pxToDpInt(float px) {
//        return (int) (pxToDp(px) + 0.5f);
//    }

    /**
     * 将px值转换为sp值
     *
     * @param pxValue
     * @return
     */
    public static float pxToSp(Context context, float pxValue) {
        return pxValue / context.getResources().getDisplayMetrics().scaledDensity;
    }

    /**
     * 将sp值转换为px值
     *
     * @param spValue
     * @return
     */
    public static float spToPx(Context context, float spValue) {
        return spValue * context.getResources().getDisplayMetrics().scaledDensity;
    }
}
