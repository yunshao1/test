package com.iflytek.cyber.iot.show.core.data.net.response;

public class VideoUserBean {
    public String name;
    public String userId;

    public VideoUserBean(String name, String userId, String userSig) {
        this.name = name;
        this.userId = userId;
        this.userSig = userSig;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserSig() {
        return userSig;
    }

    public void setUserSig(String userSig) {
        this.userSig = userSig;
    }

    public String userSig;
}
