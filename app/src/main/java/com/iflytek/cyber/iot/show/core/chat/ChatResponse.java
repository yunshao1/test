package com.iflytek.cyber.iot.show.core.chat;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @author cloud.wang
 * @date 2019-07-20
 * @Description: TODO
 */
public class ChatResponse implements Serializable {

    @SerializedName("message")
    public List<ChatMessage> message;

    @Override
    public String toString() {
        String res = "";
        for (ChatMessage msg: message) {
            res += msg.toString() + "\n";
        }
        return "ChatResponse{" +
                "message=" + message +
                '}';
    }
}
