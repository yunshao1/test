package com.iflytek.cyber.iot.show.core.data.net

import android.text.TextUtils
import android.util.Log
import com.google.gson.Gson
import com.iflytek.cyber.iot.show.core.chat.ChatDetailMessage
import com.iflytek.cyber.iot.show.core.chat.SessionParamBean
import com.iflytek.cyber.iot.show.core.chat.UserMessageParamBean
import com.iflytek.cyber.iot.show.core.data.net.response.*
import com.iflytek.cyber.iot.show.core.data.net.service.ChatService
import com.iflytek.cyber.iot.show.core.data.net.service.NetService
import com.iflytek.cyber.iot.show.core.utils.Constant
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap

class NetHandler {

    private val mRetrofit: Retrofit
    private val sGson: Gson

    init {
        val logging = HttpLoggingInterceptor()
        val head = HeadInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
                .connectionPool(SharedOkHttpConnectPool.inst!!)
                .addInterceptor(logging)
                .addInterceptor(head)
                .connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)

                .build()
        mRetrofit = Retrofit.Builder()
                .baseUrl(Constant.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()

        sGson = Gson()
    }

    fun login(map: HashMap<String, String>): Observable<LoginBean> {
        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), Gson().toJson(map))
        return mRetrofit.create(NetService::class.java).login(body)
    }

    fun fetchCommunity(keyword: String): Observable<Response<List<CommunityBean>>> {
        return mRetrofit.create(NetService::class.java).fetchCommunityContent("1", 10, 1,"","", keyword)
    }

    fun getRealTimeByGrop(): Observable<RealTimeByGroupBena> {
        return mRetrofit.create(NetService::class.java).getRealTimeByGrop()
    }

    fun getAskHealthByGrop(): Observable<AskHealthBean> {
        return mRetrofit.create(NetService::class.java).getAskHealthByGrop()
    }

    fun getMyOrderyGrop(): Observable<MyOrederBean> {
        return mRetrofit.create(NetService::class.java).getMyOrderyGrop( "", 10, 1)
    }
    fun getConsultByGrop(): Observable<ConsultBean> {
        return mRetrofit.create(NetService::class.java).getConsultByGrop(Constant.CONFIG_ELDERID, 10, 1)
    }

    fun getAdviseByGrop(): Observable<ConsultBean> {
        return mRetrofit.create(NetService::class.java).getAdviseByGrop(Constant.CONFIG_ELDERID, 10, 1)
    }
    fun getByClassifyId(type: String, keyword: String, pageSize: Int, pageNo: Int): Observable<ByClassifyIdBean> {
        return mRetrofit.create(NetService::class.java).getByClassifyId(type, keyword, pageSize, pageNo)

    }
    fun getService(type: String, keyword: String, pageSize: Int, pageNo: Int): Observable<ServiceBean> {
        return mRetrofit.create(NetService::class.java).getService(type, keyword, pageSize, pageNo)
    }

    fun getService2(url:String,map: Map<String, String>): Observable<ServiceBean> {
        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), Gson().toJson(map))

        return mRetrofit.create(NetService::class.java).getService2(url,body)
    }

    fun getService3(url:String,map: Map<String, String>): Observable<NutritiousMealsResonse> {
        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), Gson().toJson(map))
        return mRetrofit.create(NetService::class.java).getService3(url,body)
    }
    fun getService4(url:String,map: Map<String, String>): Observable<RepairResponse> {
        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), Gson().toJson(map))
        return mRetrofit.create(NetService::class.java).getService4(url,body)
    }
    fun getPhDetail(map: Map<String, String>): Observable<PharmacyDetailResponse> {
        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), Gson().toJson(map))
        return mRetrofit.create(NetService::class.java).getPhDetail(body)
    }
    fun getService7( keyword: String, pageSize: Int, pageNo: Int): Observable<ServiceBean> {
        return mRetrofit.create(NetService::class.java).getService7( keyword, pageSize, pageNo)
    }
    fun getService8(keyword: String, pageSize: Int, pageNo: Int): Observable<ServiceBean> {
        return mRetrofit.create(NetService::class.java).getService8(keyword, pageSize, pageNo)
    }
    fun getService9(keyword: String, pageSize: Int, pageNo: Int): Observable<ServiceBean> {
        return mRetrofit.create(NetService::class.java).getService9(keyword, pageSize, pageNo)
    }

    fun getFormsBriefly(): Observable<HealthReportBean> {
        return mRetrofit.create(NetService::class.java).getFormsBriefly()
    }

    fun getClassify(url:String,map:Map<String,String>): Observable<ClassifyBean> {
        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), Gson().toJson(map))
        return mRetrofit.create(NetService::class.java).getClassify(url,body)
    }

    fun submitAction(orgId: String): Observable<ActionResponse> {
        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), "{\"socialId\": \"$orgId\",\"actionName\": \"参加\"}")
        return mRetrofit.create(NetService::class.java).submitAction(body)
    }

    /** 适老旅游，康复护理，周边药房二级药物详情 */
    fun submitAction2(map: Map<String, String>): Observable<ActionResponse> {
        val date:String =Date().toString()
        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), Gson().toJson(map))
        return mRetrofit.create(NetService::class.java).submitServiceAction2(body)
    }

    /** 适老旅游，康复护理，周边药房二级药物详情 */
    fun submitServiceAction2(id: String, date: String, type: String): Observable<ActionResponse> {
        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), "{\"details\": {\"就餐时间\": \"$date\",\"就餐类型\": \"$type\",},\"id\": \"$id\",\"elderId\": \"${Constant.CONFIG_ELDERID}\",\"actionName\": \"预定\"}")
        return mRetrofit.create(NetService::class.java).submitServiceAction2(body)
    }

    /** 居家维修 */
    fun submitAction3(serviceContent: String): Observable<ActionResponse> {
        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), "{\"elderId\": \"${Constant.CONFIG_ELDERID}\",\"serviceContent\": \"$serviceContent\"}")
        return mRetrofit.create(NetService::class.java).submitOrderAction3(body)
    }

    fun playVoice(text: String) {
        val deviceId: String = Constant.DEVICE_ID
        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), "{\"text\": \"$text\",\"deviceId\": \"$deviceId\"}")
        var call = mRetrofit.create(NetService::class.java).playVoice(body)
        call.enqueue(object: Callback<RequestBody> {
            override fun onFailure(call: Call<RequestBody>, t: Throwable) {
                Log.i("Cloud", "playVoice onFailure"+ t.message)
            }

            override fun onResponse(call: Call<RequestBody>, response: retrofit2.Response<RequestBody>) {
                Log.i("Cloud", "playVoice onResponse"+ response.body().toString())
            }
        })
    }

    fun sendSOS(): Observable<Response<Empty>> {
        return mRetrofit.create(NetService::class.java).sendSOS(Constant.CONFIG_ELDERID)
    }

    fun sendConsultAsync(elderId: String, message: String, messageType: String): Observable<OnlineResBean> {
        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), "{\"elderId\": \"$elderId\",\"message\": \"$message\",\"messageType\": \"$messageType\"}")
        return mRetrofit.create(NetService::class.java).sendConsultAsync(body)
    }

    fun uploadVoice(file: File): Observable<UploadResponse> {
        //表单类型
        val fileBody = RequestBody.create(MultipartBody.FORM, file)
        //添加图片数据，body创建的请求体
        val body = MultipartBody.Part.createFormData("file", file.name, fileBody)
        return mRetrofit.create(NetService::class.java).uploadVoice(body)
    }

    /**
     * 将图片封装成可上传的MultipartBody.Part对象
     * @param path  图片所在路径
     * @return  MultipartBody.Part
     */
    fun getAvatarFile(path: String): MultipartBody.Part? {
        if (TextUtils.isEmpty(path)) {
            return null
        }

        //根据路径获取
        val file = File(path)
        //表单类型
        val fileBody = RequestBody.create(MultipartBody.FORM, file)
        //添加图片数据，body创建的请求体
        return MultipartBody.Part.createFormData("file", file.name, fileBody)
    }

    fun joinGroup(paramBean: SessionParamBean): Observable<BaseResponse<JoinGroupResponse>> {
//        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
//                "{\"groupId\": \"$groupId\",\"groupType\": $groupType,\"appKey\": \"$appKey\"}")
        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), sGson.toJson(paramBean))
        return mRetrofit.create(ChatService::class.java).joinGroup(body)
    }

    fun sendMessage(paramBean: SessionParamBean, detailMessage: ChatDetailMessage): Observable<BaseResponse<JoinGroupResponse>> {
//        val msgContent = "{\\\"msgType\\\":\\\"txt\\\"," +
//                "\\\"msgContent\\\":\\\"$content\\\"," +
//                "\\\"iconImg\\\":\\\"http://elderlyservice.shfusion.com/Image/newTow/avatar1@2x.jpg\\\"," +
//                "\\\"name\\\":\\\"Mobile\\\",\\\"nickname\\\":\\\"客户端开发\\\"}"
//        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
//                "{\"group\": \"$groupId\",\"groupType\": $groupType,\"msgType\": \"txt\",\"content\":\"$msgContent\",\"appKey\": \"$appKey\"}")

        val content = sGson.toJson(detailMessage)
        paramBean.content = content
        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), sGson.toJson(paramBean))

        return mRetrofit.create(ChatService::class.java).sendMessage(body)
    }

    fun sendUserMessage(paramBean: UserMessageParamBean, detailMessage: ChatDetailMessage): Observable<BaseResponse<JoinGroupResponse>> {
//        val msgContent = "{\\\"msgType\\\":\\\"txt\\\"," +
//                "\\\"msgContent\\\":\\\"$content\\\"," +
//                "\\\"iconImg\\\":\\\"http://elderlyservice.shfusion.com/Image/newTow/avatar1@2x.jpg\\\"," +
//                "\\\"name\\\":\\\"Mobile\\\",\\\"nickname\\\":\\\"客户端开发\\\"}"
//        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
//                "{\"group\": \"$groupId\",\"groupType\": $groupType,\"msgType\": \"txt\",\"content\":\"$msgContent\",\"appKey\": \"$appKey\"}")

        val content = sGson.toJson(detailMessage)
        paramBean.msgContent = content
        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), sGson.toJson(paramBean))

        return mRetrofit.create(ChatService::class.java).sendUserMessage(body)
    }

    fun fetchGroupIdByType(type: Int): Observable<BaseResponse<ChatGroupBean>> {
        return mRetrofit.create(ChatService::class.java).fetchGroupId(type)
    }

    fun fetchGroupIdByType2(type: Int): Observable<BaseResponse<List<ChatGroupBean2>>> {
        return mRetrofit.create(ChatService::class.java).fetchGroupId2(type)
    }
    /**
     * 获取服务
     */
    fun getPage(appCode: String,pageCode :String): Observable<ServiceNewBean> {
        return mRetrofit.create(NetService::class.java).getPage(appCode,pageCode)
    }

    /**
     * 获取广告
     */
    fun getAd(url:String,appCode: String): Observable<AdBeanResonse> {
        return mRetrofit.create(NetService::class.java).getAd(url,appCode)
    }

    /**
     * 获取提醒列表
     */
    fun getReminder(): Observable<ReminderResponse> {
        return mRetrofit.create(NetService::class.java).getReminder()
    }
    /**
     * 删除一个提醒
     */
    fun closeReminder(id: String): Observable<CloseReminder> {
        return mRetrofit.create(NetService::class.java).closeReminder(id)
    }

    /**
     * 删除提醒
     */
    fun deleteReminder(id: String): Observable<String> {
        return mRetrofit.create(NetService::class.java).deleteReminder(id)
    }


    fun creatRemind(map:Map<String,Object>): Observable<CreatRemindResponse> {
        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), Gson().toJson(map))
        return mRetrofit.create(NetService::class.java).creatRemind(body)
    }
    fun getMessage(map:Map<String,String>): Observable<GetMessageResponse> {
        return mRetrofit.create(NetService::class.java).getMessage(map)
    }
    fun sendMessageByLiuYan(map:Map<String,String>): Observable<SendMessageByLiuYanRespoonse> {
        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), Gson().toJson(map))
        return mRetrofit.create(NetService::class.java).sendMessage(body)
    }

    fun setReaded(string: String): Observable<CreatRemindResponse> {
        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), string)
        return mRetrofit.create(NetService::class.java).setReaded(body)
    }

    fun getFamilyMessages(): Observable<PersonListMessageResponse> {
        return mRetrofit.create(NetService::class.java).getFamilyMessages()
    }

    fun getUnreadMessageAbstract(): Observable<HomeUnderReadMessageResponse> {
        return mRetrofit.create(NetService::class.java).getUnreadMessageAbstract()
    }

    companion object {
        val Inst: NetHandler by lazy(mode = LazyThreadSafetyMode.SYNCHRONIZED) {
            NetHandler()
        }
    }
}