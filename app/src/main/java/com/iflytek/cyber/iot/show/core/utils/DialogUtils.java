package com.iflytek.cyber.iot.show.core.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.adapter.PopChouseWeekAdapter;
import com.iflytek.cyber.iot.show.core.data.net.response.ReminderResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DialogUtils {

    @SuppressLint("WrongConstant")
    public static void showMyToast(Context context, String msg) {
        if (TextUtils.isEmpty(msg))
            return;
        Toast toast = new Toast(context);
        toast.setDuration(0);
        toast.setGravity(Gravity.CENTER, 0, 0);
        LayoutInflater inflater = LayoutInflater.from(context);
        LinearLayout toastLayout = (LinearLayout) inflater.inflate(R.layout.own_toast, null);
        TextView txtToast = toastLayout.findViewById(R.id.toast);
        txtToast.setText(msg);
        toast.setView(toastLayout);
        toast.show();
    }

    @SuppressLint("WrongConstant")
    public static void showDisssToast(Context context) {
        Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
//        Toast toast = new Toast(context);
//        toast.setDuration(0);
//        toast.setGravity(Gravity.CENTER, 0, 0);
//        LayoutInflater inflater = LayoutInflater.from(context);
//        LinearLayout toastLayout = (LinearLayout) inflater.inflate(R.layout.toast_disssmiss, null);
//        toast.setView(toastLayout);
//        toast.show();
    }

    public static void showPopToast(Context context, String msg) {
        if (TextUtils.isEmpty(msg))
            return;
        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM, 0, -50);
        LayoutInflater inflater = LayoutInflater.from(context);
        LinearLayout toastLayout = (LinearLayout) inflater.inflate(R.layout.activity_snackbar, null);
        TextView txtToast = toastLayout.findViewById(R.id.snackbar_text);
        ViewGroup.LayoutParams params = txtToast.getLayoutParams();
//        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.width = 3000;
        txtToast.setLayoutParams(params);
        txtToast.setText(msg);
        toast.setView(toastLayout);
        toast.show();
    }

    public static PopupWindow showToalkPop(Context context, View view) {
        PopupWindow popupWindow = null;
        View contentView = LayoutInflater.from(context).inflate(R.layout.popuplayout, null);
        popupWindow = new PopupWindow(contentView,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setContentView(contentView);
        //设置各个控件的点击响应
        //显示PopupWindow
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
        return popupWindow;
    }

    public static PopupWindow showChouseWeekPop(Context context, View view,OnItemClickListener onItemClickListener) {
        View contentView = LayoutInflater.from(context).inflate(R.layout.pop_chouse_weeks, null);
        final PopupWindow popupWindow = new PopupWindow(contentView,
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setContentView(contentView);

        TextView cancel = contentView.findViewById(R.id.cancel);
        TextView tv_ok = contentView.findViewById(R.id.tv_ok);
        RecyclerView recly_view = contentView.findViewById(R.id.recycler);
        PopChouseWeekAdapter adapter = new PopChouseWeekAdapter(context);
        recly_view.setLayoutManager(new LinearLayoutManager(context));
        recly_view.setAdapter(adapter);
        adapter.setOnItemClickListener(new PopChouseWeekAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                if (pos == 0) {
                    if (adapter.getMap().get(pos)) {
                        adapter.getMap().put(pos, false);
                    } else {
                        adapter.getMap().put(pos, true);
                    }

                    for (int i = 0; i < adapter.getList().length; i++) {
                        if (i != pos) {
                            adapter.getMap().put(i, false);
                        }
                    }
                } else {
                    if (adapter.getMap().get(pos)) {
                        adapter.getMap().put(pos, false);
                    } else {
                        adapter.getMap().put(0, false);
                        adapter.getMap().put(pos, true);
                    }

                }

                adapter.notifyDataSetChanged();
            }
        });
        tv_ok.setOnClickListener(v -> {
            if (getWeeks(adapter).size() != 0) {
                if (onItemClickListener!=null){
                    onItemClickListener.onItemClick(getWeeks(adapter));
                }

                popupWindow.dismiss();
            } else {
                Toast.makeText(context, "请先选择重复时间", Toast.LENGTH_SHORT).show();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        //设置各个控件的点击响应
        //显示PopupWindow
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
        return popupWindow;
    }

    public interface OnItemClickListener {
        void onItemClick(List<String> weeks);
    }

    private static List<String> getWeeks(PopChouseWeekAdapter adapter) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < adapter.getList().length; i++) {
            if (i == 0) {
                if (adapter.getMap().get(i)) {
                    list.add("0");
                }
            } else {
                if (adapter.getMap().get(i)) {
                    int count = i;
                    list.add(count + "");
                }
            }
        }
        return list;
    }

    public static PopupWindow showNuPop(Context context, View view) {
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_fragment_nutritious, null);
        final PopupWindow popupWindow = new PopupWindow(contentView,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setContentView(contentView);
        contentView.findViewById(R.id.confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        contentView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        //设置各个控件的点击响应
        //显示PopupWindow
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
        return popupWindow;
    }

    /**
     * 选择组员
     *
     * @param context
     */
    public static View openMyminderDialog(final Activity context, ReminderResponse.ListBean listBean, OnVodioClickListener onVodioClickListener) {
        if (context == null)
            return null;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_open_myreminder, null);
        final Dialog dialog = new Dialog(context, R.style.DialogTheme);
        ImageView tv_cancal = view.findViewById(R.id.im_close);
        TextView tv_time = view.findViewById(R.id.tv_time);
        TextView image_edit = view.findViewById(R.id.image_edit);
        TextView tv_content = view.findViewById(R.id.tv_content);
        ImageView image = view.findViewById(R.id.image);

        String wewkDay = DateUtils.getWeekOfIntDate(new Date(), listBean.getWeek());
        int hour = listBean.getHour();
        int min = listBean.getMinute();
        String time = "";
        if (hour < 10) {
            time = wewkDay + " 0" + hour + ":";
        } else {
            time = wewkDay + " " + hour + ":";
        }
        if (min < 10) {
            time = time + " 0" + min;
        } else {
            time = time + " " + min;
        }
        tv_time.setText(time);
        String voiceUrl = listBean.getAudioUrl();
        if (TextUtils.isEmpty(voiceUrl)) {
            image.setVisibility(View.GONE);
            tv_content.setVisibility(View.VISIBLE);
        } else {
            image.setVisibility(View.VISIBLE);
            tv_content.setVisibility(View.GONE);
        }
        image.setOnClickListener(v -> {
            if (onVodioClickListener != null) {
                onVodioClickListener.onItemClick(voiceUrl, 1);
            }
        });

        if (onVodioClickListener != null) {
            if (TextUtils.isEmpty(voiceUrl)) {
                onVodioClickListener.onItemClick(listBean.getText(), 0);
            }
        }
        tv_cancal.setOnClickListener(v -> dialog.dismiss());
        image_edit.setOnClickListener(v -> dialog.dismiss());
        dialog.setContentView(view);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        dialog.setCanceledOnTouchOutside(false);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.copyFrom(window.getAttributes());
//        WindowManager wm1 = context.getWindowManager();
//        int width1 = wm1.getDefaultDisplay().getWidth();
//        int height1 = wm1.getDefaultDisplay().getHeight();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER;
        window.setAttributes(lp);
        hideBack(dialog);
        dialog.show();
        return view;
    }

    /**
     * 隐藏虚拟按键
     *
     * @param dialog
     */
    public static void hideBack(Dialog dialog) {
        dialog.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        dialog.getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(visibility -> {
            int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    //布局位于状态栏下方
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                    //全屏
                    View.SYSTEM_UI_FLAG_FULLSCREEN |
                    //隐藏导航栏
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
            if (Build.VERSION.SDK_INT >= 19) {
                uiOptions |= 0x00001000;
            } else {
                uiOptions |= View.SYSTEM_UI_FLAG_LOW_PROFILE;
            }
            dialog.getWindow().getDecorView().setSystemUiVisibility(uiOptions);
        });
    }

    /**
     * 1是语音0 是文字
     */
    public interface OnVodioClickListener {
        void onItemClick(String url, int type);
    }


}
