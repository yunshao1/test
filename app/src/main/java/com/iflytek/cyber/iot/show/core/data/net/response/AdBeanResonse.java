package com.iflytek.cyber.iot.show.core.data.net.response;

import java.util.List;

public class AdBeanResonse {


    private String message;
    private String tryTxt;
    private String readTxt;
    private List<ListBean> list;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTryTxt() {
        return tryTxt;
    }

    public void setTryTxt(String tryTxt) {
        this.tryTxt = tryTxt;
    }

    public String getReadTxt() {
        return readTxt;
    }

    public void setReadTxt(String readTxt) {
        this.readTxt = readTxt;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * relateId : S-20190717032911
         * img : http://resource.shfusion.com/image/ad/health/家庭医生.png
         * title : 家庭医生
         * content : VIP2对1，提供专业和个性化的保健服务
         * linkUrl : http://speaker.api.shfusion.com/api/Service/GetSingleService
         * openType : 0
         */

        private String relateId;
        private String img;
        private String name;
        private String content;
        private String linkUrl;
        private int openType;

        public String getRelateId() {
            return relateId;
        }

        public void setRelateId(String relateId) {
            this.relateId = relateId;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getTitle() {
            return name;
        }

        public void setTitle(String title) {
            this.name = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getLinkUrl() {
            return linkUrl;
        }

        public void setLinkUrl(String linkUrl) {
            this.linkUrl = linkUrl;
        }

        public int getOpenType() {
            return openType;
        }

        public void setOpenType(int openType) {
            this.openType = openType;
        }
    }
}
