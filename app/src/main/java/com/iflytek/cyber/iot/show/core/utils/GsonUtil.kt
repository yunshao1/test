package com.iflytek.cyber.iot.show.core.utils

import com.google.gson.Gson
import com.iflytek.cyber.iot.show.core.chat.ChatDetailMessage
import com.iflytek.cyber.iot.show.core.model.CustomDirective
import org.json.JSONObject
import java.lang.Exception

/**
 * @author cloud.wang
 * @date 2019/6/1
 * @Description: TODO
 */
object GsonUtil {

    val sGson = Gson()

    fun convertCustomDirective(jsonStr: JSONObject): CustomDirective? {
        val directive = jsonStr.getJSONObject("directive")
        val payload = directive.getJSONObject("payload")
        val result = payload.optString("text")

        if (result.isEmpty()) {
            return null
        }

        return sGson.fromJson<CustomDirective>(result, CustomDirective::class.java)
    }

    fun convertChatDetailMessage(jsonStr: String): ChatDetailMessage? {
        try {
            return sGson.fromJson(jsonStr, ChatDetailMessage::class.java)
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }
    }
}