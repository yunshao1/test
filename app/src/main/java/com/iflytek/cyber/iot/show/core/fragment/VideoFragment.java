package com.iflytek.cyber.iot.show.core.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.navigation.fragment.NavHostFragment;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.adapter.VideoAdapter;
import com.iflytek.cyber.iot.show.core.chat.manager.VideoManager;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.BaseResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.ChatGroupBean;
import com.iflytek.cyber.iot.show.core.data.net.response.JoinGroupResponse;
import com.iflytek.cyber.iot.show.core.impl.SpeechRecognizer.SpeechRecognizerHandler;
import com.iflytek.cyber.iot.show.core.utils.Constant;
import com.iflytek.cyber.iot.show.core.utils.RoomUtils;
import com.iflytek.cyber.iot.show.core.utils.ToastUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class VideoFragment extends BaseFragment {
    LinearLayout ll_finsh;
    private View view;
    private VideoAdapter adapter;
    private RecyclerView recly_view;
    private TextView txl;
    private TextView thjl;
    private final static int REQ_PERMISSION_CODE = 0x1000;
    private SpeechRecognizerHandler instance;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private ProgressDialog progressDialog;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_video, container, false);
            initView();
        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        instance = SpeechRecognizerHandler.getInstance(getContext(), null, false, false, "");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setHideBottomBar(true);
        setTipText("试试，“蓝小飞，打电话给第一个”", R.style.TextBlackFont);
        load();
    }

    @Override
    public void onResume() {
        super.onResume();

        checkPermission();

        instance.startAudioInput();
        closeDialog();
    }

    @Override
    public void onStop() {
        super.onStop();
        closeDialog();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        instance.startAudioInput();
    }

    private void load() {
        Disposable disposable = NetHandler.Companion.getInst().fetchGroupIdByType(Constant.FRIEND_TYPE_ADDRESS_BOOK)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<BaseResponse<ChatGroupBean>>() {
                               @Override
                               public void accept(BaseResponse<ChatGroupBean> baseResponse) throws Exception {
                                   if (baseResponse.isSuccess() && baseResponse.result.groups != null && baseResponse.result.groups.size() > 0) {
                                       adapter.setDataList(baseResponse.result.groups);
                                   }
                               }
                           }, new Consumer<Throwable>() {
                               @Override
                               public void accept(Throwable throwable) throws Exception {
                                   ToastUtil.showLongToastCenter("通讯录请求失败" + throwable.getMessage());
                               }
                           }
                );

        mCompositeDisposable.add(disposable);
    }

    public void showDialog() {
        progressDialog.show();
        progressDialog.setContentView(R.layout.progressdialog_layout);
    }

    public void closeDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    private void initView() {
        progressDialog = new ProgressDialog(getActivity(), R.style.CustomProgressDialog);
        progressDialog.setCanceledOnTouchOutside(false);
        ll_finsh = this.view.findViewById(R.id.ll_finsh);
        txl = this.view.findViewById(R.id.txl);
        thjl = this.view.findViewById(R.id.thjl);
        thjl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recly_view.setLayoutManager(new GridLayoutManager(getActivity(), 1));
                adapter.setType(1);
                thjl.setBackgroundColor(getResources().getColor(R.color.color_2eba90));
                txl.setBackgroundResource(R.drawable.video_title_bg_left);
                txl.setTextColor(getResources().getColor(R.color.color_2eba90));
                thjl.setTextColor(getResources().getColor(R.color.color_white));
            }
        });
        txl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recly_view.setLayoutManager(new GridLayoutManager(getActivity(), 3));
                txl.setBackgroundColor(getResources().getColor(R.color.color_2eba90));
                thjl.setBackgroundResource(R.drawable.video_title_bg);
                adapter.setType(0);
                txl.setTextColor(getResources().getColor(R.color.color_white));
                thjl.setTextColor(getResources().getColor(R.color.color_2eba90));

            }
        });
        recly_view = view.findViewById(R.id.recycler);
        adapter = new VideoAdapter(getActivity());
        adapter.setOnItemClickListener(new VideoAdapter.OnItemClickListener() {
            @Override
            public void onItemclick(ChatGroupBean.GroupsBean bean) {
                instance.stopAudioInput();
                showDialog();
                HashMap<String, String> map = new HashMap<>();
                map.put("groupName", bean.groupName);
                map.put("active", "true");
                map.put("img", bean.img);
                map.put("video_status", "0");
                map.put("toName", bean.groupName);
                VideoManager.getInstance().initiateVideoCall(getLauncher(),
                        RoomUtils.getRoomId(), bean.notifyId, map, new VideoManager.RequestCallBack<JoinGroupResponse>() {
                    @Override
                    public void onSuccess(JoinGroupResponse joinGroupResponse) {
                        closeDialog();
                    }

                    @Override
                    public void onFailed(String msg) {
                        closeDialog();
                    }
                });
            }
        });
        recly_view.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        adapter.setType(0);
        recly_view.setAdapter(adapter);
        ll_finsh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(VideoFragment.this).navigateUp();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQ_PERMISSION_CODE:
                for (int ret : grantResults) {
                    if (PackageManager.PERMISSION_GRANTED != ret) {
                        Toast.makeText(getActivity(), "用户没有允许需要的权限，使用可能会受到限制！", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            default:
                break;
        }
    }

    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> permissions = new ArrayList<>();
            if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO)) {
                permissions.add(Manifest.permission.RECORD_AUDIO);
            }

            if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)) {
                permissions.add(Manifest.permission.CAMERA);
            }
            if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }

            if (permissions.size() != 0) {
                requestPermissions((String[]) permissions.toArray(new String[0]),
                        REQ_PERMISSION_CODE);
                return false;
            }
        }

        return true;
    }

//    private void sendNotifyMessage(int roomId, ChatGroupBean.GroupsBean bean) {
//        SessionParamBean param = new SessionParamBean(bean.notifyId + "");
//        param.setGroupType(4);
//        Disposable disposable = NetHandler.Companion.getInst().joinGroup(param)
//                .subscribeOn(Schedulers.io())
//                .flatMap(new Function<BaseResponse<JoinGroupResponse>, ObservableSource<BaseResponse<JoinGroupResponse>>>() {
//                    @Override
//                    public ObservableSource<BaseResponse<JoinGroupResponse>> apply(BaseResponse<JoinGroupResponse> joinGroupResponse) throws Exception {
//                        if (!joinGroupResponse.isSuccess()) {
//                            return Observable.error(new Throwable("加入指定人的组失败，请重试"));
//                        }
//
//                        SessionParamBean paramBean = new SessionParamBean(bean.notifyId + "");
//                        paramBean.setGroup(bean.notifyId + "");
//                        paramBean.setGroupType(4);
//                        paramBean.setMsgType("txt");
//                        ChatDetailMessage detailMessage = new ChatDetailMessage();
//                        detailMessage.setMsgType("txt");
//                        detailMessage.setMsgContent(roomId + "");
//                        detailMessage.setIconImg(LoginSession.INSTANCE.getSUserAvatar());
//                        detailMessage.setNickname(LoginSession.INSTANCE.getSUserName());
//
//                        return NetHandler.Companion.getInst().sendMessage(paramBean, detailMessage);
//                    }
//                })
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<BaseResponse<JoinGroupResponse>>() {
//                               @Override
//                               public void accept(BaseResponse<JoinGroupResponse> joinGroupResponse) throws Exception {
//                                   Log.i("Cloud", "JoinGroupResponse " + joinGroupResponse.toString());
//                                   if (joinGroupResponse.isSuccess()) {
//                                       onJoinRoom(roomId, bean);
//                                   }
//                               }
//                           }, new Consumer<Throwable>() {
//                               @Override
//                               public void accept(Throwable throwable) throws Exception {
//                                   Log.i("Cloud", "JoinGroupResponse failed " + throwable.getMessage());
//                               }
//                           }
//                );
//        mCompositeDisposable.add(disposable);
//    }
//
//    private void onJoinRoom(final int roomId, ChatGroupBean.GroupsBean bean) {
//        instance.stopAudioInput();
////        ProgressDialog dialog = ProgressDialog.show(getContext(), "提示", "正在进入房间…", true, false, null);
//        final Intent intent = new Intent(getActivity(), TRTCMainActivity.class);
//        String userSig = SharedPreUtils.getInstance(getContext()).getValue(SharedPreUtils.USER_SIG, "");
//        String userId = SharedPreUtils.getInstance(getContext()).getValue(SharedPreUtils.USER_ID, "");
//        intent.putExtra("roomId", roomId);
//        intent.putExtra("userId",userId);
//        intent.putExtra("AppScene", TRTCCloudDef.TRTC_APP_SCENE_VIDEOCALL);
//        intent.putExtra("customVideoCapture", false);
//        intent.putExtra("videoFile", false);
//        intent.putExtra("sdkAppId", 1400230639);
//        intent.putExtra("userSig", userSig);
//
//        startActivity(intent);
//    }
}
