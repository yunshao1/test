package com.iflytek.cyber.iot.show.core.fragment;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.adapter.MyReminderAdapter;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.ReminderResponse;
import com.iflytek.cyber.iot.show.core.utils.Constant;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import androidx.navigation.fragment.NavHostFragment;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * 我的提醒
 */
public class MyReminderFragment extends BaseFragment {
    LinearLayout ll_finsh;
    private RecyclerView recyclerView;
    private MyReminderAdapter adapter;

    private View view;
    private TextView tv_add, tv_edit;
    private MediaPlayer mediaPlayer;
    private boolean isRun = true;
    private ExecutorService executorService = Executors.newFixedThreadPool(2);
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            view = inflater.inflate(R.layout.fragment_my_reminder, container, false);
            initView();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        isRun = true;
    }

    @Override
    public void onActivityCreated(@org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        load("");
    }

    public void load(final String keyword) {
        NetHandler.Companion.getInst().getReminder()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ReminderResponse>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(ReminderResponse listResponse) {
                        if (listResponse != null && listResponse.getList() != null) {
                            setData(listResponse);


                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public void delete(String ids) {
        NetHandler.Companion.getInst().deleteReminder(ids)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<String>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(String listResponse) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void setData(ReminderResponse listResponse) {
        if (listResponse.getList() != null && listResponse.getList().size() > 0) {
            if (listResponse.getTryTxt() != null) {
                setHideBottomBar(true);
                setTipText(listResponse.getTryTxt(), R.style.TextWhiteFont);
            }
            adapter.setData(listResponse.getList());
        }
    }


    private void initView() {
        ll_finsh = view.findViewById(R.id.ll_finsh);
        recyclerView = view.findViewById(R.id.recyclerView);
        tv_add = view.findViewById(R.id.tv_add);
        tv_edit = view.findViewById(R.id.tv_edit);
        adapter = new MyReminderAdapter(getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        ll_finsh.setOnClickListener(v -> NavHostFragment.findNavController(MyReminderFragment.this).navigateUp());
        tv_add.setOnClickListener(v -> NavHostFragment.findNavController(MyReminderFragment.this).navigate(R.id.action_to_add_my_reminder_fragment));
        tv_edit.setOnClickListener(v -> {
            if ("完成".equals(tv_edit.getText().toString())) {

                tv_add.setVisibility(View.VISIBLE);
                tv_edit.setText("编辑");
                adapter.setType(0);
            } else {
                adapter.setType(1);
                tv_add.setVisibility(View.GONE);
                tv_edit.setText("完成");
            }
        });
        adapter.setOnItemClickListener((url, pos) -> {
            if (!adapter.getList().get(pos).isPlaying()) {

                for (int i = 0; i < adapter.getList().size(); i++) {
                    adapter.getList().get(i).setIsPlaying(false);
                }
                adapter.getList().get(pos).setIsPlaying(true);
                adapter.notifyDataSetChanged();
                executorService.execute(new PlayThread(url, pos));

            } else {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
                adapter.getList().get(pos).setIsPlaying(false);
                adapter.notifyDataSetChanged();
            }
                }
        );
        adapter.setOnItemDeleteClickListener(pos -> {
            delete(adapter.getList().get(pos).getId());
            adapter.getList().remove(pos);
            adapter.notifyDataSetChanged();

        });
    }


    private class PlayThread implements Runnable {
        private String url;
        private int pos;

        public PlayThread(String url, int pos) {
            this.url = url;
            this.pos = pos;
        }
        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public void run() {
            if (isRun) {
                if (mediaPlayer != null) {
                    try {
                        if (mediaPlayer.isPlaying()) {
                            mediaPlayer.stop();
                            mediaPlayer.release();
                            mediaPlayer = null;
                        }
                    } catch (IllegalStateException e) {
                        mediaPlayer = null;

                    }
                    try {
                        if (mediaPlayer != null) {
                            mediaPlayer.release();
                            mediaPlayer = null;
                        }
                        mediaPlayer = new MediaPlayer();
                        mediaPlayer.setDataSource(url);
                        mediaPlayer.prepare();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        mediaPlayer = new MediaPlayer();
                        mediaPlayer.setDataSource(url);
                        mediaPlayer.prepare();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (mediaPlayer != null) {
                    mediaPlayer.start();

                    mediaPlayer.setOnCompletionListener(mp ->
                            getActivity().runOnUiThread(() -> {
                                adapter.getList().get(pos).setIsPlaying(false);
                                adapter.notifyDataSetChanged();
                            }));
                }
            }

        }
    }
    @Override
    public void onStop() {
        super.onStop();
        isRun = false;
        if (mediaPlayer != null) {
            try {
                mediaPlayer.stop();
            } catch (Exception e) {
                // TODO 如果当前java状态和jni里面的状态不一致，
                //e.printStackTrace();
                mediaPlayer = null;
                mediaPlayer = new MediaPlayer();
            }
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        if (thread != null && !thread.isInterrupted()) {
//            thread.interrupt();
//        }
    }
}
