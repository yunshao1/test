package com.iflytek.cyber.iot.show.core.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.CommunityBean;
import com.iflytek.cyber.iot.show.core.data.net.response.Response;

import java.util.List;

import androidx.navigation.fragment.NavHostFragment;
import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ApplyFragment extends BaseFragment {

    private View mContentView;

    private TextView mBackTv;
    private TextView mContentTv;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mContentView == null) {
            mContentView = inflater.inflate(R.layout.fragment_apply, container, false);
        }

        initView();

        return mContentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void initView() {
        mBackTv = mContentView.findViewById(R.id.tv_back);
        mContentTv = mContentView.findViewById(R.id.tv_content);

        Bundle bundle = getArguments();
        if (bundle == null || bundle.isEmpty()) {
            mBackTv.setText("帮我报名这个活动");
        } else {
            mBackTv.setText(bundle.getString("title"));
            mContentTv.setText(bundle.getString("content"));
            mContentTv.setLineSpacing(32f, 1f);
        }

        mBackTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(ApplyFragment.this).navigateUp();
            }
        });
    }
}
