package com.iflytek.cyber.iot.show.core.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.adapter.MyOrderAdapter;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.MyOrederBean;

import androidx.navigation.fragment.NavHostFragment;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * 我的订单
 */
public class MyOrderFragment extends BaseFragment {
    LinearLayout ll_finsh;
    private RecyclerView recyclerView;
    private MyOrderAdapter adapter;
    private TextView tv_content;

    private View view;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_my_order, container, false);
            initView();
            load();
        }

        return view;
    }

    private void load() {


        NetHandler.Companion.getInst().getMyOrderyGrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MyOrederBean>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(MyOrederBean listResponse) {
                        if ("success".equals(listResponse.getMessage())) {
                            setData(listResponse);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void setData(MyOrederBean listResponse) {
        if (listResponse == null)
            return;
        adapter.setData(listResponse.getList());
        if (listResponse.getTryTxt() != null) {
            setHideBottomBar(true);
            setTipText(listResponse.getTryTxt(), R.style.TextWhiteFont);
        }
    }


    private void initView() {
        ll_finsh = view.findViewById(R.id.ll_finsh);
        recyclerView = view.findViewById(R.id.recyclerView);
        tv_content = view.findViewById(R.id.tv_content);
        adapter = new MyOrderAdapter(getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        ll_finsh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(MyOrderFragment.this).navigateUp();

            }
        });
    }

}
