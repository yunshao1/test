package com.iflytek.cyber.iot.show.core.chat;

/**
 * @author cloud.wang
 * @date 2019-07-27
 * @Description: TODO
 */
public class SessionParamBean {

    private String groupId;

    private String group;

    //2：聊天消息，5：通知消息
    private int groupType = 2;

    private String appKey = "Elderly.SOS";

    private String msgType;

    private String content;

    public SessionParamBean(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getGroupType() {
        return groupType;
    }

    public void setGroupType(int groupType) {
        this.groupType = groupType;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "SessionParamBean{" +
                "groupId='" + groupId + '\'' +
                ", group='" + group + '\'' +
                ", groupType=" + groupType +
                ", appKey='" + appKey + '\'' +
                ", msgType='" + msgType + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
