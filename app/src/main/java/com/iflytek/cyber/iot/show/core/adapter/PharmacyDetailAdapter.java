package com.iflytek.cyber.iot.show.core.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.response.ByClassifyIdBean;
import com.iflytek.cyber.iot.show.core.data.net.response.PharmacyDetailResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.ServiceBean;
import com.iflytek.cyber.iot.show.core.utils.GlideUtil;
import com.iflytek.cyber.iot.show.core.widget.recyclerview.CardAdapterHelper;

import java.util.List;

public class PharmacyDetailAdapter extends RecyclerView.Adapter<PharmacyDetailAdapter.VHolder> {

    private List<PharmacyDetailResponse.ListBean> mList;
    private PharmacyDetailAdapter.OnItemClickListener listener;

    private CardAdapterHelper mCardAdapterHelper = new CardAdapterHelper();

    public void updateList(List<PharmacyDetailResponse.ListBean> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    public List<PharmacyDetailResponse.ListBean> getmList() {
        return mList;
    }

    public void setItemClickListener(PharmacyDetailAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public List<PharmacyDetailResponse.ListBean> getList() {
        return mList;
    }

    @NonNull
    @Override
    public PharmacyDetailAdapter.VHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pharmacy_tetail, viewGroup, false);
        RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(RecyclerView.LayoutParams.WRAP_CONTENT,
                RecyclerView.LayoutParams.MATCH_PARENT);
        params.leftMargin =90;
        itemView.setLayoutParams(params);
        mCardAdapterHelper.onCreateViewHolder(viewGroup, itemView);

        return new PharmacyDetailAdapter.VHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final PharmacyDetailAdapter.VHolder vHolder, int position) {
        PharmacyDetailResponse.ListBean bean = mList.get(position);
        if (bean == null) {
            return;
        }

        mCardAdapterHelper.onBindViewHolder(vHolder.itemView, position, getItemCount());
        vHolder.tv_name.setText(bean.getName());
        vHolder.tv_guige.setText(bean.getDrugSpecification());//没有这个字段
        vHolder.tv_palace.setText(bean.getSellerName());
        vHolder.tv_price.setText("￥"+bean.getPrice());

        vHolder.tv_description.setText(bean.getIntro());
        vHolder.tv_shuoming1.setText(bean.getTypeText());//没有这个字段
        if (bean.isDrugIsInsurance()){
            vHolder.tv_shuoming2.setText("医保");
            vHolder.tv_shuoming2.setVisibility(View.VISIBLE);
        }else {
            vHolder.tv_shuoming2.setVisibility(View.GONE);
        }



        vHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClickListener(v, vHolder.getAdapterPosition());
                }
            }
        });

        GlideUtil.loadImage(vHolder.itemView.getContext(), bean.getPicture(), R.drawable.icon_error_image, vHolder.coverIv);
    }

    @Override
    public int getItemCount() {
        if (mList == null)
            return 0;
        else
            return mList.size();
    }


    class VHolder extends RecyclerView.ViewHolder {
        TextView tv_description;
        TextView tv_shuoming2;
        TextView tv_shuoming1;

        TextView tv_price;
        TextView tv_palace;
        TextView tv_guige;
        TextView tv_name;

        ImageView coverIv;

        VHolder(@NonNull View itemView) {
            super(itemView);

            tv_description = itemView.findViewById(R.id.tv_description);
            tv_shuoming2 = itemView.findViewById(R.id.tv_shuoming2);
            tv_shuoming1 = itemView.findViewById(R.id.tv_shuoming1);
            coverIv = itemView.findViewById(R.id.iv_cover);

            tv_price = itemView.findViewById(R.id.tv_price);
            tv_palace = itemView.findViewById(R.id.tv_palace);
            tv_guige = itemView.findViewById(R.id.tv_guige);
            tv_name = itemView.findViewById(R.id.tv_name);
        }
    }

    public interface OnItemClickListener {
        void onItemClickListener(View view, int position);
    }
}
