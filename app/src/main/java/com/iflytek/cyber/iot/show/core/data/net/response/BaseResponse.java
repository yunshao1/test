package com.iflytek.cyber.iot.show.core.data.net.response;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BaseResponse<T> implements Serializable {

    @SerializedName("msg")
    public String message;

    @SerializedName("result")
    public T result;

    @SerializedName("result_code")
    public String resultCode;

    public boolean isSuccess() {
        return TextUtils.equals("200", resultCode);
    }

    @Override
    public String toString() {
        return "BaseResponse{" +
                "message='" + message + '\'' +
                ", result=" + result +
                ", resultCode='" + resultCode + '\'' +
                '}';
    }
}
