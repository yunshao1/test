package com.iflytek.cyber.iot.show.core.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.navigation.fragment.NavHostFragment;

import com.google.gson.Gson;
import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.adapter.RelativesMessageAdapter;
import com.iflytek.cyber.iot.show.core.adapter.TextMessageAdapter;
import com.iflytek.cyber.iot.show.core.chat.manager.VideoManager;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.CreatRemindResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.GetMessageResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.SendMessageByLiuYanRespoonse;
import com.iflytek.cyber.iot.show.core.data.net.response.UploadResponse;
import com.iflytek.cyber.iot.show.core.impl.SpeechRecognizer.SpeechRecognizerHandler;
import com.iflytek.cyber.iot.show.core.utils.DialogUtils;
import com.iflytek.cyber.iot.show.core.utils.RoomUtils;
import com.iflytek.cyber.iot.show.core.utils.SharedPreUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * 亲属留言
 */
public class RelativesMessageFragment extends BaseFragment implements View.OnTouchListener {
    LinearLayout ll_finsh;
    private View view;
    private TextView tv_title;
    private RelativesMessageAdapter adapter;
    private RecyclerView recly_view;
    private MediaPlayer mediaPlayer = new MediaPlayer();
    private ImageView im_ly;//录音
    private RelativeLayout re_top;
    private PopupWindow popupWindow;

    private MediaRecorder recorder;
    private File tempFile;
    private File SDPathDir;
    private boolean isSDCardExit; // 判断SDCard是否存在
    private String toId;

    private RecyclerView recycler_message;
    private TextMessageAdapter textMessageAdapter;

    private ExecutorService service = Executors.newFixedThreadPool(2);

    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private static int REQUEST_PERMISSION_CODE = 1;


    private int lastItemPosition;

    private boolean isRun = true;

    private String audioUrl = "";
    private String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/android/liuyanvoice.mp3";
    private String name;
    private String img;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private Handler handler = new Handler(Looper.getMainLooper());
    private MyRunable runable = new MyRunable();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_relatives_message, container, false);
            initView();
            load(true);
        }
        isSDCardExit = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
        if (isSDCardExit) {
            SDPathDir = Environment.getExternalStorageDirectory();
        }
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (runable != null) {
            runable.cancal();
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        isRun = true;
    }


    private class MyRunable implements Runnable {
        private boolean isrunning = true;

        void cancal() {
            isrunning = false;
        }

        @Override
        public void run() {
            if (isrunning) {
                load(false);
                handler.postDelayed(runable, 10000);
            }


        }
    }

    private void load(boolean isFirst) {

        Map<String, String> map = new HashMap<>();
//        map.put("_LE_DateTime_", "");
        map.put("_EQ_ToId_", "");
        map.put("_EQ_FromId_", "");
        map.put("ToOrFromId", SharedPreUtils.getInstance(getActivity()).getValue(SharedPreUtils.USER_ID, ""));
        map.put("ToOrFromId2", toId);
        map.put("Size", "100");
        NetHandler.Companion.getInst().getMessage(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GetMessageResponse>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(GetMessageResponse listResponse) {
                        setData(listResponse,isFirst);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void setData(GetMessageResponse listResponse, boolean isfirst) {
        if (listResponse == null || listResponse.getList() == null)
            return;
        adapter.setData(listResponse.getList());
        setToRead(getList(listResponse));
        if (isfirst) {
            recly_view.scrollToPosition(adapter.getItemCount() - 1);
        }
//            recly_view.scrollBy(0, recly_view.getHeight());

    }

    private List<String> getList(GetMessageResponse listResponse) {
        if (listResponse.getList() == null)
            return new ArrayList<>();
        List<String> list = new ArrayList<>();
        for (int i = 0; i < listResponse.getList().size(); i++) {
            if (!listResponse.getList().get(i).isIsMine()) {
                if (!listResponse.getList().get(i).isIsRead()) {
                    list.add(listResponse.getList().get(i).getId());
                }
            }
        }
        return list;
    }

    @Override
    public void onStop() {
        super.onStop();
        isRun = false;
        if (mediaPlayer != null) {
            try {
                mediaPlayer.stop();
            } catch (Exception e) {
                // TODO 如果当前java状态和jni里面的状态不一致，
                //e.printStackTrace();
                mediaPlayer = null;
                mediaPlayer = new MediaPlayer();
            }
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }


    private void initView() {

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), PERMISSIONS_STORAGE, REQUEST_PERMISSION_CODE);
            }
        }
        toId = getArguments().getString("toId");
        if (getArguments()!=null){
            name = getArguments().getString("name");
            img = getArguments().getString("img");
        }

        ll_finsh = view.findViewById(R.id.ll_finsh);
        recly_view = view.findViewById(R.id.recycler);
        im_ly = view.findViewById(R.id.im_ly);
        view.findViewById(R.id.iv_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SpeechRecognizerHandler.speechRecognizerHandler.isOpen()) {
                    //关闭讯飞的语音类，防止音频通道占用
                    SpeechRecognizerHandler.speechRecognizerHandler.stopAudioInput();
                }
                //视频通话
                HashMap<String, String> map = new HashMap<>();
                map.put("groupName",name);
                map.put("active","true");
                map.put("img",img);
                map.put("video_status", "0");
                map.put("toName", name);
                VideoManager.getInstance().initiateVideoCall(getLauncher(), RoomUtils.getRoomId(), toId, map);
            }
        });
        re_top = view.findViewById(R.id.re_top);
        tv_title = view.findViewById(R.id.tv_title);
        recycler_message = view.findViewById(R.id.recycler_message);

        //
        recycler_message.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        textMessageAdapter = new TextMessageAdapter(getActivity());
        recycler_message.setAdapter(textMessageAdapter);
        textMessageAdapter.setOnItemClickListener(bean -> {
            sendConsultAsync("", bean);
//            recly_view.scrollBy(0, recly_view.getHeight());
        });
        tv_title.setText("亲属留言");
        im_ly.setOnTouchListener(this);
        recly_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                LinearLayoutManager linearManager = (LinearLayoutManager) layoutManager;
                lastItemPosition = linearManager.findLastVisibleItemPosition();
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });
        adapter = new RelativesMessageAdapter(getActivity());
        recly_view.setLayoutManager(new LinearLayoutManager(getActivity()));
        recly_view.setAdapter(adapter);
        ll_finsh.setOnClickListener(v -> NavHostFragment.findNavController(RelativesMessageFragment.this).navigateUp());
        adapter.setOnItemClickListener(url -> service.execute(new PlayThread(url)));
        adapter.setOnReadClickListener(url -> {
            playVoice(url);
        });

        handler.postDelayed(runable, 10000);
    }

    private class PlayThread implements Runnable {
        private String url;

        public PlayThread(String url) {
            this.url = url;
        }
        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public void run() {
            if (isRun) {
                if (mediaPlayer != null) {
                    try {
                        if (mediaPlayer.isPlaying()) {
                            mediaPlayer.stop();
                            mediaPlayer.release();
                            mediaPlayer = null;
                        }
                    } catch (IllegalStateException e) {
                        mediaPlayer = null;

                    }
                    try {
                        if (mediaPlayer != null) {
                            mediaPlayer.release();
                            mediaPlayer = null;
                        }
                        mediaPlayer = new MediaPlayer();
                        mediaPlayer.setDataSource(url);
                        mediaPlayer.prepare();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        mediaPlayer = new MediaPlayer();
                        mediaPlayer.setDataSource(url);
                        mediaPlayer.prepare();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (mediaPlayer != null) {
                    mediaPlayer.start();
                }
            }

        }
    }


    private void playVoice(String speakText) {
        if (!TextUtils.isEmpty(speakText)) {
            NetHandler.Companion.getInst().playVoice(speakText);
        }
    }
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.im_ly:
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    popupWindow = DialogUtils.showToalkPop(getActivity(), re_top);
                    if (SpeechRecognizerHandler.speechRecognizerHandler != null) {
                        if (SpeechRecognizerHandler.speechRecognizerHandler.isOpen()) {
                            SpeechRecognizerHandler.speechRecognizerHandler.stopAudioInput();
                        }
                    }
                    initRecorder();
                    startRecorder();
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (popupWindow != null && popupWindow.isShowing()) {
                        popupWindow.dismiss();
                        stopRecorder();
                        if (SpeechRecognizerHandler.speechRecognizerHandler != null) {
                            if (!SpeechRecognizerHandler.speechRecognizerHandler.isOpen()) {
                                SpeechRecognizerHandler.speechRecognizerHandler.startAudioInput();
                            }
                        }
                        final File file = new File(filePath);
                        new Thread() {
                            @Override
                            public void run() {
                                super.run();
                                up(file);
                            }
                        }.start();

                    }
                }
                break;
        }

        return true;
    }

    /**
     * 准备录音
     */
    private void initRecorder() {
        recorder = new MediaRecorder();
        /* 设置音频源*/
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        /* 设置输出格式*/
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        /* 设置音频编码器，注意这里，设置成AAC，不然苹果手机播放不了*/
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
//        try {
//            /* 创建一个临时文件，用来存放录音*/
//            tempFile = File.createTempFile("tempFile", ".WAV", SDPathDir);
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        File file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        /* 设置录音文件*/
        recorder.setOutputFile(file.getAbsolutePath());
    }

    /**
     * 开始录音
     */
    private void startRecorder() {
        if (!isSDCardExit) {
            Toast.makeText(getActivity(), "ddddd", Toast.LENGTH_LONG).show();
            return;
        }
        try {
            recorder.prepare();
            recorder.start();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 停止录音
     */
    private void stopRecorder() {
        if (recorder != null) {
            try {
                recorder.stop();
            } catch (Exception e) {
                // TODO 如果当前java状态和jni里面的状态不一致，
                //e.printStackTrace();
                recorder = null;
                recorder = new MediaRecorder();
            }
            recorder.release();
            recorder = null;
        }
    }

    private void up(File file) {
        NetHandler.Companion.getInst().uploadVoice(file)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UploadResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(UploadResponse uploadResponse) {
                        if (uploadResponse.getResult() != null) {
                            sendConsultAsync(uploadResponse.getResult().getUrl(), "");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @SuppressLint("CheckResult")
    private void sendConsultAsync(String audioUrl, String msgContent) {
        Map<String, String> map = new HashMap<>();
        int senconde = 0;
        try {
            if (!TextUtils.isEmpty(audioUrl)) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                mediaPlayer.setDataSource(filePath);
                mediaPlayer.prepare();
                senconde = mediaPlayer.getDuration() / 1000;
            }
            map.put("fromId", SharedPreUtils.getInstance(getActivity()).getValue(SharedPreUtils.USER_ID, ""));
            map.put("fromType", "Wristband");
            map.put("toId", toId);
            map.put("msgContent", msgContent);
            map.put("audioUrl", audioUrl);
            map.put("audioSecond", senconde + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        final int ss = senconde;
        NetHandler.Companion.getInst().sendMessageByLiuYan(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SendMessageByLiuYanRespoonse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }
                    @Override
                    public void onNext(SendMessageByLiuYanRespoonse uploadResponse) {
                        GetMessageResponse.ListBean listBean = new GetMessageResponse.ListBean();
                        if (TextUtils.isEmpty(msgContent)){
                            listBean.setIsAudio(true);
                        }else {
                            listBean.setIsAudio(false);
                        }
                        listBean.setIsMine(true);
                        listBean.setIsRead(false);
                        listBean.setMsgContent(msgContent);
                        listBean.setAudioUrl(audioUrl);
                        listBean.setAudioSecond(ss);
                        adapter.getList().add(listBean);
                        adapter.notifyDataSetChanged();
                        recly_view.scrollToPosition(adapter.getItemCount() - 1);
                    }
                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });

    }

    /**
     * 设置为已读
     * @param list
     */
    private void setToRead(List<String> list) {
        if (list == null || list.size() == 0)
            return;
        NetHandler.Companion.getInst().setReaded(new Gson().toJson(list))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CreatRemindResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }
                    @Override
                    public void onNext(CreatRemindResponse uploadResponse) {
                    }
                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}


