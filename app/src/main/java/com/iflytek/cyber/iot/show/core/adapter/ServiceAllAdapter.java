package com.iflytek.cyber.iot.show.core.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.response.ServiceBean;
import com.iflytek.cyber.iot.show.core.utils.GlideUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 服务城公用的适配器
 */
public class ServiceAllAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<ServiceBean.ResultBean> list = new ArrayList<>();
    private int type = 0;//0、1、2、3、4、5、6、7


    public ServiceAllAdapter(Context context) {
        this.context = context;
    }

    public int getType() {
        return type;
    }

    public ServiceAllAdapter setType(int type) {
        this.type = type;
        return this;
    }

    public void setData(List<ServiceBean.ResultBean> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();

    }

    public List<ServiceBean.ResultBean> getList() {
        return list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        RecyclerView.ViewHolder vHodler = null;
        View view = null;
        switch (type) {
            case 0:
                view = LayoutInflater.from(context).inflate(R.layout.item_all_service, viewGroup, false);
                RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(RecyclerView.LayoutParams.WRAP_CONTENT,
                        RecyclerView.LayoutParams.MATCH_PARENT);
                params.leftMargin=90;
                view.setLayoutParams(params);
                vHodler = new ServiceAllAdapter.VHodler(view);
                break;
        }

        return vHodler;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vHodler, final int position) {
        ((VHodler) vHodler).tv_title.setText(list.get(position).getTitle());
        ((VHodler) vHodler).tv_content.setText(list.get(position).getIntro());
        ((VHodler) vHodler).tv_price.setText("￥"+list.get(position).getPrice()+"/人");
        GlideUtil.loadImage(context, list.get(position).getImageUrl(), 0, ((VHodler) vHodler).iv_cover);


    }

    @Override
    public int getItemViewType(int position) {
        return type;
    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    public class VHodler extends RecyclerView.ViewHolder {
        public TextView tv_title;
        public ImageView iv_cover;
        public TextView tv_content;
        public TextView tv_price;

        public VHodler(@NonNull View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            iv_cover = itemView.findViewById(R.id.iv_cover);
            tv_content = itemView.findViewById(R.id.tv_content);
            tv_price = itemView.findViewById(R.id.tv_price);

        }
    }

}
