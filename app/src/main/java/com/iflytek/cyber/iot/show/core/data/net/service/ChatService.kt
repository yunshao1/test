package com.iflytek.cyber.iot.show.core.data.net.service

import com.iflytek.cyber.iot.show.core.data.net.NetConfig
import com.iflytek.cyber.iot.show.core.data.net.response.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Kaka on 19/1/10.
 */
interface ChatService {

    @POST("${NetConfig.API_CHAT_BASE_URL}/group/JoinGroup")
    @Headers("Content-Type: application/json")
    fun joinGroup(@Body requestBody: RequestBody): Observable<BaseResponse<JoinGroupResponse>>

    @POST("${NetConfig.API_CHAT_BASE_URL}/group/SendMessage")
    @Headers("Content-Type: application/json")
    fun sendMessage(@Body requestBody: RequestBody): Observable<BaseResponse<JoinGroupResponse>>

    @POST("${NetConfig.API_CHAT_BASE_URL}/group/SendUserMessage")
    @Headers("Content-Type: application/json")
    fun sendUserMessage(@Body requestBody: RequestBody): Observable<BaseResponse<JoinGroupResponse>>

    @GET("/api/Chat/GetFriendTypeGroupId")
    fun fetchGroupId(@Query("FriendType") type: Int): Observable<BaseResponse<ChatGroupBean>>

    @GET("/api/Chat/GetBook")
    fun fetchGroupId2(@Query("FriendType") type: Int): Observable<BaseResponse<List<ChatGroupBean2>>>
}
