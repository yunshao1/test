package com.iflytek.cyber.iot.show.core.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.response.ClassifyBean;
import com.iflytek.cyber.iot.show.core.utils.GlideUtil;

import java.util.List;

public class PharmacySecondAdapter extends RecyclerView.Adapter<PharmacySecondAdapter.VHodler> {

    private Context context;
    private List<ClassifyBean.ListBeanX.ListBean> list;


    public PharmacySecondAdapter(Context context, List<ClassifyBean.ListBeanX.ListBean> list) {
        this.context = context;
        this.list = list;
    }


    @NonNull
    @Override
    public PharmacySecondAdapter.VHodler onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        PharmacySecondAdapter.VHodler vHodler = null;
        vHodler = new PharmacySecondAdapter.VHodler(LayoutInflater.from(context).inflate(R.layout.item_fragment_second_pharmacy, viewGroup, false));
        return vHodler;
    }

    @Override
    public void onBindViewHolder(@NonNull PharmacySecondAdapter.VHodler vHodler, final int position) {
        vHodler.tv_name.setText(list.get(position).getName());
        GlideUtil.loadImage(context, list.get(position).getImg(), R.drawable.icon_error_image, vHodler.image);
        vHodler.ll_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener!=null){
                    onItemClickListener.onItemClick(list.get(position));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    public class VHodler extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView tv_name;
        public LinearLayout ll_click;

        public VHodler(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            tv_name = itemView.findViewById(R.id.tv_name);
            ll_click = itemView.findViewById(R.id.ll_click);

        }
    }
    public interface  OnItemClickListener{
        void onItemClick(ClassifyBean.ListBeanX.ListBean bean);
    }
    public  OnItemClickListener onItemClickListener;
    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

}
