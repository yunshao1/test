package com.iflytek.cyber.iot.show.core.chat.manager;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.iflytek.cyber.iot.show.core.TRTCMainActivity;
import com.iflytek.cyber.iot.show.core.chat.ChatDetailMessage;
import com.iflytek.cyber.iot.show.core.chat.SessionParamBean;
import com.iflytek.cyber.iot.show.core.chat.UserMessageParamBean;
import com.iflytek.cyber.iot.show.core.chat.event.ChatEvent;
import com.iflytek.cyber.iot.show.core.data.local.LoginSession;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.BaseResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.JoinGroupResponse;
import com.iflytek.cyber.iot.show.core.utils.Constant;
import com.iflytek.cyber.iot.show.core.utils.ToastUtil;
import com.tencent.trtc.TRTCCloudDef;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * @author cloud.wang
 * @date 2019-08-03
 * @Description: TODO
 */
public class VideoManager {

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    private VideoManager() {

    }

    public static VideoManager getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        static VideoManager INSTANCE = new VideoManager();
    }

    public void initiateVideoCall(Context context, int roomId, String notifyId, Map<String, String> paramMap, RequestCallBack<JoinGroupResponse> callBack) {
        sendNotifyMessage(context, roomId, notifyId, paramMap, callBack);
    }

    public void initiateVideoCall(Context context, int roomId, String notifyId, Map<String, String> paramMap) {
        sendNotifyMessage(context, roomId, notifyId, paramMap, null);
    }

    private void sendNotifyMessage(Context context, int roomId, String notifyId, Map<String, String> paramMap, RequestCallBack<JoinGroupResponse> callBack) {
        UserMessageParamBean paramBean = new UserMessageParamBean();
        paramBean.setToId(notifyId);
        paramBean.setToName(paramMap.get("toName"));
        paramBean.setMsgType("txt");

        ChatDetailMessage detailMessage = new ChatDetailMessage();
        detailMessage.setMsgType("txt");
        detailMessage.setMsgContent(roomId + "");
        detailMessage.setVideoState(paramMap.get("video_status"));
        detailMessage.setIconImg(LoginSession.INSTANCE.getSUserAvatar());
        detailMessage.setNickname(LoginSession.INSTANCE.getSUserName());

        Disposable disposable = NetHandler.Companion.getInst().sendUserMessage(paramBean, detailMessage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<BaseResponse<JoinGroupResponse>>() {
                               @Override
                               public void accept(BaseResponse<JoinGroupResponse> joinGroupResponse) throws Exception {
                                   Log.i("Cloud", "JoinGroupResponse " + joinGroupResponse.toString());
                                   if (joinGroupResponse.isSuccess()) {
                                       if (!"2".equals(paramMap.get("video_status"))) {
                                           onJoinRoom(context, roomId, paramMap, notifyId);
                                       }
                                       if (callBack != null) {
                                           callBack.onSuccess(joinGroupResponse.result);
                                       }
                                   } else {
                                       if (callBack != null) {
                                           callBack.onFailed(joinGroupResponse.message);
                                       }
                                       ToastUtil.showLongToastCenter("发送唤醒消息失败，请重新发起通话,服务器返回错误"+joinGroupResponse.message);
                                   }
                               }
                           }, new Consumer<Throwable>() {
                               @Override
                               public void accept(Throwable throwable) throws Exception {
                                   if (callBack != null) {
                                       callBack.onFailed(throwable.getMessage());
                                   }
                                   ToastUtil.showLongToastCenter("发送唤醒消息失败，请重新发起通话"+throwable.getMessage());
                               }
                           }
                );

//        SessionParamBean param = new SessionParamBean(notifyId);
//        param.setGroupType(4);
//        Disposable disposable = NetHandler.Companion.getInst().joinGroup(param)
//                .subscribeOn(Schedulers.io())
//                .flatMap(new Function<BaseResponse<JoinGroupResponse>, ObservableSource<BaseResponse<JoinGroupResponse>>>() {
//                    @Override
//                    public ObservableSource<BaseResponse<JoinGroupResponse>> apply(BaseResponse<JoinGroupResponse> joinGroupResponse) throws Exception {
//                        if (!joinGroupResponse.isSuccess()) {
//                            return Observable.error(new Throwable("加入指定人的组失败，请重试"));
//                        }
//
//                        SessionParamBean paramBean = new SessionParamBean(notifyId);
//                        paramBean.setGroup(notifyId);
//                        paramBean.setGroupType(4);
//                        paramBean.setMsgType("txt");
//                        ChatDetailMessage detailMessage = new ChatDetailMessage();
//                        detailMessage.setMsgType("txt");
//                        detailMessage.setMsgContent(roomId + "");
//                        detailMessage.setVideoState(paramMap.get(Constant.VIDEO_STATUS)==null?0:Integer.parseInt(paramMap.get(Constant.VIDEO_STATUS)));
//                        detailMessage.setIconImg(LoginSession.INSTANCE.getSUserAvatar());
//                        detailMessage.setNickname(LoginSession.INSTANCE.getSUserName());
//
//                        return NetHandler.Companion.getInst().sendMessage(paramBean, detailMessage);
//                    }
//                })
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<BaseResponse<JoinGroupResponse>>() {
//                               @Override
//                               public void accept(BaseResponse<JoinGroupResponse> joinGroupResponse) throws Exception {
//                                   Log.i("Cloud", "JoinGroupResponse " + joinGroupResponse.toString());
//                                   if (joinGroupResponse.isSuccess()) {
//                                       if (!"2".equals(paramMap.get(Constant.VIDEO_STATUS)))
//                                       onJoinRoom(context, roomId, paramMap,notifyId);
//
//                                   }
//                               }
//                           }, new Consumer<Throwable>() {
//                               @Override
//                               public void accept(Throwable throwable) throws Exception {
//                                   Toast.makeText(context,"JoinGroupResponse failed"+throwable.getMessage(),Toast.LENGTH_LONG).show();
//                                   Log.i("Cloud", "JoinGroupResponse failed " + throwable.getMessage());
//                               }
//                           }
//                );
        mCompositeDisposable.add(disposable);
    }

    private void onJoinRoom(Context context, final int roomId, Map<String, String> paramMap, String notifyid) {
        final Intent intent = new Intent(context, TRTCMainActivity.class);
        String userSig = LoginSession.INSTANCE.getSUserSig();
        String userId = LoginSession.INSTANCE.getSUserId();
        intent.putExtra("roomId", roomId);
        intent.putExtra("userId", userId);
        intent.putExtra("fromId", notifyid);
        intent.putExtra("AppScene", TRTCCloudDef.TRTC_APP_SCENE_VIDEOCALL);
        intent.putExtra("customVideoCapture", false);
        intent.putExtra("videoFile", false);
        intent.putExtra("sdkAppId", Constant.APP_ID);
        intent.putExtra("userSig", userSig);
        intent.putExtra("groupId", notifyid);
        intent.putExtra("active", paramMap.get("active") == null ? "" : paramMap.get("active"));
        intent.putExtra("img", paramMap.get("img") == null ? "" : paramMap.get("img"));
        intent.putExtra("name", paramMap.get("groupName") == null ? "" : paramMap.get("groupName"));

        context.startActivity(intent);
    }

    public void clearDisposable() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }

        EventBus.getDefault().unregister(this);
    }

    public interface RequestCallBack<T> {
        void onSuccess(T t);

        void onFailed(String msg);
    }
}
