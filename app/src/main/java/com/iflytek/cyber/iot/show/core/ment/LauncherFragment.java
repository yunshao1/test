package com.iflytek.cyber.iot.show.core.ment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.navigation.fragment.NavHostFragment;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.chat.ChatService;
import com.iflytek.cyber.iot.show.core.chat.SessionParamBean;
import com.iflytek.cyber.iot.show.core.chat.event.ChatEvent;
import com.iflytek.cyber.iot.show.core.data.local.LoginSession;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.Empty;
import com.iflytek.cyber.iot.show.core.data.net.response.HomeUnderReadMessageResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.LoginBean;
import com.iflytek.cyber.iot.show.core.data.net.response.Response;
import com.iflytek.cyber.iot.show.core.fragment.BaseFragment;
import com.iflytek.cyber.iot.show.core.utils.DateUtils;
import com.iflytek.cyber.iot.show.core.utils.SharedPreUtils;
import com.iflytek.cyber.iot.show.core.utils.ToastUtil;
import com.iflytek.cyber.iot.show.core.view.viewpage.HomeAdInfo;
import com.iflytek.cyber.iot.show.core.view.viewpage.JazzyPagerAdapter;
import com.iflytek.cyber.iot.show.core.view.viewpage.JazzyPagerlistener;
import com.iflytek.cyber.iot.show.core.view.viewpage.JazzyViewPager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static com.iflytek.cyber.iot.show.core.chat.event.ChatEvent.EVENT_CREATE_NOTIFY_SESSION;

public class LauncherFragment extends BaseFragment implements View.OnClickListener {

    //在程序未完全退出时，登录过一次后不再重新登录
    private static boolean isLogin = false;
    JazzyViewPager jvp_the_second;
    LinearLayout home_sencond_viewGroup;
    private ImageView[] tipsThSencond;
    private List<HomeAdInfo> bannerList = new ArrayList<>();
    // 计时器任务
    private ImageTimerTask theSecondTimeTaks;
    // 计时器
    private Timer theSecondGallery;
    // 广告栏滚动时间
    private int PhotoChangeSecondtime = 6000;

    //第一个添加原点标志
    private boolean theSecondadvertiseAddPointFlag = false;


    private View ll_more;
    private View ll_notifi;
    private View ll_help;
    private TextView tv_notifi_name;
    private TextView tv_help_content;
    private boolean isOpen = true;

    private View ll_online;
    private ImageView im_video;
    private View ll_service;

    private View ll_comm;
    private View ll_order;
    private View ll_health;

    private TextView tv_time;
    private TextView tv_date;
//    private View view = null;
private Myrunabel myrunabel;
    private boolean isRun = true;

    private final Handler mHandler = new Handler(Looper.getMainLooper());
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private boolean isClickSOS = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_launcher, container, false);

        initView(view);
        init();
        myrunabel = new Myrunabel();
        EventBus.getDefault().register(this);
        isRun = true;
        load();
        mHandler.postDelayed(myrunabel, 20000);
        return view;
    }

    private class Myrunabel implements Runnable {
        @Override
        public void run() {
            if (isRun) {

                load();
                mHandler.postDelayed(myrunabel, 10000);
            }

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHandler != null) {
            isRun = false;
            mHandler.removeCallbacksAndMessages(null);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isRun = false;
        if (mHandler!=null){
            mHandler.removeCallbacksAndMessages(null);
        }
    }

    public void load() {
        NetHandler.Companion.getInst().getUnreadMessageAbstract()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<HomeUnderReadMessageResponse>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(HomeUnderReadMessageResponse listResponse) {
                        initDate();
                        if (listResponse != null && listResponse.getList() != null
                                && listResponse.getList().getUnreadMessages() != null) {
                            if (listResponse.getList().getUnreadMessages().size() > 0) {
                                    tv_notifi_name.setText("收到一条来自"+"的留言");
                                tv_notifi_name.setVisibility(View.VISIBLE);
                            } else {
                                tv_notifi_name.setVisibility(View.GONE);
                            }
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initDate();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!isLogin) {
            login();
        } else {
            if (getLauncher() != null) {
                getLauncher().startService(new Intent(getLauncher(), ChatService.class));
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mCompositeDisposable.clear();

//        if (getLauncher() != null) {
//            getLauncher().unbindService(mChatConnection);
//        }

        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }

        EventBus.getDefault().unregister(this);
    }

    private void login() {
        HashMap<String, String> map = new HashMap<>();
//        String name = "lyj";
//        String name = "wzg";
        String name = "zch";
        String pwd = "123456";
        map.put("userName", name);
        map.put("pwd", pwd);
        map.put("appCode", "VoiceBox");
        Disposable disposable = NetHandler.Companion.getInst().login(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<LoginBean>() {
                    @Override
                    public void accept(LoginBean loginBean) throws Exception {
                        if (loginBean.getList() != null && loginBean.getList().getToken() != null) {
                            if (getLauncher() == null) {
                                return;
                            }
                            SharedPreUtils instance = SharedPreUtils.getInstance(getLauncher());
                            instance.saveValue(SharedPreUtils.USER_TOKEN, loginBean.getList().getToken());
                            instance.saveValue(SharedPreUtils.USER_ID, loginBean.getList().getId());
                            instance.saveValue(SharedPreUtils.USER_REAL_NAME, loginBean.getList().getRealName());
                            instance.saveValue(SharedPreUtils.USER_AVATAR, loginBean.getList().getImg());
                            instance.saveValue(SharedPreUtils.USER_SIG, loginBean.getList().getUserSig());
                            instance.saveValue(SharedPreUtils.LOGIN_NAME, name);
                            instance.saveValue(SharedPreUtils.LOGIN_PWD, pwd);

                            LoginSession.INSTANCE.setSUserId(loginBean.getList().getId());
                            LoginSession.INSTANCE.setSUserToken(loginBean.getList().getToken());
                            LoginSession.INSTANCE.setSUserSig(loginBean.getList().getUserSig());
                            LoginSession.INSTANCE.setSUserAvatar(loginBean.getList().getImg());
                            LoginSession.INSTANCE.setSUserName(loginBean.getList().getRealName());

                            getLauncher().startService(new Intent(getLauncher(), ChatService.class));
                            isLogin = true;
                            Log.e("ChatService", "d===" + loginBean.getList());
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        isLogin = false;
                        throwable.printStackTrace();
                        ToastUtil.showLongToastCenter("登录失败," + throwable.getMessage());
                    }
                });
        mCompositeDisposable.add(disposable);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleEvent(ChatEvent chatEvent) {
        if (chatEvent.getMessageType() == ChatEvent.EVENT_FINISH_CONNECTION) {
            //拉取留言/通知等信息
            //监听自身前，需要先joinGroup到自己的组中，并向SingleR请求获取推送的通知消息
//            joinGroup();
            SessionParamBean paramBean = new SessionParamBean(LoginSession.INSTANCE.getSUserId());
            //监听自身，并向SingleR请求获取推送的通知消息
            paramBean.setGroupType(1);
            EventBus.getDefault().post(new ChatEvent(EVENT_CREATE_NOTIFY_SESSION, paramBean));
        }
    }

//    private void joinGroup() {
//        SessionParamBean param = new SessionParamBean(LoginSession.INSTANCE.getSUserId() + "");
//        param.setGroupType(4);
//        Disposable disposable = NetHandler.Companion.getInst().joinGroup(param)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<BaseResponse<JoinGroupResponse>>() {
//                               @Override
//                               public void accept(BaseResponse<JoinGroupResponse> joinGroupResponse) throws Exception {
//                                   Log.i("ChatService", "JoinGroupResponse  " + joinGroupResponse.toString());
//                                   SessionParamBean paramBean = new SessionParamBean(LoginSession.INSTANCE.getSUserId());
//                                   //监听自身，并向SingleR请求获取推送的通知消息
//                                   paramBean.setGroupType(4);
//                                   EventBus.getDefault().post(new ChatEvent(EVENT_CREATE_SESSION, paramBean));
//                               }
//                           }, new Consumer<Throwable>() {
//                               @Override
//                               public void accept(Throwable throwable) throws Exception {
//                                   Log.i("ChatService", "JoinGroupResponse failed " + throwable.getMessage());
//                               }
//                           }
//                );
//        mCompositeDisposable.add(disposable);
//    }

    private void initDate() {
        tv_time.setText(DateUtils.getTime("HH:mm"));
        tv_date.setText(DateUtils.getTime("MM月dd") + " " + DateUtils.getWeekOfDate(new Date()));
    }

    private void initView(View view) {
        setHideBottomBar(false);
        ll_online = view.findViewById(R.id.ll_online);
        ll_service = view.findViewById(R.id.ll_service);
        im_video = view.findViewById(R.id.im_video);
        ll_comm = view.findViewById(R.id.ll_comm);
        ll_order = view.findViewById(R.id.ll_order);
        ll_health = view.findViewById(R.id.ll_health);
        tv_time = view.findViewById(R.id.tv_time);
        tv_date = view.findViewById(R.id.tv_date);

        ll_online.setOnClickListener(this);
        ll_service.setOnClickListener(this);
        im_video.setOnClickListener(this);
        ll_comm.setOnClickListener(this);
        ll_order.setOnClickListener(this);
        ll_health.setOnClickListener(this);


        jvp_the_second = view.findViewById(R.id.jvp_the_second);
        home_sencond_viewGroup = view.findViewById(R.id.home_sencond_viewGroup);
        ll_more = view.findViewById(R.id.ll_more);
        ll_more.setOnClickListener(this);
        ll_notifi = view.findViewById(R.id.ll_notifi);
        ll_notifi.setOnClickListener(this);
        ll_help = view.findViewById(R.id.ll_help);
        ll_help.setOnClickListener(this);
        im_video.setOnClickListener(this);
        tv_notifi_name = view.findViewById(R.id.tv_notifi_name);
        tv_notifi_name.setOnClickListener(this);
        tv_help_content = view.findViewById(R.id.tv_help_content);
    }

    private void init() {
        theSecondGallery = new Timer();
        theSecondTimeTaks = new ImageTimerTask(2);
        bannerList.clear();
        theSecondGallery.scheduleAtFixedRate(theSecondTimeTaks, PhotoChangeSecondtime, PhotoChangeSecondtime);
        bannerList.add(new HomeAdInfo("", 0, ""));
        setupTheSecondJazziness(JazzyViewPager.TransitionEffect.Standard, bannerList);
    }

    // 为广告栏加载数据，配置效果及绑定事件处理类
    private void setupTheSecondJazziness(JazzyViewPager.TransitionEffect effect, List<HomeAdInfo> banner) {

        tipsThSencond = new ImageView[banner.size()];

        if (theSecondadvertiseAddPointFlag) {

            home_sencond_viewGroup.removeAllViews();
        }
        // 根据数据元素个数，动态添加小圆点
        for (int i = 0; i < tipsThSencond.length; i++) {
            ImageView imageView = new ImageView(getActivity());
            imageView.setLayoutParams(new ViewGroup.LayoutParams(5, 5));
            tipsThSencond[i] = imageView;
            if (i == 0) {
                tipsThSencond[i].setBackgroundResource(R.drawable.banner_dot_bg_active);
            } else {
                tipsThSencond[i].setBackgroundResource(R.drawable.banner_dot_bg);
            }
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    new ViewGroup.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));
            layoutParams.leftMargin = 10;
            layoutParams.rightMargin = 10;
            home_sencond_viewGroup.addView(imageView, layoutParams);
            theSecondadvertiseAddPointFlag = true;
        }

        if (tipsThSencond.length == 1) {
            home_sencond_viewGroup.setVisibility(View.INVISIBLE);
            jvp_the_second.setPagingEnabled(false);
        } else {
            home_sencond_viewGroup.setVisibility(View.VISIBLE);
            jvp_the_second.setPagingEnabled(true);
        }

        // 广告栏配置JazzyPagerAdapter
        jvp_the_second.setAdapter(new JazzyPagerAdapter(jvp_the_second, getContext(), banner));

        jvp_the_second.setRemainder(banner.size());
        jvp_the_second.setTransitionEffect(effect);

        // 获取广告栏在设备中的显示宽度，通过长宽比计算出广告栏占用高度


        jvp_the_second.addOnPageChangeListener(new JazzyPagerlistener(banner, tipsThSencond));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_more:
                NavHostFragment.findNavController(LauncherFragment.this).navigate(R.id.action_to_more);
                break;
            case R.id.ll_online:
                NavHostFragment.findNavController(LauncherFragment.this).navigate(R.id.action_to_online_fragment);
                break;
            case R.id.im_video:
                NavHostFragment.findNavController(LauncherFragment.this).navigate(R.id.action_to_video_fragment);
                break;
            case R.id.ll_service:
                NavHostFragment.findNavController(LauncherFragment.this).navigate(R.id.action_to_service);
                break;
            case R.id.ll_comm:
                Bundle bundle = new Bundle();
                bundle.putBoolean("is_online_ask", false);
                NavHostFragment.findNavController(LauncherFragment.this).navigate(R.id.action_to_online_fragment, bundle);
                break;
            case R.id.ll_order:
                Bundle bundle4 = new Bundle();
                bundle4.putString("title", "周边药房");
                bundle4.putString("code", "T-005");
                bundle4.putString("apiUrl", "http://speaker.api.shfusion.com/api/Service/GetByCode");
                NavHostFragment.findNavController(LauncherFragment.this).navigate(R.id.action_to_pharmacy_fragment, bundle4);
                break;
            case R.id.ll_health:
                Bundle bundle3 = new Bundle();
                bundle3.putString("title", "健康膳食");
                bundle3.putString("code", "T-009");
                bundle3.putString("apiUrl", "http://speaker.api.shfusion.com/api/Service/GetByCode");
                NavHostFragment.findNavController(LauncherFragment.this).navigate(R.id.action_to_nutritious_fragment, bundle3);
                break;
            case R.id.tv_notifi_name:
                NavHostFragment.findNavController(LauncherFragment.this).navigate(R.id.action_to_person_list_message_fragment);

                break;
            case R.id.ll_help:
//                if (!isClickSOS) {
//                    isClickSOS = true;
                sendSOS();
//                }
                break;
        }
    }

    // 计时器(正常每隔1秒滚动一次，如遇手工滑动则暂停自动滚动)
    private class ImageTimerTask extends TimerTask {

        int type;

        public ImageTimerTask(int type) {
            this.type = type;
        }

        public void run() {
            switch (type) {
                case 2:
                    if (bannerList.size() != 1) {
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (bannerList != null && bannerList.size() > 1) {
                                    setImageBackground(jvp_the_second.getCurrentItem() % bannerList.size(), tipsThSencond);
                                    jvp_the_second.setCurrentItem(jvp_the_second.getCurrentItem() + 1);
                                }
                            }
                        }, 0);
                    }
                    break;
            }
        }
    }

    /**
     * 设置选中的tip的背景
     * 遍历圆点集体，选中项为白点，其他皆设置灰点
     */
    private void setImageBackground(int selectItems, ImageView[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (i == selectItems) {
                arr[i].setBackgroundResource(R.drawable.banner_dot_bg_active);
            } else {
                arr[i].setBackgroundResource(R.drawable.banner_dot_bg);
            }
        }
    }

    private void sendSOS() {
        NetHandler.Companion.getInst().sendSOS()
                .subscribeOn(Schedulers.io())
                .doOnNext(new Consumer<Response<Empty>>() {
                    @Override
                    public void accept(Response<Empty> emptyResponse) throws Exception {
                        if (emptyResponse.message.equals("success")) {
                            NetHandler.Companion.getInst().playVoice("已为您通知养老服务中心");
                        }
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<Empty>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mCompositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Response<Empty> emptyResponse) {
                        Log.e("Cloud", "onNext:" + emptyResponse.message);
                        if (emptyResponse.message.equals("success")) {
                            isClickSOS = false;
                        }

                        //TODO 会导致无法再次接收数据
//                        mCompositeDisposable.dispose();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Cloud", "onError:" + e.getMessage());
//                        mCompositeDisposable.dispose();
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

}
