package com.iflytek.cyber.iot.show.core.widget.recyclerview;

import android.util.Log;
import android.view.View;

import com.iflytek.cyber.iot.show.core.BuildConfig;

/**
 * Created by chensuilun on 2016/12/16.
 */
public class AlphaTransformer implements GalleryLayoutManager.ItemTransformer {

    private static final String TAG = "AlphaTransformer";

    @Override
    public void transformItem(GalleryLayoutManager layoutManager, View item, float fraction) {
        //以圆心进行渐变
        item.setPivotX(item.getWidth() / 2.f);
        item.setPivotY(item.getHeight()/2.0f);
        float alpha = 1 - 0.6f * Math.abs(fraction);
        if (BuildConfig.DEBUG) {
//            Log.e(TAG, "transformItem: alpha:" + alpha + "  fraction:" + fraction);
        }
        item.setAlpha(alpha);
//        item.setScaleX(scale);
//        item.setScaleY(scale);
    }
}
