package com.iflytek.cyber.iot.show.core.data.net.response;

public class VideoMessageContent {

    /**
     * iconImg : http://resource.shfusion.com/image/common/elder/user.jpg
     * msgContent : 123
     * msgType : txt
     * nickname : 刘以玖
     */

    private String iconImg;
    private String msgContent;
    private String msgType;
    private String nickname;

    public String getIconImg() {
        return iconImg;
    }

    public void setIconImg(String iconImg) {
        this.iconImg = iconImg;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
