package com.iflytek.cyber.iot.show.core.data.net.response;

import java.util.List;

public class ByClassifyIdBean {


    private String message;
    private List<String> tryTxt;
    private List<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getTryTxt() {
        return tryTxt;
    }

    public void setTryTxt(List<String> tryTxt) {
        this.tryTxt = tryTxt;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {

        private String id;
        private String name;
        private String imageUrl;
        private String shopName;
        private boolean isInsurance;
        private String price;
        private String drugClassify;
        private String indication;
        private String specification;
        private String readTxt = "";
        private List<String> tryTxt;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getShopName() {
            return shopName;
        }

        public void setShopName(String shopName) {
            this.shopName = shopName;
        }

        public boolean isIsInsurance() {
            return isInsurance;
        }

        public void setIsInsurance(boolean isInsurance) {
            this.isInsurance = isInsurance;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDrugClassify() {
            return drugClassify;
        }

        public void setDrugClassify(String drugClassify) {
            this.drugClassify = drugClassify;
        }

        public String getIndication() {
            return indication;
        }

        public void setIndication(String indication) {
            this.indication = indication;
        }

        public String getSpecification() {
            return specification;
        }

        public void setSpecification(String specification) {
            this.specification = specification;
        }

        public String getReadTxt() {
            return readTxt;
        }

        public void setReadTxt(String readTxt) {
            this.readTxt = readTxt;
        }

        public List<String> getTryTxt() {
            return tryTxt;
        }

        public void setTryTxt(List<String> tryTxt) {
            this.tryTxt = tryTxt;
        }
    }
}
