package com.iflytek.cyber.iot.show.core.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.response.AskHealthBean;

import java.util.List;

public class AskHealthAdapter extends RecyclerView.Adapter<AskHealthAdapter.VHodler> {

    private Context context;
    private List<AskHealthBean.ListBean> list;


    public AskHealthAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<AskHealthBean.ListBean> list) {
        this.list = list;
        notifyDataSetChanged();

    }


    @NonNull
    @Override
    public AskHealthAdapter.VHodler onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        VHodler vHodler = null;
        vHodler = new VHodler(LayoutInflater.from(context).inflate(R.layout.item_fragment_ask_health, viewGroup, false));
        return vHodler;
    }

    @Override
    public void onBindViewHolder(@NonNull AskHealthAdapter.VHodler vHodler, final int position) {
        vHodler.tv_title.setText(list.get(position).getName());
        vHodler.tv_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener!=null){
                    onItemClickListener.onItemClick(list.get(position));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    public class VHodler extends RecyclerView.ViewHolder {
        public TextView tv_title;

        public VHodler(@NonNull View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
        }
    }
    public interface  OnItemClickListener{
        void onItemClick(AskHealthBean.ListBean bean);
    }
    public  OnItemClickListener onItemClickListener;
    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }
}
