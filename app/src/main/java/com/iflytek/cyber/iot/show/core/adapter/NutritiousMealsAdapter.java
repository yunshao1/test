package com.iflytek.cyber.iot.show.core.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.response.NutritiousMealsResonse;
import com.iflytek.cyber.iot.show.core.data.net.response.ServiceBean;
import com.iflytek.cyber.iot.show.core.fragment.NutritiousDialogFragment;
import com.iflytek.cyber.iot.show.core.utils.GlideUtil;

import java.util.List;
/**
 * 营养配餐适配器
 */
public class NutritiousMealsAdapter extends RecyclerView.Adapter<NutritiousMealsAdapter.VHodler> {

    private Context context;
    private List<NutritiousMealsResonse.ListBean> list;
    private FragmentManager fragmentManager;

    public NutritiousMealsAdapter(Context context, FragmentManager manager) {
        this.context = context;
        this.fragmentManager = manager;
    }

    public void setData(List<NutritiousMealsResonse.ListBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public NutritiousMealsAdapter.VHodler onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        NutritiousMealsAdapter.VHodler vHodler = null;
        vHodler = new NutritiousMealsAdapter.VHodler(LayoutInflater.from(context).inflate(R.layout.item_fragment_meals, viewGroup, false));
        vHodler.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new NutritiousDialogFragment().show(fragmentManager, "Dialog Nutritious");
            }
        });
        return vHodler;
    }

    @Override
    public void onBindViewHolder(@NonNull NutritiousMealsAdapter.VHodler vHodler, final int position) {
        vHodler.tv_title.setText(list.get(position).getName());
        GlideUtil.loadImage(context, list.get(position).getPicture(), R.drawable.icon_error_image, vHodler.iv_cover);
        vHodler.tv_price.setText(list.get(position).getPrice());
    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    public class VHodler extends RecyclerView.ViewHolder {
        public TextView tv_title;
        public ImageView iv_cover;
        public TextView tv_price;

        public VHodler(@NonNull View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            iv_cover = itemView.findViewById(R.id.iv_cover);
            tv_price = itemView.findViewById(R.id.tv_price);

        }
    }

}
