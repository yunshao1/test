package com.iflytek.cyber.iot.show.core.data.net.response;

import java.util.List;

public class HealthReportBean {


    private String message;
    private String tryTxt;
    private List<ResultBean> list;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTryTxt() {
        return tryTxt;
    }

    public void setTryTxt(String tryTxt) {
        this.tryTxt = tryTxt;
    }

    public List<ResultBean> getResult() {
        return list;
    }

    public void setResult(List<ResultBean> result) {
        this.list = result;
    }

    public static class ResultBean {
        /**
         * id : 1
         * collectionTime : 2019-05-28T00:00:00
         * title : 综合报告单
         * summary : 尿常规提示:尿白细胞 ++;镜检白细胞 ++Cell/HP。建议内科随诊。
         * imageUrl : http://resource.shfusion.com/image/health/form/cmtjfm.jpg
         * readTxt : 2019年5月28日综合报告单，健康建议：尿常规提示:尿白细胞 ++;镜检白细胞 ++Cell/HP。建议内科随诊。
         * timePassed : 1周前
         */

        private String id;
        private String collectionTime;
        private String title;
        private String summary;
        private String attachments;
        private String readTxt = "";
        private String timePassed;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCollectionTime() {
            return collectionTime;
        }

        public void setCollectionTime(String collectionTime) {
            this.collectionTime = collectionTime;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public String getImageUrl() {
            return attachments;
        }

        public void setImageUrl(String imageUrl) {
            this.attachments = imageUrl;
        }

        public String getReadTxt() {
            return readTxt;
        }

        public void setReadTxt(String readTxt) {
            this.readTxt = readTxt;
        }

        public String getTimePassed() {
            return timePassed;
        }

        public void setTimePassed(String timePassed) {
            this.timePassed = timePassed;
        }
    }
}
