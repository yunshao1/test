package com.iflytek.cyber.iot.show.core.data.net

/**
 * @author cloud.wang
 * @date 2019-07-20
 * @Description: TODO
 */
object NetConfig {

    public const val API_CHAT_BASE_URL = "http://message.api.shfusion.com"

    public const val API_CHAT_SERVER_URL = "$API_CHAT_BASE_URL/messageHub"

}