package com.iflytek.cyber.iot.show.core.data.net.response;

public class LoginBean {

    private String message;
    private ListBean list;
    private String tryTxt;
    private String readTxt;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ListBean getList() {
        return list;
    }

    public void setList(ListBean list) {
        this.list = list;
    }

    public String getTryTxt() {
        return tryTxt;
    }

    public void setTryTxt(String tryTxt) {
        this.tryTxt = tryTxt;
    }

    public String getReadTxt() {
        return readTxt;
    }

    public void setReadTxt(String readTxt) {
        this.readTxt = readTxt;
    }

    public static class ListBean {
        /**
         * "id": "6190",
         * name : lyj
         * img : http://elderlyservice.shfusion.com/Image/newTow/avatar1@2x.jpg
         * realName : 刘以玖
         * token : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBY2NvdW50Ijoie1wiSWRcIjpcIjYxOTBcIixcIk5hbWVcIjpcImx5alwiLFwiQWNjb3VudFR5cGVcIjowLFwiUmVsYXRlXCI6XCI2MTlcIixcIkFwcENvZGVcIjowLFwiUmVhbE5hbWVcIjpcIuWImOS7peeOllwifSIsImV4cCI6MTU2MzA4MTc4NCwiaXNzIjoibWUiLCJhdWQiOiJ5b3UifQ.VF1cIEbn2gul0PRBtITGEDWRKDKlAVgJz7-S-fAwl4Q
         * needChangePwd : true
         * changePwdReason : 请修改初始密码
         */
        private String id;
        private String name;
        private String img;
        private String realName;

        public String getUserSig() {
            return userSig;
        }

        public void setUserSig(String userSig) {
            this.userSig = userSig;
        }

        private String userSig;
        private String token;
        private boolean needChangePwd;
        private String changePwdReason;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getRealName() {
            return realName;
        }

        public void setRealName(String realName) {
            this.realName = realName;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public boolean isNeedChangePwd() {
            return needChangePwd;
        }

        public void setNeedChangePwd(boolean needChangePwd) {
            this.needChangePwd = needChangePwd;
        }

        public String getChangePwdReason() {
            return changePwdReason;
        }

        public void setChangePwdReason(String changePwdReason) {
            this.changePwdReason = changePwdReason;
        }

        @Override
        public String toString() {
            return "ListBean{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", img='" + img + '\'' +
                    ", realName='" + realName + '\'' +
                    ", userSig='" + userSig + '\'' +
                    ", token='" + token + '\'' +
                    ", needChangePwd=" + needChangePwd +
                    ", changePwdReason='" + changePwdReason + '\'' +
                    '}';
        }
    }
}
