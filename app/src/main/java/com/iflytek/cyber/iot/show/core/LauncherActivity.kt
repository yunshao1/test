/*
 * Copyright (C) 2018 iFLYTEK CO.,LTD.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iflytek.cyber.iot.show.core

import android.Manifest
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.*
import android.support.v4.app.ActivityCompat
import android.support.v4.content.PermissionChecker
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import cn.iflyos.iace.core.PlatformInterface
import cn.iflyos.iace.iflyos.AuthProvider
import cn.iflyos.iace.iflyos.IflyosClient
import com.google.gson.Gson
import com.iflytek.cyber.iot.show.core.chat.ChatDetailMessage
import com.iflytek.cyber.iot.show.core.chat.ChatResponse
import com.iflytek.cyber.iot.show.core.chat.ChatService
import com.iflytek.cyber.iot.show.core.chat.event.ChatEvent
import com.iflytek.cyber.iot.show.core.data.local.LoginSession
import com.iflytek.cyber.iot.show.core.data.net.service.MyRemindService
import com.iflytek.cyber.iot.show.core.impl.Logger.LogEntry
import com.iflytek.cyber.iot.show.core.impl.Logger.LoggerHandler
import com.iflytek.cyber.iot.show.core.impl.SpeechRecognizer.SpeechRecognizerHandler
import com.iflytek.cyber.iot.show.core.utils.*
import com.iflytek.cyber.iot.show.core.utils.scheduler.TaskScheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_launcher.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject
import java.lang.ref.SoftReference
import java.lang.ref.WeakReference
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashSet

class LauncherActivity : AppCompatActivity(), Observer {

    /** 科大讯飞语音SDK基于的Service */
    var mEngineService: EngineService? = null

    /** 来自Fragment的观察者 */
    private val observerSet = HashSet<Observer>()

    //    private val longPressHandler = LongPressHandler(this)
    private var mCompositeDisposable = CompositeDisposable()

    /** 网络是否可用 */
    private var isNetworkAvailable: Boolean = false
        get() {
            return if (Build.VERSION.SDK_INT < 21) {
                ConnectivityUtils.isNetworkAvailable(this)
            } else {
                field
            }
        }

    fun setViewText(text: String, isHideBottomBar: Boolean, styleId: Int?) {
        if (isHideBottomBar) {
            if (styleId != null && styleId > 0) {
                tv_bottom_tip.setTextAppearance(baseContext, styleId)
            }
            tv_bottom_tip.visibility = View.VISIBLE
            tv_bottom_tip?.text = text
        } else {
            tv_bottom_tip.visibility = View.GONE
        }
    }

    /** 相关的错误码 */
    private var mStatusType = StatusType.NORMAL

    companion object {
        const val EXTRA_TEMPLATE = "template"

        private const val IAT_OFFSET = 2000L
        //        private const val sTag = "LauncherActivity"
        private const val sTag = "Cloud"

        private const val REQUEST_PERMISSION_CODE = 1001
    }

    enum class StatusType {
        AUTHORIZE_ERROR,
        NETWORK_ERROR,
        NORMAL,
        RETRYING,
        SERVER_ERROR,
        TIMEOUT_ERROR,
        UNKNOWN_ERROR,
        UNSUPPORTED_DEVICE_ERROR,
    }

    fun addObserver(observer: Observer) {
        observerSet.add(observer)
    }

    fun deleteObserver(observer: Observer) {
        observerSet.remove(observer)
    }

    /**
     * 监听来自EngineService中LoggerHandler分发的实时通知,运行在"异步线程"中
     */
    override fun update(observable: Observable?, arg: Any?) {
        dispatchMessageToFragment(observable, arg)

        if (observable !is LoggerHandler.LoggerObservable) {
            Log.e(sTag, "not come from LoggerHandler.LoggerObservable")
            return
        }

        if (arg !is LogEntry) {
            Log.e(sTag, "args must not be LogEntry")
            return
        }

        try {
            val message = arg.json
            val template = message.getJSONObject("template")

            when (arg.type) {

                        //AuthProviderHandler授权状态改变
                LoggerHandler.AUTH_LOG -> {
                    handleAuthLog(template)


                }
                //SpeechRecognizerHandler中AudioReader
                LoggerHandler.RECORD_VOLUME -> {
//                    Log.e("234","template888==="+arg.type)
                    //Log.i(sTag, "讯飞返回结果： RECORD_VOLUME 录音中...")
                }
                //IflyosClientHandler中OnIntermediaText
                LoggerHandler.IAT_LOG -> {
//                    Log.i(sTag, "讯飞返回结果： IAT_LOG****$message")
                    showVoiceBar(template.getString("text"))
                }
                //IflyosClientHandler中OnCustomDirective返回自定义的指令集
                LoggerHandler.CUSTOM_DIRECTIVE -> {
//                    Log.i(sTag,"当前的内容是 AUTH_LOG_DIDECTIVE ::$message")
                    dispatchCustomDirective(template)
                }
                //IflyosClientHandler中connectionStatusChanged
                LoggerHandler.CONNECTION_STATE -> {
                    changConnectionState(template)
                }
                //IflyosClientHandler中dialogStateChanged
                LoggerHandler.DIALOG_STATE -> {
                    changeDialogState(template)
                }
                //跳转天气界面
                LoggerHandler.WEATHER_TEMPLATE -> {
//                    Log.i(sTag, "讯飞返回结果： WEATHER_TEMPLATE****")
                }
                //以下几个跳转对应界面
                LoggerHandler.BODY_TEMPLATE1, LoggerHandler.BODY_TEMPLATE2 -> {
//                    Log.i(sTag, "讯飞返回结果： BODY_TEMPLATE1 BODY_TEMPLATE2****")
                }
                LoggerHandler.BODY_TEMPLATE3 -> {
                    Log.i(sTag, "讯飞返回结果： BODY_TEMPLATE3 ****")
                }
                LoggerHandler.LIST_TEMPLATE1 -> {
                    Log.i(sTag, "讯飞返回结果： LIST_TEMPLATE1****")
                }
                //异常Log，比如：Token 过期或已取消绑定
                LoggerHandler.EXCEPTION_LOG -> {
                    val code = template.getString("code")
//                    Log.i(sTag, "讯飞返回结果： EXCEPTION_LOG$code****")
                    when (code) {
                        "UNAUTHORIZED_EXCEPTION" -> {
                            // Token 过期或已取消绑定
                            //TODO 这里需要根据不同的mStatusType，给出对应的"错误提示"
                            mStatusType = StatusType.AUTHORIZE_ERROR
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun changeDialogState(template: JSONObject) {
        val state = template.optString("state")
        Log.e("234","template222111==="+template)
//        Log.i(sTag, "讯飞返回结果： DIALOG_STATE $state  $template ****")
        when (state) {
            IflyosClient.DialogState.LISTENING.toString() -> {
                mEngineService?.stopAlert()

                //跳转语音交流界面
//                handleVoiceStart()
                showVoiceBar("")
//                Log.i(sTag, "讯飞返回结果： DIALOG_STATE LISTENING${state} $template****")
            }
            IflyosClient.DialogState.IDLE.toString() -> {
//                handleVoiceEnd()
                hideVoiceBar(true)
//                Log.i(sTag, "讯飞返回结果： DIALOG_STATE IDLE${state} $template****")
            }
            else -> {
                hideVoiceBar(false)
//                Log.i(sTag, "讯飞返回结果： DIALOG_STATE else ${state} $template****")
            }
        }
    }

    private var isVoiceShowing = false
    private var isVoiceHide = true

    private fun showVoiceBar(speak: String) {
        runOnUiThread {
            tv_bottom_speak.text = speak
            if (!isVoiceShowing) {
//                Log.i(sTag, "showVoiceBar TT： ${isVoiceShowing} $isVoiceHide****")
                tv_bottom_speak.visibility = View.VISIBLE
                isVoiceShowing = true
                isVoiceHide = false
                io.reactivex.Observable.interval(1000, TimeUnit.MILLISECONDS)
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(object : io.reactivex.Observer<Long> {
                            var disposable: Disposable? = null
                            override fun onComplete() {
                            }

                            override fun onSubscribe(d: Disposable) {
                                disposable = d
                                mCompositeDisposable.add(d)
                            }

                            override fun onNext(t: Long) {
                                if (isVoiceHide) {
//                                    Log.i(sTag, "onNext TT： ${isVoiceShowing} $isVoiceHide****")
                                    tv_bottom_speak.visibility = View.GONE
                                    isVoiceShowing = false
                                    isVoiceHide = true
                                    disposable?.dispose()
                                }
                            }

                            override fun onError(e: Throwable) {
                                disposable?.dispose()
                            }
                        })
            }
        }
    }

    private fun hideVoiceBar(isHide: Boolean) {
        isVoiceHide = isHide
//        runOnUiThread {
//            tv_bottom_speak.visibility = View.GONE
//        }
    }

    private fun dispatchCustomDirective(template: JSONObject) {
        val customDirective = GsonUtil.convertCustomDirective(template)
        if (customDirective != null) {
            NavigateUtil.navigatePage(fragment, customDirective)
        }
    }

    private fun changConnectionState(template: JSONObject) {
//        Log.i(sTag, "讯飞返回结果： CONNECTION_STATE****")
        val status = template.optString("status")
        val reason = template.optString("reason")
        when (status) {
            IflyosClient.ConnectionStatus.CONNECTED.toString() -> {
//                Log.i(sTag, "讯飞返回结果： 链接CONNECTED****")
            }
            IflyosClient.ConnectionStatus.DISCONNECTED.toString() -> {
//                Log.i(sTag, "讯飞返回结果： 断开DISCONNECTED****")
                if (reason != IflyosClient.ConnectionChangedReason.ACL_CLIENT_REQUEST.toString()) {
                    val navController = NavHostFragment.findNavController(fragment)
                    if (navController.currentDestination?.id != R.id.main_fragment) {
                        when (navController.currentDestination?.id) {
//                            R.id.body_template_fragment,
//                            R.id.body_template_3_fragment, R.id.weather_fragment,
//                            R.id.list_fragment, R.id.about_fragment -> {
//                                navController.navigateUp()
//                            }
                        }
                    }
                }
            }
            IflyosClient.ConnectionStatus.PENDING.toString() -> {
//                Log.i(sTag, "讯飞返回结果： IflyosClient连接中PENDING****")
                if (reason == IflyosClient.ConnectionChangedReason.SERVER_SIDE_DISCONNECT.toString()) {
                    val navController = findNavController(R.id.fragment)
                    if (navController.currentDestination?.id != R.id.main_fragment) {
                        when (navController.currentDestination?.id) {
//                            R.id.body_template_fragment,
//                            R.id.body_template_3_fragment, R.id.weather_fragment,
//                            R.id.list_fragment, R.id.about_fragment -> {
//                                navController.navigateUp()
//                            }
                        }
                    }
                }
            }
        }
    }

    private fun handleAuthLog(template: JSONObject) {
        val type = template.optInt("type")
        when (type) {
            LoggerHandler.AUTH_LOG_STATE -> {
                val authState = template.optString("auth_state")
                val authError = template.optString("auth_error")
                if (authState == AuthProvider.AuthState.REFRESHED.toString()) {
                    mStatusType = StatusType.NORMAL
                    val navController = NavHostFragment.findNavController(fragment)
                    //在启动页，则跳转至主页
                    if (navController.currentDestination?.id == R.id.splash_fragment) {
                        navController.navigate(R.id.action_to_main_fragment)
                    }
                } else if (authState == AuthProvider.AuthState.UNINITIALIZED.toString()) {
                    //授权失败
                    val navController = NavHostFragment.findNavController(fragment)
                    if (mEngineService?.getLocalAccessToken().isNullOrEmpty()) {
                        //未授权，前往授权扫码
                        if (navController.currentDestination?.id == R.id.splash_fragment) {
                            Log.i(sTag, "初始化跳转 action_to_welcome_fragment")
                            NavHostFragment.findNavController(fragment).navigate(R.id.action_to_wifi_fragment)
                        }
                    } else {
                        //跳转主界面
                        if (navController.currentDestination?.id == R.id.splash_fragment) {
                            navController.navigate(R.id.action_to_main_fragment)
                        }

                        val error = AuthProvider.AuthError.valueOf(authError)
                        when (error) {
                            AuthProvider.AuthError.AUTHORIZATION_EXPIRED -> {
                                mStatusType = StatusType.AUTHORIZE_ERROR
                            }
                            AuthProvider.AuthError.AUTHORIZATION_FAILED -> {
                                mStatusType = if (isNetworkAvailable)
                                    StatusType.AUTHORIZE_ERROR
                                else
                                    StatusType.NETWORK_ERROR
                            }
                            AuthProvider.AuthError.SERVER_ERROR,
                            AuthProvider.AuthError.INTERNAL_ERROR -> {
                                mStatusType = StatusType.SERVER_ERROR
                            }
                            AuthProvider.AuthError.UNAUTHORIZED_CLIENT -> {
                                mStatusType = StatusType.UNSUPPORTED_DEVICE_ERROR
                            }
                            AuthProvider.AuthError.UNKNOWN_ERROR -> {
                                mStatusType = StatusType.UNKNOWN_ERROR
                            }
                            AuthProvider.AuthError.NO_ERROR -> {
                                // ignore
                            }
                            else -> {
                                mStatusType = StatusType.UNKNOWN_ERROR
                            }
                        }

                        //TODO 这里需要根据不同的mStatusType，给出对应的"错误提示"
                        Toast.makeText(applicationContext, "当前的错误内容是$error", Toast.LENGTH_LONG).show()
                        runOnUiThread {
                            showErrorTips()
                        }
                    }
                } else {

                }
            }
        }
    }

    private fun dispatchMessageToFragment(o: Observable?, arg: Any?) {
        observerSet.map {
            it.update(o, arg)
        }
    }

    private fun showErrorTips() {
        when (mStatusType) {
            StatusType.NORMAL -> {
//                tvTipsSimple?.setText(R.string.tips_sentence_simple)
//                tvTipsSimple?.setOnClickListener(tapToTalkClickListener)
//                ivLogo?.setOnClickListener(tapToTalkClickListener)
//                ivLogo?.setImageResource(R.drawable.ic_voice_bar_regular_white_32dp)
            }
            StatusType.AUTHORIZE_ERROR -> {
//                tvTipsSimple?.setText(R.string.authorize_error)
//                tvTipsSimple?.setOnClickListener(authorizeErrorClickListener)
//                ivLogo?.setOnClickListener(authorizeErrorClickListener)
//                ivLogo?.setImageResource(R.drawable.ic_voice_bar_exception_white_32dp)
            }
            StatusType.SERVER_ERROR -> {
//                tvTipsSimple?.setText(R.string.unknown_error)
//                tvTipsSimple?.setOnClickListener(null)
//                ivLogo?.setImageResource(R.drawable.ic_voice_bar_exception_white_32dp)
//                ivLogo?.setOnClickListener(null)
            }
            StatusType.NETWORK_ERROR -> {
//                tvTipsSimple?.setText(R.string.network_error)
//                tvTipsSimple?.setOnClickListener(networkErrorClickListener)
//                ivLogo?.setOnClickListener(networkErrorClickListener)
//                ivLogo?.setImageResource(R.drawable.ic_voice_bar_exception_white_32dp)
            }
            StatusType.UNKNOWN_ERROR -> {
//                tvTipsSimple?.setText(R.string.unknown_error)
//                tvTipsSimple?.setOnClickListener(null)
//                ivLogo?.setImageResource(R.drawable.ic_voice_bar_exception_white_32dp)
//                ivLogo?.setOnClickListener(null)
            }
            StatusType.RETRYING -> {
//                tvTipsSimple?.setText(R.string.retry_connecting)
//                tvTipsSimple?.setOnClickListener(null)
//                ivLogo?.setImageResource(R.drawable.ic_voice_bar_exception_white_32dp)
//                ivLogo?.setOnClickListener(null)
            }
            StatusType.UNSUPPORTED_DEVICE_ERROR -> {
//                tvTipsSimple?.setText(R.string.unsupported_device_error)
//                tvTipsSimple?.setOnClickListener(null)
//                ivLogo?.setImageResource(R.drawable.ic_voice_bar_exception_white_32dp)
//                ivLogo?.setOnClickListener(null)
            }
            else -> {

            }
        }
    }

    fun hideSimpleTips() {

    }

    fun showSimpleTips() {
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        setImmersiveFlags()
    }

    /** 沉浸式状态栏 */
    private fun setImmersiveFlags() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)
        Constant.activity = this
        val intent = Intent(this, MyRemindService::class.java)
        startService(intent)
//        checkSSL()
//
//        setupView()
//
//        setImmersiveFlags()
//
//        hideIatPage()

//        receiveFlyosClientCustomDirective()

        EventBus.getDefault().register(this)

        bindService(Intent(this, EngineService::class.java), connection, Context.BIND_AUTO_CREATE)

//        setWifiStatusChangeListener()
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun handleEvent(chatEvent: ChatEvent) {
        if (chatEvent.messageType == ChatEvent.EVENT_NOTIFY) {
            if (chatEvent.messageContent !is ChatResponse) {
                return
            }

            val response = chatEvent.messageContent as ChatResponse?
            response ?: return

            for (message in response.message.reversed()) {//优先取最新消息
                Log.i("ChatService", message.toString())
                //是否是视频通讯相关的信息, 不是则跳到下一个
                if (!TextUtils.equals(message.type, "1")) {
                    continue
                }

                //自己发的消息过滤
                if (LoginSession.sUserId == message.fromId) {
                    continue
                }

                //视频相关的最新信息是否在一分钟以内，不是则跳出循环
                val currentTimeMillis = System.currentTimeMillis()
                val gapSeconds = currentTimeMillis - DateUtils.date2Timestamp(message.sendTime)
                if (gapSeconds > 60 * 1000 || gapSeconds < 0) {
                    break
                }
                val messageContent = GsonUtil.convertChatDetailMessage(message.msgContent)
                messageContent ?: continue


                if (TextUtils.equals("0", messageContent.getVideoState())) {//唤醒
                    val bundle = Bundle()
                    bundle.putString("roomId", messageContent.msgContent)
                    bundle.putString("name", messageContent.nickname)
                    bundle.putString("img", messageContent.iconImg)
                    bundle.putString("groupId", message.fromId)
                    bundle.putString("toName", message.fromName)
                    NavHostFragment.findNavController(fragment).navigate(R.id.videocoming_fragment, bundle)

                    break
                } else {//挂断，可能在唤醒界面/视频聊天界面
                    EventBus.getDefault().post(ChatEvent(ChatEvent.EVENT_VIDEO_DROPPED, message))
                    break
                }
            }
        }
    }

    private fun receiveFlyosClientCustomDirective() {
        RxBus.getInstance().toObservable(String::class.java).observeOn(AndroidSchedulers.mainThread()).subscribe(object : io.reactivex.Observer<String> {
            override fun onSubscribe(d: Disposable) {
                mCompositeDisposable.add(d)
            }

            override fun onNext(directive: String) {
                try {
//                    dispatchCustomDirective(JSONObject(directive))
                    val customDirective = GsonUtil.convertCustomDirective(JSONObject(directive))
                    if (customDirective != null) {
//                        DialogUtils.showPopToast(baseContext, customDirective.input)
                        showSnackBar(customDirective.input)
                    }
                } catch (e: Exception) {
                    Toast.makeText(applicationContext, "自定义指令解析发生错误", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onError(e: Throwable) {
                Toast.makeText(applicationContext, "自定义指令解析发生错误${e.message}", Toast.LENGTH_SHORT).show()
            }

            override fun onComplete() {

            }
        })
    }

    private fun showSnackBar(speak: String) {
        tv_bottom_speak.text = speak
        tv_bottom_speak.visibility = View.VISIBLE

        mCompositeDisposable.add(io.reactivex.Observable.interval(3000, TimeUnit.MILLISECONDS).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    tv_bottom_speak.visibility = View.GONE
                })
    }

    override fun onStart() {
        super.onStart()

        if (SpeechRecognizerHandler.speechRecognizerHandler != null
                && !SpeechRecognizerHandler.speechRecognizerHandler.isOpen) {
            //打开讯飞的语音类，防止讯飞无法唤醒
            SpeechRecognizerHandler.speechRecognizerHandler.startAudioInput()
        }
    }

    override fun onResume() {
        super.onResume()

        if (PermissionChecker.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PermissionChecker.PERMISSION_GRANTED
                || PermissionChecker.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PermissionChecker.PERMISSION_GRANTED
                || PermissionChecker.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED
                || PermissionChecker.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED) {
            requestPermission()
        }
    }

    override fun onStop() {
        super.onStop()

        //防止一直占用通道
        if (SpeechRecognizerHandler.speechRecognizerHandler.isOpen) {
            //关闭讯飞的语音类，防止音频通道占用
            SpeechRecognizerHandler.speechRecognizerHandler.stopAudioInput()
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE),
                REQUEST_PERMISSION_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.i(sTag, "请求权限的结构onRequestPermissionsResult---")
        if (requestCode == REQUEST_PERMISSION_CODE) {
            var isGranted = true
            for (i in 0 until permissions.size) {
                if (!isGranted) {
                    break
                }

                if (permissions[i] == Manifest.permission.RECORD_AUDIO) {
                    isGranted = isGranted && grantResults[i] == PermissionChecker.PERMISSION_GRANTED
                }

                if (permissions[i] == Manifest.permission.WRITE_EXTERNAL_STORAGE) {
                    isGranted = isGranted && grantResults[i] == PermissionChecker.PERMISSION_GRANTED
                }
            }

            if (isGranted) {
                mEngineService?.recreate()
                mEngineService?.addObserver(this@LauncherActivity)
                if (mEngineService?.getAuthState() == AuthProvider.AuthState.UNINITIALIZED
                        && mEngineService?.getLocalAccessToken().isNullOrEmpty()) {
                    Log.i(sTag, "AuthProvider.AuthState.UNINITIALIZED---${mEngineService?.getLocalAccessToken()}")
                    if (NavHostFragment.findNavController(fragment).currentDestination?.id == R.id.splash_fragment) {
                        NavHostFragment.findNavController(fragment).navigate(R.id.action_to_wifi_fragment)
                    }
                }
            } else {
                AlertDialog.Builder(this)
                        .setTitle(R.string.dialog_title_permission)
                        .setMessage(getString(R.string.dialog_permission_failed,
                                getString(R.string.permission_record_audio)))
                        .setPositiveButton(R.string.retry) { _, _ ->
                            requestPermission()
                        }
                        .setNegativeButton(R.string.close) { _, _ ->
                            finish() //TODO: As a Launcher, app wouldn't finish at fact, remove this in future
                        }
                        .show()
            }
        }
    }

    /** 在welcome和main，按back键，则退出 */
    override fun onBackPressed() {
        val controller = NavHostFragment.findNavController(fragment)
        when (controller.currentDestination?.id) {
            R.id.main_fragment -> {
                finish()
            }
            else -> {
                super.onBackPressed()
            }
        }
    }

    fun getHandler(namespace: String): PlatformInterface? {
        return mEngineService?.getHandler(namespace)
    }

    override fun onDestroy() {
        super.onDestroy()

        unbindService(connection)
        stopService(Intent(this, EngineService::class.java))

//        unbindService(mChatConnection)
        stopService(Intent(this, ChatService::class.java))

        if (!mCompositeDisposable.isDisposed) {
            mCompositeDisposable.dispose()
        }

        EventBus.getDefault().unregister(this)

        if (Build.VERSION.SDK_INT < 21)
//            unregisterReceiver(connectStateReceiver)
        else {
//            val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
//            if (connectivityManager is ConnectivityManager) {
//                connectivityManager.unregisterNetworkCallback(networkCallback)
//            }
        }
    }

    private val connection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            Log.i(sTag, "绑定service onServiceDisconnected---")
            mEngineService?.deleteObserver(this@LauncherActivity)
            mEngineService = null
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            Log.i(sTag, "绑定service onServiceConnected---")
            if (service is EngineService.EngineBinder) {
                mEngineService = service.getService()
                mEngineService?.recreate()
                mEngineService?.addObserver(this@LauncherActivity)
                val authState = mEngineService?.getAuthState()
                Log.d(sTag, authState.toString())
                if (authState == AuthProvider.AuthState.UNINITIALIZED
                        && mEngineService?.getLocalAccessToken().isNullOrEmpty()) {
                    Log.d(sTag, "跳转action_to_welcome_fragment")
                    NavHostFragment.findNavController(fragment).navigate(R.id.action_to_wifi_fragment)
                } else if (authState == AuthProvider.AuthState.REFRESHED) {
                    NavHostFragment.findNavController(fragment).navigate(R.id.action_to_main_fragment)
                }
            }
        }

    }

    private fun dismissTemplate(fragmentId: Int) {
        try {
            val nav = findNavController(R.id.fragment)
            if (nav.currentDestination?.id == fragmentId) {
                nav.navigateUp()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    class CounterHandler constructor(activity: LauncherActivity) : Handler() {

        private var count = 0

        private var stopped = false

        private val reference: WeakReference<LauncherActivity>?

        init {
            reference = WeakReference(activity)
        }

        override fun handleMessage(msg: Message) {
            if (stopped && count != 0) {
                stopped = false
                count = 0
                return
            }
            if (reference != null) {
                when (msg.what) {
                    START_COUNT -> {
                        stopped = false
                        count = 0
                        val continueMsg = Message.obtain()
                        continueMsg.what = CONTINUE_COUNT
                        continueMsg.arg1 = msg.arg1
                        sendMessageDelayed(continueMsg, 1000)
                    }
                    CONTINUE_COUNT -> {
                        val activity = reference.get()
                        if (activity != null) {
                            if (count >= MAX_COUNT_SECONDS) {
                                count = 0
                                val id = msg.arg1
                                reference.get()?.dismissTemplate(id)
                            } else {
                                count++
                                val newMsg = Message.obtain()
                                newMsg.what = msg.what
                                newMsg.arg1 = msg.arg1
                                sendMessageDelayed(newMsg, 1000)
                            }
                        }
                    }
                }
            }
        }

        internal fun stop() {
            stopped = true
            removeCallbacksAndMessages(null)
        }

        companion object {
            internal const val START_COUNT = 0x1
            internal const val CONTINUE_COUNT = 0x2
            internal const val MAX_COUNT_SECONDS = 20
            internal const val SHOW_COUNT_SECONDS = 10
        }
    }

    private fun offsetVolume(volume: Byte) {
        mEngineService?.offsetVolume(volume)
    }

//    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
//        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
//            longPressHandler.sendEmptyMessage(LongPressHandler.VOLUME_DOWN)
//            return true
//        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
//            longPressHandler.sendEmptyMessage(LongPressHandler.VOLUME_UP)
//            return true
//        }
//        return super.onKeyDown(keyCode, event)
//    }
//
//    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
//        when (keyCode) {
//            KeyEvent.KEYCODE_VOLUME_UP, KeyEvent.KEYCODE_VOLUME_DOWN -> {
//                longPressHandler.longPressEnable = false
//                longPressHandler.sendEmptyMessage(LongPressHandler.VOLUME_REPORT)
//                return true
//            }
//        }
//        return super.onKeyUp(keyCode, event)
//    }
//
//    override fun onKeyLongPress(keyCode: Int, event: KeyEvent?): Boolean {
//        when (keyCode) {
//            KeyEvent.KEYCODE_VOLUME_DOWN -> {
//                longPressHandler.longPressEnable = true
//                longPressHandler.sendEmptyMessage(LongPressHandler.VOLUME_DOWN)
//                return true
//            }
//            KeyEvent.KEYCODE_VOLUME_UP -> {
//                longPressHandler.longPressEnable = true
//                longPressHandler.sendEmptyMessage(LongPressHandler.VOLUME_UP)
//                return true
//            }
//        }
//        return super.onKeyLongPress(keyCode, event)
//    }

    private class LongPressHandler(activity: LauncherActivity) : Handler() {
        val softReference = SoftReference<LauncherActivity>(activity)
        var longPressEnable = false

        companion object {
            const val VOLUME_UP = 1
            const val VOLUME_DOWN = 0
            const val VOLUME_REPORT = 2

            private const val VOLUME_OFFSET_UP: Byte = 10
            private const val VOLUME_OFFSET_DOWN: Byte = -10
        }

        override fun handleMessage(msg: Message?) {
            val activity = softReference.get()
            activity?.let {
                when (msg?.what) {
                    VOLUME_DOWN -> {
                        activity.offsetVolume(VOLUME_OFFSET_DOWN)
                        if (longPressEnable) {
                            sendEmptyMessageDelayed(VOLUME_DOWN, 200)
                        } else {

                        }
                    }
                    VOLUME_UP -> {
                        activity.offsetVolume(VOLUME_OFFSET_UP)
                        if (longPressEnable) {
                            sendEmptyMessageDelayed(VOLUME_UP, 200)
                        } else {

                        }
                    }
                    VOLUME_REPORT -> {

                    }
                    else -> {
                        // ignore
                    }
                }
            }
        }
    }

}