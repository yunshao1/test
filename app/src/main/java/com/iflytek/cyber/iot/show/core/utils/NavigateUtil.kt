package com.iflytek.cyber.iot.show.core.utils

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.TextUtils
import android.util.Log
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.iflytek.cyber.iot.show.core.R
import com.iflytek.cyber.iot.show.core.data.net.NetHandler
import com.iflytek.cyber.iot.show.core.fragment.*
import com.iflytek.cyber.iot.show.core.model.Constant
import com.iflytek.cyber.iot.show.core.model.CustomDirective

/**
 * @author cloud.wang
 * @date 2019/6/1
 * @Description: TODO
 */
object NavigateUtil {

    fun navigatePage(fragment: Fragment, customDirective: CustomDirective) {
        val navController = NavHostFragment.findNavController(fragment)
        Log.i("Cloud", "navigatePage:: $customDirective")
//        if (navController.currentDestination?.id == R.id.main_fragment) {
//            Log.i("Cloud", "navController.currentDestination:: $customDirective")
//        }
        Log.e("345", "output == "+customDirective.output)
        Log.e("345", "output == "+customDirective.type)
        if ("0".equals(customDirective.type)) {
            //显示input说的话，播放output答复，不做跳转
            val bundle = Bundle()
            bundle.putString("title", customDirective.input)
            bundle.putString("content", customDirective.output)
            navController.navigate(R.id.apply_fragment, bundle)
            return
        }

        when(customDirective.intentType) {
            "navigate" -> {
                handleIntentNavigate(navController, customDirective)
            }
            "pagesearch" -> {
                //关键字
                handleIntentSearch(fragment, customDirective)
            }
            "joinaction" -> {
                //关键字
                handleIntentJoin(fragment, customDirective)
            }
            else -> {
                //音箱直接读的文本
            }
        }

        return
    }

    private fun handleIntentSearch(fragment: Fragment, customDirective: CustomDirective) {
        val targetFragment = fragment.childFragmentManager.findFragmentById(R.id.fragment)
        if (targetFragment is CommunityFragment) {
            Log.i("Cloud", "pagesearch:: 社区邻里查询 ${customDirective.output}")
            targetFragment.load(customDirective.output)
        } else if (targetFragment is ServiceDetailFragment) {
            Log.i("Cloud", "pagesearch:: 适老旅游/康复护理查询 ${customDirective.output}")
            targetFragment.load(customDirective.output)
        } else if (targetFragment is PharmacyDetailFragment) {
            Log.i("Cloud", "pagesearch:: 周边药房二级药物查询")
            targetFragment.load(customDirective.output)
        } else if (targetFragment is NutritiousMealsFragment) {
            Log.i("Cloud", "pagesearch:: 营养配餐查询")
            targetFragment.load(customDirective.output)
        } else {
            Log.i("Cloud", "pagesearch:: 找不到你要的内容")
            playVoice("找不到你要的内容")
        }
    }

    private fun handleIntentJoin(fragment: Fragment, customDirective: CustomDirective) {
        val targetFragment = fragment.childFragmentManager.findFragmentById(R.id.fragment)
        if (targetFragment is CommunityFragment) {
            Log.i("Cloud", "joinaction:: 社区邻里下单")
            targetFragment.submitAction(customDirective.input)
        } else if (targetFragment is ServiceDetailFragment) {
            Log.i("Cloud", "joinaction:: 适老旅游/康复护理下单")
            targetFragment.submitAction(customDirective.input)
        } else if (targetFragment is HomeRepairFragment) {
            Log.i("Cloud", "joinaction:: 居家维修下单")
            targetFragment.submitAction("居家维修 ${customDirective.input}")
        } else if (targetFragment is PharmacyDetailFragment) {
            Log.i("Cloud", "joinaction:: 周边药房二级药物下单")
            targetFragment.submitAction(customDirective.input)
        } else if (targetFragment is NutritiousMealsFragment) {
            Log.i("Cloud", "joinaction:: 营养配餐下单")
            targetFragment.submitAction(customDirective.input)
        } else {
            Log.i("Cloud", "joinaction:: 当前页面还不能下单噢")
            playVoice("当前页面还不能下单噢")
        }
    }

    private fun handleIntentNavigate(navController: NavController, customDirective: CustomDirective) {
        when(customDirective.output) {
            Constant.PAGE_HOME -> {
                navController.navigate(R.id.main_fragment)
            }
            Constant.PAGE_APPOINTMENT -> {
                val bundle = Bundle()
                bundle.putString("title", "社区预约")
                bundle.putString("url", com.iflytek.cyber.iot.show.core.utils.Constant.URL_SQYY)
                navController.navigate(R.id.webview_fragment,bundle)
            }
            Constant.PAGE_BACK -> {
                navController.navigateUp()
            }
            Constant.PAGE_CATERING -> {
                navController.navigate(R.id.nutritious_meals_fragment)
            }
            Constant.PAGE_COMMUNITY -> {
                navController.navigate(R.id.community_fragment)
            }
            Constant.PAGE_DOCTOR -> {
                navController.navigate(R.id.online_fragment)
            }
            Constant.PAGE_DRUGSTORE -> {
                navController.navigate(R.id.pharmacy_fragment)
            }
            Constant.PAGE_HEALTH -> {
                navController.navigate(R.id.fragment_healthy)
            }
            Constant.PAGE_HEALTH_AIDE -> {
                val bundle = Bundle()
                bundle.putString("title", "康复护理")
                bundle.putString("code", "T-006")
                bundle.putString("apiUrl", "http://speaker.api.shfusion.com/api/Service/GetByCode")
                navController.navigate(R.id.service_detail_fragment,bundle)
            }
            Constant.PAGE_HEALTH_DATA -> {
                val bundle4 = Bundle()
                bundle4.putString("title", "报告详情")
                bundle4.putString("url", "http://elderlyweixin.shfusion.com/Interface/Android/ReportInfo")
                navController.navigate(R.id.webview_fragment,bundle4)
            }
            Constant.PAGE_HEALTH_TECH -> {
                val bundle = Bundle()
                bundle.putString("title", "健康科技")
                bundle.putString("code", "T-003")
                bundle.putString("apiUrl", "http://speaker.api.shfusion.com/api/Service/GetByCode")
                navController.navigate(R.id.service_detail_fragment,bundle)
            }
            Constant.PAGE_LIFE_CARE -> {
                val bundle = Bundle()
                bundle.putString("title", "生活照料")
                bundle.putString("code", "T-004")
                bundle.putString("apiUrl", "http://speaker.api.shfusion.com/api/Service/GetByCode")
                navController.navigate(R.id.service_detail_fragment,bundle)
            }
            Constant.PAGE_MAINTAIN -> {
                navController.navigate(R.id.home_repair_fragment)
            }
            Constant.PAGE_ORDER -> {
                navController.navigate(R.id.fragment_my_order)
            }
            Constant.PAGE_REPORT -> {
                navController.navigate(R.id.health_report_fragment)
            }
            Constant.PAGE_SERVICE_CITY -> {
                navController.navigate(R.id.service_fragment)
            }
            Constant.PAGE_TRAVEL -> {
                val bundle = Bundle()
                bundle.putString("title", "适老旅行")
                bundle.putString("code", "T-008")
                bundle.putString("apiUrl", "http://speaker.api.shfusion.com/api/Service/GetByCode")
                navController.navigate(R.id.service_detail_fragment,bundle)
            }
            Constant.PAGE_VIDEO -> {
                navController.navigate(R.id.video_fragment)
            }
            Constant.PAGE_MERCHANDISE -> {
                val bundle = Bundle()
                bundle.putString("title", "商品租售")
                bundle.putString("code", "T-002")
                bundle.putString("apiUrl", "http://speaker.api.shfusion.com/api/Service/GetByCode")
                navController.navigate(R.id.service_detail_fragment,bundle)
            }
            Constant.PAGE_HEALTH_QUERY -> {
                navController.navigate(R.id.fragment_ask_healthy)
            }
            else -> {
                //关键字
            }
        }
    }

    fun playVoice(speakText: String) {
        if (!TextUtils.isEmpty(speakText)) {
            NetHandler.Inst.playVoice(speakText)
        }
    }

}