package com.iflytek.cyber.iot.show.core.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;

public class TextMessageAdapter extends RecyclerView.Adapter<TextMessageAdapter.VHodler> {

    private Context context;
    private String[] list = {"什么时候回家?", "找你有事，不忙了给我发消息", "吃饭了么，忙不忙"};


    public TextMessageAdapter(Context context) {
        this.context = context;
    }


    @NonNull
    @Override
    public TextMessageAdapter.VHodler onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        VHodler vHodler = null;
        vHodler = new VHodler(LayoutInflater.from(context).inflate(R.layout.item_text_message, viewGroup, false));
        return vHodler;
    }

    @Override
    public void onBindViewHolder(@NonNull TextMessageAdapter.VHodler vHodler, final int position) {
        vHodler.tv_title.setText(list[position]);
        vHodler.tv_title.setOnClickListener(v -> {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(list[position]);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.length;
    }

    public class VHodler extends RecyclerView.ViewHolder {
        public TextView tv_title;

        public VHodler(@NonNull View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(String bean);
    }

    public OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
