package com.iflytek.cyber.iot.show.core.chat.event

/**
 * @author cloud.wang
 * @date 2019-07-20
 * @Description: TODO
 */
class ChatEvent constructor(val messageType: Int, val messageContent: Any?) {

    companion object {
        const val EVENT_FINISH_CONNECTION = 0x01
        const val EVENT_DISCONNECTION = 0x02

        const val EVENT_CREATE_MESSAGE_SESSION = 0x03
        const val EVENT_CREATE_NOTIFY_SESSION = 0x06

        const val EVENT_MESSAGE = 0x04
        const val EVENT_SEND_MESSAGE_FAILED = 0x05

        const val EVENT_NOTIFY = 0x08

        const val EVENT_VIDEO_DROPPED = 0x14
    }

}