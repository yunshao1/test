package com.iflytek.cyber.iot.show.core.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.ArrayMap;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.navigation.fragment.NavHostFragment;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.ActionResponse;
import com.iflytek.cyber.iot.show.core.utils.DateUtils;

import java.util.Date;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * @author cloud.wang
 * @date 2019/6/8
 * @Description: TODO
 */
public class NutritiousDialogFragment extends DialogFragment implements View.OnClickListener {

    private TextView tv_today;
    private TextView tv_torrow;
    private TextView tv_last;
    private TextView tv_lunch;
    private TextView tv_night;
    String goodProperty = "午餐";
    private View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_fragment_nutritious, container);
        initView();
        return view;
    }
    private void initView(){

        tv_today = view.findViewById(R.id.tv_today);
        tv_torrow = view.findViewById(R.id.tv_torrow);
        tv_last = view.findViewById(R.id.tv_last);
        tv_lunch = view.findViewById(R.id.tv_lunch);
        tv_night = view.findViewById(R.id.tv_night);

        tv_today.setOnClickListener(this);
        tv_torrow.setOnClickListener(this);
        tv_last.setOnClickListener(this);
        tv_lunch.setOnClickListener(this);
        tv_night.setOnClickListener(this);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitAction("下单");
                dismiss();
            }
        });

        view.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Window window = getDialog().getWindow();
        if (window == null) {
            return;
        }

        window.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                  | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            );

//        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        window.setBackgroundDrawableResource(R.drawable.shape_bg_white_corners_16);
        window.getDecorView().setPadding(0, 0, 0, 0); //消除边距

        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = 1200;   //设置宽度充满屏幕
        lp.height = 610;
        lp.gravity = Gravity.CENTER;
        window.setAttributes(lp);

        setCancelable(true);

    }

    /**
     * @param input 下单或者安排
     */
    @SuppressLint("CheckResult")
    public void submitAction(final String input) {
        Map<String,String> map = new ArrayMap<>();
        map.put("serviceId","");
        map.put("typeCode","T-009");
        map.put("goodProperty",goodProperty);//配餐传早餐中餐午餐
        map.put("date", DateUtils.getStrDate(new Date()));
        map.put("content","营养配餐");
        map.put("actionName","下单");
       NetHandler.Companion.getInst().submitAction2(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ActionResponse>() {
                    @Override
                    public void accept(ActionResponse actionResponse) throws Exception {
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.i("Cloud", "onError::"+ throwable.getMessage());
//                        Toast.makeText(getLauncher(), "网络请求失败，导致报名失败，请重新"+ throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_today:
                tv_today.setTextColor(getActivity().getResources().getColor(R.color.white));
                tv_today.setBackgroundResource(R.drawable.shape_btn_stoke);

                tv_torrow.setTextColor(getActivity().getResources().getColor(R.color.time_bg_normal));
                tv_torrow.setBackgroundResource(R.drawable.layerlist_btn_stoke);
                tv_last.setTextColor(getActivity().getResources().getColor(R.color.time_bg_normal));
                tv_last.setBackgroundResource(R.drawable.layerlist_btn_stoke);
                break;
            case R.id.tv_torrow:
                tv_today.setTextColor(getActivity().getResources().getColor(R.color.time_bg_normal));
                tv_today.setBackgroundResource(R.drawable.layerlist_btn_stoke);

                tv_torrow.setTextColor(getActivity().getResources().getColor(R.color.white));
                tv_torrow.setBackgroundResource(R.drawable.shape_btn_stoke);

                tv_last.setTextColor(getActivity().getResources().getColor(R.color.time_bg_normal));
                tv_last.setBackgroundResource(R.drawable.layerlist_btn_stoke);
                break;
            case R.id.tv_last:
                tv_today.setTextColor(getActivity().getResources().getColor(R.color.time_bg_normal));
                tv_today.setBackgroundResource(R.drawable.layerlist_btn_stoke);

                tv_torrow.setTextColor(getActivity().getResources().getColor(R.color.time_bg_normal));
                tv_torrow.setBackgroundResource(R.drawable.layerlist_btn_stoke);

                tv_last.setTextColor(getActivity().getResources().getColor(R.color.white));
                tv_last.setBackgroundResource(R.drawable.shape_btn_stoke);
                break;
            case R.id.tv_lunch:
                tv_lunch.setTextColor(getActivity().getResources().getColor(R.color.white));
                tv_lunch.setBackgroundResource(R.drawable.shape_btn_stoke);
                goodProperty = "午餐";
                tv_night.setTextColor(getActivity().getResources().getColor(R.color.time_bg_normal));
                tv_night.setBackgroundResource(R.drawable.layerlist_btn_stoke);
                break;
            case R.id.tv_night:
                tv_lunch.setTextColor(getActivity().getResources().getColor(R.color.time_bg_normal));
                tv_lunch.setBackgroundResource(R.drawable.layerlist_btn_stoke);
                goodProperty = "晚餐";
                tv_night.setTextColor(getActivity().getResources().getColor(R.color.white));
                tv_night.setBackgroundResource(R.drawable.shape_btn_stoke);
                break;
        }


    }
}
