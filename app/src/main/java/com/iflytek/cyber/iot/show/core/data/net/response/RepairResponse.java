package com.iflytek.cyber.iot.show.core.data.net.response;

import java.util.List;

public class RepairResponse {


    private String message;
    private String tryTxt;
    private String readTxt;
    private List<ListBean> list;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTryTxt() {
        return tryTxt;
    }

    public void setTryTxt(String tryTxt) {
        this.tryTxt = tryTxt;
    }

    public String getReadTxt() {
        return readTxt;
    }

    public void setReadTxt(String readTxt) {
        this.readTxt = readTxt;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {

        private int id;
        private String code;
        private String name;
        private int parentId;
        private String planWay;
        private boolean isRoot;
        private String status;
        private String remark;
        private String creater;
        private String createDate;
        private String editor;
        private String editDate;
        private String img;
        private List<?> list;
        private List<?> orgs;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public String getPlanWay() {
            return planWay;
        }

        public void setPlanWay(String planWay) {
            this.planWay = planWay;
        }

        public boolean isIsRoot() {
            return isRoot;
        }

        public void setIsRoot(boolean isRoot) {
            this.isRoot = isRoot;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getCreater() {
            return creater;
        }

        public void setCreater(String creater) {
            this.creater = creater;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getEditor() {
            return editor;
        }

        public void setEditor(String editor) {
            this.editor = editor;
        }

        public String getEditDate() {
            return editDate;
        }

        public void setEditDate(String editDate) {
            this.editDate = editDate;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public List<?> getList() {
            return list;
        }

        public void setList(List<?> list) {
            this.list = list;
        }

        public List<?> getOrgs() {
            return orgs;
        }

        public void setOrgs(List<?> orgs) {
            this.orgs = orgs;
        }
    }
}
