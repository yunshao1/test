package com.iflytek.cyber.iot.show.core.fragment.healthy;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.adapter.HealthReportAdapter;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.HealthReportBean;
import com.iflytek.cyber.iot.show.core.fragment.BaseFragment;
import com.iflytek.cyber.iot.show.core.widget.recyclerview.AlphaTransformer;
import com.iflytek.cyber.iot.show.core.widget.recyclerview.FlingRecycleView;
import com.iflytek.cyber.iot.show.core.widget.recyclerview.GalleryLayoutManager;
import com.iflytek.cyber.iot.show.core.widget.recyclerview.LinearItemDecoration;

import androidx.navigation.fragment.NavHostFragment;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


/**
 * 健康报告
 */
public class HealthReportFragment extends BaseFragment {
    LinearLayout ll_finsh;
    private FlingRecycleView recyclerView;
    private HealthReportAdapter adapter;
    private String type = "";
    private String title = "";
    private TextView tv_title;

    private View view;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_service_detail, container, false);
            initView();
        }
        return view;
    }

    @Override
    public void onActivityCreated(@org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        load();
    }

    private void load() {


        NetHandler.Companion.getInst().getFormsBriefly()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<HealthReportBean>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(HealthReportBean listResponse) {
                        if (listResponse != null) {
                            setData(listResponse);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void setData(HealthReportBean listResponse) {
        if (listResponse.getTryTxt() != null) {
            setHideBottomBar(true);
            setTipText(listResponse.getTryTxt(), R.style.TextWhiteFont);
        }
        adapter.setData(listResponse.getResult());

    }


    private void initView() {
//        type = getArguments().getString("type");
        ll_finsh = view.findViewById(R.id.ll_finsh);
        tv_title = view.findViewById(R.id.tv_title);
        tv_title.setText("健康报告");
        recyclerView = view.findViewById(R.id.recyclerView);


        recyclerView.setFlingAble(false);
        GalleryLayoutManager layoutManager = new GalleryLayoutManager(GalleryLayoutManager.HORIZONTAL);
        layoutManager.attach(recyclerView, 0);
        layoutManager.setOnItemSelectedListener(new GalleryLayoutManager.OnItemSelectedListener() {
            @Override
            public void onItemSelected(RecyclerView recyclerView, View item, int position) {
                HealthReportBean.ResultBean bean = adapter.getList().get(position);
                playVoice(bean);


            }
        });
        layoutManager.setItemTransformer(new AlphaTransformer());
        LinearItemDecoration divider = new LinearItemDecoration.Builder(getActivity())
                .setOrientation(LinearLayoutManager.HORIZONTAL)
                .setSpan(48f)
                .setColorResource(android.R.color.transparent)
                .setShowLastLine(true)
                .build();
        recyclerView.addItemDecoration(divider);
        adapter = new HealthReportAdapter(getActivity());
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new HealthReportAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(HealthReportBean.ResultBean bean) {
                Bundle bundle4 = new Bundle();
                bundle4.putString("title", "报告详情");
                bundle4.putString("url", "http://elderlyweixin.shfusion.com/Interface/Android/ReportInfo");
                NavHostFragment.findNavController(HealthReportFragment.this).navigate(R.id.action_to_webview_fragment, bundle4);
            }
        });
        ll_finsh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(HealthReportFragment.this).navigateUp();

            }
        });
    }

    private void playVoice(HealthReportBean.ResultBean bean) {
        if (bean == null || TextUtils.isEmpty(bean.getReadTxt()))
            return;
        NetHandler.Companion.getInst().playVoice(bean.getReadTxt());
    }
}
