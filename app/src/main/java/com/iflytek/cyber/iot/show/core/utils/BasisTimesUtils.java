package com.iflytek.cyber.iot.show.core.utils;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.NumberPicker;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * time 工具类
 */

public class BasisTimesUtils {
    public static int THEME_DEVICE_DEFAULT_LIGHT = AlertDialog.THEME_DEVICE_DEFAULT_LIGHT;
    public static int THEME_DEVICE_DEFAULT_DARK = AlertDialog.THEME_DEVICE_DEFAULT_DARK;
    public static int THEME_TRADITIONAL = AlertDialog.THEME_TRADITIONAL;
    public static int THEME_HOLO_LIGHT = AlertDialog.THEME_HOLO_LIGHT;
    public static int THEME_HOLO_DARK = AlertDialog.THEME_HOLO_DARK;
    private static DatePickerDialog mDatePickerDialog;//日期选择器







    /**
     * 显示日期选择器
     *
     * @param themeLight true 白色背景; false 黑色背景
     */

    public static BasisTimesUtils showDatePickerDialog(Context context, boolean themeLight, String title, int year, int month, int day, OnDatePickerListener onDateTimePickerListener) {

        int themeId = AlertDialog.THEME_HOLO_LIGHT;//默认白色背景

        if (!themeLight) {

            themeId = AlertDialog.THEME_HOLO_DARK;//黑色背景

        }

        return showDatePickerDialog(context, themeId, title, year, month, day, onDateTimePickerListener);

    }


    /**
     * 显示日期选择器, 默认白色背景
     */

    public static BasisTimesUtils showDatePickerDialog(Context context, String title, int year, int month, int day, OnDatePickerListener onDateTimePickerListener) {

        return showDatePickerDialog(context, AlertDialog.THEME_HOLO_LIGHT, title, year, month, day, onDateTimePickerListener);

    }


    /**
     * 显示日期选择器
     */

    public static BasisTimesUtils showDatePickerDialog(Context context, int themeId, String title, int year, int month, int day,

                                                       final OnDatePickerListener onDateTimePickerListener) {

        mDatePickerDialog = new DatePickerDialog(context, themeId, new DatePickerDialog.OnDateSetListener() {

            @Override

            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                month = month + 1;//月份加一

                if (onDateTimePickerListener != null) {

                    onDateTimePickerListener.onConfirm(year, month, dayOfMonth);

                }

            }


        }, year, month - 1, day);//月份减一


        mDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

            @Override

            public void onCancel(DialogInterface dialog) {

                if (onDateTimePickerListener != null) {

                    onDateTimePickerListener.onCancel();

                }

            }

        });


        if (!TextUtils.isEmpty(title)) {

            mDatePickerDialog.setTitle(title);

        }

        mDatePickerDialog.show();

        return new BasisTimesUtils();

    }


    /**
     * 隐藏年, 只显示月和日
     */

    public void setYearGone() {

        setSpecialDatePicker(1);

    }


    /**
     * 隐藏日, 只显示年和月
     */

    public void setDayGone() {

        setSpecialDatePicker(2);

    }


    private void setSpecialDatePicker(int state) {

        try {

            DatePicker dp = mDatePickerDialog.getDatePicker();


            NumberPicker view0 = (NumberPicker) ((ViewGroup) ((ViewGroup) dp.getChildAt(0)).getChildAt(0)).getChildAt(0); //获取最前一位的宽度

            NumberPicker view1 = (NumberPicker) ((ViewGroup) ((ViewGroup) dp.getChildAt(0)).getChildAt(0)).getChildAt(1); //获取中间一位的宽度

            NumberPicker view2 = (NumberPicker) ((ViewGroup) ((ViewGroup) dp.getChildAt(0)).getChildAt(0)).getChildAt(2);//获取最后一位的宽度


            //年的最大值为2100

            //月的最大值为11

            //日的最大值为28,29,30,31

            int value0 = view0.getMaxValue();//获取第一个View的最大值

            int value1 = view1.getMaxValue();//获取第二个View的最大值

            int value2 = view2.getMaxValue();//获取第三个View的最大值


            if (state == 1) {//隐藏年, 只显示月和日

                if (value0 > 252) {

                    view0.setVisibility(View.GONE);

                } else if (value1 > 252) {

                    view1.setVisibility(View.GONE);

                } else if (value2 > 252) {

                    view2.setVisibility(View.GONE);

                }

            } else if (state == 2) {//隐藏日, 只显示年和月

                if (value0 > 25 && value0 < 252) {

                    view0.setVisibility(View.GONE);

                } else if (value1 > 25 && value1 < 252) {

                    view1.setVisibility(View.GONE);

                } else if (value2 > 25 && value2 < 252) {

                    view2.setVisibility(View.GONE);

                }

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

    }


    /**
     * 显示时间选择器
     */

    public static void showTimerPickerDialog(Context context, boolean themeLight, String title, int hourOfDay, int minute, boolean is24HourView, final OnTimerPickerListener onTimerPickerListener) {

        int themeId = AlertDialog.THEME_HOLO_LIGHT;//默认白色背景

        if (!themeLight) {

            themeId = AlertDialog.THEME_HOLO_DARK;//黑色背景

        }

        showTimerPickerDialog(context, themeId, title, hourOfDay, minute, is24HourView, onTimerPickerListener);

    }


    /**
     * 显示时间选择器, 默认白色背景
     */

    public static void showTimerPickerDialog(Context context, String title, int hourOfDay, int minute, boolean is24HourView, final OnTimerPickerListener onTimerPickerListener) {

        showTimerPickerDialog(context, AlertDialog.THEME_HOLO_LIGHT, title, hourOfDay, minute, is24HourView, onTimerPickerListener);

    }


    /**
     * 显示时间选择器
     */

    public static void showTimerPickerDialog(Context context, int themeId, String title, int hourOfDay, int minute, boolean is24HourView, final OnTimerPickerListener onTimerPickerListener) {

        TimePickerDialog dialog = new TimePickerDialog(context, themeId, new TimePickerDialog.OnTimeSetListener() {

            @Override

            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                if (onTimerPickerListener != null) {

                    onTimerPickerListener.onConfirm(hourOfDay, minute);

                }

            }

        }, hourOfDay, minute, is24HourView);


        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

            @Override

            public void onCancel(DialogInterface dialog) {

                if (onTimerPickerListener != null) {

                    onTimerPickerListener.onCancel();

                }

            }

        });


        if (!TextUtils.isEmpty(title)) {

            dialog.setTitle(title);

        }

        dialog.show();

    }



    /**
     * 日期选择器监听
     */

    public interface OnDatePickerListener {

        void onConfirm(int year, int month, int dayOfMonth);


        void onCancel();

    }


    /**
     * 时间选择器监听
     */

    public interface OnTimerPickerListener {

        void onConfirm(int hourOfDay, int minute);


        void onCancel();

    }

}

