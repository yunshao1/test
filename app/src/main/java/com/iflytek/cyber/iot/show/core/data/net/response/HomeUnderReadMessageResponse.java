package com.iflytek.cyber.iot.show.core.data.net.response;

import java.util.List;

public class HomeUnderReadMessageResponse {

    private String message;
    private ListBean list;
    private String tryTxt;
    private String readTxt;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ListBean getList() {
        return list;
    }

    public void setList(ListBean list) {
        this.list = list;
    }

    public String getTryTxt() {
        return tryTxt;
    }

    public void setTryTxt(String tryTxt) {
        this.tryTxt = tryTxt;
    }

    public String getReadTxt() {
        return readTxt;
    }

    public void setReadTxt(String readTxt) {
        this.readTxt = readTxt;
    }

    @Override
    public String toString() {
        return "HomeUnderReadMessageResponse{" +
                "message='" + message + '\'' +
                ", list=" + list +
                ", tryTxt='" + tryTxt + '\'' +
                ", readTxt='" + readTxt + '\'' +
                '}';
    }

    public static class ListBean {
        /**
         * unreadMessages : [{"fromId":"6190","fromType":"Wristband","toId":"6190","msgContent":"天气变化大，注意身体","audioUrl":"","id":"aa9b1db31d5f-b290-c948-34ce-a8d31103","isRead":false,"isAudio":false,"isMine":false,"fromUserImg":"http://resource.shfusion.com/image/common/elder/user.jpg"}]
         * count : 1
         */

        private int count;
        private List<UnreadMessagesBean> unreadMessages;

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public List<UnreadMessagesBean> getUnreadMessages() {
            return unreadMessages;
        }

        public void setUnreadMessages(List<UnreadMessagesBean> unreadMessages) {
            this.unreadMessages = unreadMessages;
        }

        @Override
        public String toString() {
            return "ListBean{" +
                    "count=" + count +
                    ", unreadMessages=" + unreadMessages +
                    '}';
        }

        public static class UnreadMessagesBean {

            private String fromId;
            private String fromType;
            private String toId;
            private String msgContent;
            private String audioUrl;
            private String id;
            private boolean isRead;
            private boolean isAudio;
            private boolean isMine;
            private String fromUserImg;

            public String getFromId() {
                return fromId;
            }

            public void setFromId(String fromId) {
                this.fromId = fromId;
            }

            public String getFromType() {
                return fromType;
            }

            public void setFromType(String fromType) {
                this.fromType = fromType;
            }

            public String getToId() {
                return toId;
            }

            public void setToId(String toId) {
                this.toId = toId;
            }

            public String getMsgContent() {
                return msgContent;
            }

            public void setMsgContent(String msgContent) {
                this.msgContent = msgContent;
            }

            public String getAudioUrl() {
                return audioUrl;
            }

            public void setAudioUrl(String audioUrl) {
                this.audioUrl = audioUrl;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public boolean isIsRead() {
                return isRead;
            }

            public void setIsRead(boolean isRead) {
                this.isRead = isRead;
            }

            public boolean isIsAudio() {
                return isAudio;
            }

            public void setIsAudio(boolean isAudio) {
                this.isAudio = isAudio;
            }

            public boolean isIsMine() {
                return isMine;
            }

            public void setIsMine(boolean isMine) {
                this.isMine = isMine;
            }

            public String getFromUserImg() {
                return fromUserImg;
            }

            public void setFromUserImg(String fromUserImg) {
                this.fromUserImg = fromUserImg;
            }

            @Override
            public String toString() {
                return "UnreadMessagesBean{" +
                        "fromId='" + fromId + '\'' +
                        ", fromType='" + fromType + '\'' +
                        ", toId='" + toId + '\'' +
                        ", msgContent='" + msgContent + '\'' +
                        ", audioUrl='" + audioUrl + '\'' +
                        ", id='" + id + '\'' +
                        ", isRead=" + isRead +
                        ", isAudio=" + isAudio +
                        ", isMine=" + isMine +
                        ", fromUserImg='" + fromUserImg + '\'' +
                        '}';
            }
        }
    }
}
