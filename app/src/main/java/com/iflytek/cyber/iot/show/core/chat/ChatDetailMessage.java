package com.iflytek.cyber.iot.show.core.chat;

import com.iflytek.cyber.iot.show.core.utils.SharedPreUtils;

/**
 * @author cloud.wang
 * @date 2019-07-27
 * @Description: TODO
 */
public class ChatDetailMessage {

    /**
     * msgType : txt
     * msgContent : Hello World!!!
     * iconImg : http://elderlyservice.shfusion.com/Image/newTow/avatar1@2x.jpg
     * name : Mobile
     * nickname : 客户端开发
     */
    private String msgType;
    //默认0：唤醒  1：挂断
    private String videoState;

    private String msgContent;
    private String iconImg;
    private String name;
    private String nickname;

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public String getIconImg() {
        return iconImg;
    }

    public void setIconImg(String iconImg) {
        this.iconImg = iconImg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getVideoState() {
        return videoState;
    }

    public void setVideoState(String videoState) {
        this.videoState = videoState;
    }
}
