package com.iflytek.cyber.iot.show.core.fragment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.response.CommunityBean;
import com.iflytek.cyber.iot.show.core.utils.GlideUtil;
import com.iflytek.cyber.iot.show.core.widget.recyclerview.CardAdapterHelper;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

public class CommunityAdapter extends CommonAdapter<CommunityBean> {

    public CommunityAdapter(Context context, int layoutId, List<CommunityBean> beanList) {
        super(context, layoutId, beanList);
    }

    @Override
    public void onViewHolderCreated(ViewHolder holder, View itemView) {
        super.onViewHolderCreated(holder, itemView);

        RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT,
                                                        RecyclerView.LayoutParams.MATCH_PARENT);
        params.leftMargin = 90;
        params.rightMargin = 90;
        itemView.setLayoutParams(params);
    }

    @Override
    protected void convert(final ViewHolder holder, CommunityBean communityBean, int position) {
        CommunityBean bean = mDatas.get(position);
        if (bean == null) {
            return;
        }

        holder.<TextView>getView(R.id.tv_title).setText(bean.title);
        holder.<TextView>getView(R.id.tv_content).setText(Html.fromHtml(bean.content));

        if ("1".equals(bean.type)) {
            holder.<TextView>getView(R.id.tv_bottom).setText("我要报名");
            holder.<TextView>getView(R.id.tv_bottom).setBackgroundResource(R.drawable.shape_btn_bg_green_corners8);
        } else if ("2".equals(bean.type)) {
        } else {
            holder.<TextView>getView(R.id.tv_bottom).setText("我要加入");
            holder.<TextView>getView(R.id.tv_bottom).setBackgroundResource(R.drawable.shape_btn_bg_blue_corners8);
        }

        holder.<TextView>getView(R.id.tv_bottom).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    int position = holder.getAdapterPosition();
                    mOnItemClickListener.onItemClick(v, holder , position);
                }
            }
        });

        GlideUtil.loadImage(holder.getConvertView().getContext(),
                bean.imageUrl, R.drawable.icon_error_image, holder.<ImageView>getView(R.id.iv_cover));
    }

    public void updateList(List<CommunityBean> list) {
        mDatas.clear();
        mDatas.addAll(list);
        notifyDataSetChanged();
    }
}
