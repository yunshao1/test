package com.iflytek.cyber.iot.show.core.data.net.response;

import java.util.List;

public class RealTimeByGroupBena {


    /**
     * message : success
     * list : {"indicatorData":[{"items":[{"items":[],"indicatorId":"1","name":"舒张压","unit":"mmgh","value":"600","createTime":"2019-07-08T19:39:02.723+08:00"},{"items":[],"indicatorId":"1","alarmLevel":"0","name":"收缩压","unit":"mmgh","value":"98","normalHighValue":"110","normalLowValue":"130","createTime":"2019-05-25T00:00:00+08:00"}],"indicatorId":"3","name":"心率","unit":"次/分","value":"80","createTime":"2019-05-25T00:00:00+08:00","sort":1},{"items":[],"indicatorId":"4","name":"步数","unit":"步","value":"6523","createTime":"2019-05-25T12:00:00+08:00","sort":2},{"items":[{"items":[],"indicatorId":"1","name":"舒张压","unit":"mmgh","value":"600","createTime":"2019-07-08T19:39:02.723+08:00"},{"items":[],"indicatorId":"1","alarmLevel":"0","name":"收缩压","unit":"mmgh","value":"98","normalHighValue":"110","normalLowValue":"130","createTime":"2019-05-25T00:00:00+08:00"}],"indicatorId":"1","name":"血压        ","createTime":"2019-05-25T00:00:00+08:00","sort":3},{"items":[],"indicatorId":"5","name":"血糖","unit":"mmol/L","value":"7.3","normalHighValue":"3.9","normalLowValue":"6.1","createTime":"2019-05-25T00:00:00+08:00","sort":4}],"alerts":"<b>用药提醒<\/b><br />上午 8:00  饭前服用半粒甲状腺药<br />下午 18:00  饭后服用2粒降血压药"}
     * tryTxt : 试试，“蓝小飞，我今天走多少步了”
     * readTxt :
     */

    private String message;
    private ListBean list;
    private String tryTxt;
    private String readTxt;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ListBean getList() {
        return list;
    }

    public void setList(ListBean list) {
        this.list = list;
    }

    public String getTryTxt() {
        return tryTxt;
    }

    public void setTryTxt(String tryTxt) {
        this.tryTxt = tryTxt;
    }

    public String getReadTxt() {
        return readTxt;
    }

    public void setReadTxt(String readTxt) {
        this.readTxt = readTxt;
    }

    public static class ListBean {
        /**
         * indicatorData : [{"items":[{"items":[],"indicatorId":"1","name":"舒张压","unit":"mmgh","value":"600","createTime":"2019-07-08T19:39:02.723+08:00"},{"items":[],"indicatorId":"1","alarmLevel":"0","name":"收缩压","unit":"mmgh","value":"98","normalHighValue":"110","normalLowValue":"130","createTime":"2019-05-25T00:00:00+08:00"}],"indicatorId":"3","name":"心率","unit":"次/分","value":"80","createTime":"2019-05-25T00:00:00+08:00","sort":1},{"items":[],"indicatorId":"4","name":"步数","unit":"步","value":"6523","createTime":"2019-05-25T12:00:00+08:00","sort":2},{"items":[{"items":[],"indicatorId":"1","name":"舒张压","unit":"mmgh","value":"600","createTime":"2019-07-08T19:39:02.723+08:00"},{"items":[],"indicatorId":"1","alarmLevel":"0","name":"收缩压","unit":"mmgh","value":"98","normalHighValue":"110","normalLowValue":"130","createTime":"2019-05-25T00:00:00+08:00"}],"indicatorId":"1","name":"血压        ","createTime":"2019-05-25T00:00:00+08:00","sort":3},{"items":[],"indicatorId":"5","name":"血糖","unit":"mmol/L","value":"7.3","normalHighValue":"3.9","normalLowValue":"6.1","createTime":"2019-05-25T00:00:00+08:00","sort":4}]
         * alerts : <b>用药提醒</b><br />上午 8:00  饭前服用半粒甲状腺药<br />下午 18:00  饭后服用2粒降血压药
         */

        private String alerts;
        private List<IndicatorDataBean> indicatorData;

        public String getAlerts() {
            return alerts;
        }

        public void setAlerts(String alerts) {
            this.alerts = alerts;
        }

        public List<IndicatorDataBean> getIndicatorData() {
            return indicatorData;
        }

        public void setIndicatorData(List<IndicatorDataBean> indicatorData) {
            this.indicatorData = indicatorData;
        }

        public static class IndicatorDataBean {
            /**
             * items : [{"items":[],"indicatorId":"1","name":"舒张压","unit":"mmgh","value":"600","createTime":"2019-07-08T19:39:02.723+08:00"},{"items":[],"indicatorId":"1","alarmLevel":"0","name":"收缩压","unit":"mmgh","value":"98","normalHighValue":"110","normalLowValue":"130","createTime":"2019-05-25T00:00:00+08:00"}]
             * indicatorId : 3
             * name : 心率
             * unit : 次/分
             * value : 80
             * createTime : 2019-05-25T00:00:00+08:00
             * sort : 1
             * normalHighValue : 3.9
             * normalLowValue : 6.1
             */

            private String indicatorId;
            private String name;
            private String unit;
            private String value;
            private String createTime;
            private String sort;
            private String normalHighValue;
            private String normalLowValue;
            private List<ItemsBean> items;

            public String getIndicatorId() {
                return indicatorId;
            }

            public void setIndicatorId(String indicatorId) {
                this.indicatorId = indicatorId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getUnit() {
                return unit;
            }

            public void setUnit(String unit) {
                this.unit = unit;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getSort() {
                return sort;
            }

            public void setSort(String sort) {
                this.sort = sort;
            }

            public String getNormalHighValue() {
                return normalHighValue;
            }

            public void setNormalHighValue(String normalHighValue) {
                this.normalHighValue = normalHighValue;
            }

            public String getNormalLowValue() {
                return normalLowValue;
            }

            public void setNormalLowValue(String normalLowValue) {
                this.normalLowValue = normalLowValue;
            }

            public List<ItemsBean> getItems() {
                return items;
            }

            public void setItems(List<ItemsBean> items) {
                this.items = items;
            }

            public static class ItemsBean {
                /**
                 * items : []
                 * indicatorId : 1
                 * name : 舒张压
                 * unit : mmgh
                 * value : 600
                 * createTime : 2019-07-08T19:39:02.723+08:00
                 * alarmLevel : 0
                 * normalHighValue : 110
                 * normalLowValue : 130
                 */

                private String indicatorId;
                private String name;
                private String unit;
                private String value;
                private String createTime;
                private String alarmLevel;
                private String normalHighValue;
                private String normalLowValue;
                private List<?> items;

                public String getIndicatorId() {
                    return indicatorId;
                }

                public void setIndicatorId(String indicatorId) {
                    this.indicatorId = indicatorId;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getUnit() {
                    return unit;
                }

                public void setUnit(String unit) {
                    this.unit = unit;
                }

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getCreateTime() {
                    return createTime;
                }

                public void setCreateTime(String createTime) {
                    this.createTime = createTime;
                }

                public String getAlarmLevel() {
                    return alarmLevel;
                }

                public void setAlarmLevel(String alarmLevel) {
                    this.alarmLevel = alarmLevel;
                }

                public String getNormalHighValue() {
                    return normalHighValue;
                }

                public void setNormalHighValue(String normalHighValue) {
                    this.normalHighValue = normalHighValue;
                }

                public String getNormalLowValue() {
                    return normalLowValue;
                }

                public void setNormalLowValue(String normalLowValue) {
                    this.normalLowValue = normalLowValue;
                }

                public List<?> getItems() {
                    return items;
                }

                public void setItems(List<?> items) {
                    this.items = items;
                }
            }
        }
    }
}
