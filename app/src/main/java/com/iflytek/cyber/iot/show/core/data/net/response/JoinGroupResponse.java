package com.iflytek.cyber.iot.show.core.data.net.response;

/**
 * @author cloud.wang
 * @date 2019-07-13
 * @Description: TODO
 */
public class JoinGroupResponse {

    private String connectionId;
    private String userId;
    private String userName;
    private String group;
    private int groupType;

    public String getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getGroupType() {
        return groupType;
    }

    public void setGroupType(int groupType) {
        this.groupType = groupType;
    }

    @Override
    public String toString() {
        return "JoinGroupResponse{" +
                "connectionId='" + connectionId + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", group='" + group + '\'' +
                ", groupType=" + groupType +
                '}';
    }
}
