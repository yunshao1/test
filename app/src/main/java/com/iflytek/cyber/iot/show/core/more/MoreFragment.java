package com.iflytek.cyber.iot.show.core.more;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.fragment.BaseFragment;
import com.iflytek.cyber.iot.show.core.utils.DialogUtils;

import androidx.navigation.fragment.NavHostFragment;

public class MoreFragment extends BaseFragment {
    private RecyclerView recly_view;
    private LinearLayout image;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_more, container, false);
    }


    private void initView(View view) {
        setHideBottomBar(false);
        recly_view = view.findViewById(R.id.recycler);
        image = view.findViewById(R.id.image);
        MoreAdapter moreAdapter = new MoreAdapter(getActivity());
        moreAdapter.setItemClickListener(new MoreAdapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(View view, int position) {
                if (position == 0) {
                    NavHostFragment.findNavController(MoreFragment.this).navigate(R.id.action_more_to_community);
                } else if (position == 2) {
                    NavHostFragment.findNavController(MoreFragment.this).navigate(R.id.action_to_healthy);
                } else if (position == 1) {
                    NavHostFragment.findNavController(MoreFragment.this).navigate(R.id.action_to_service);
                } else if (position == 3) {
                    NavHostFragment.findNavController(MoreFragment.this).navigate(R.id.action_to_my_order);
                } else if (position == 4) {
                    NavHostFragment.findNavController(MoreFragment.this).navigate(R.id.action_to_ask_health);
                } else if (position == 5) {
                    NavHostFragment.findNavController(MoreFragment.this).navigate(R.id.action_to_online_fragment);
                } else if (position == 10) {
                    Bundle bundle4 = new Bundle();
                    bundle4.putString("title", "周边药房");
                    bundle4.putString("code", "T-005");
                    bundle4.putString("apiUrl", "http://speaker.api.shfusion.com/api/Service/GetByCode");
                    NavHostFragment.findNavController(MoreFragment.this).navigate(R.id.action_to_pharmacy_fragment,bundle4);
                } else if (position == 11) {
                    Bundle bundle4 = new Bundle();
                    bundle4.putString("title", "便民维修");
                    bundle4.putString("code", "T-007");
                    bundle4.putString("apiUrl", "http://speaker.api.shfusion.com/api/Service/GetByCode");
                    NavHostFragment.findNavController(MoreFragment.this).navigate(R.id.action_to_home_repair_fragment,bundle4);
                } else if (position == 12) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("is_online_ask", false);
                    NavHostFragment.findNavController(MoreFragment.this).navigate(R.id.action_to_online_fragment, bundle);
                } else if (position == 14) {
                    Bundle bundle4 = new Bundle();
                    bundle4.putString("title", "健康膳食");
                    bundle4.putString("code", "T-009");
                    bundle4.putString("apiUrl", "http://speaker.api.shfusion.com/api/Service/GetByCode");
                    NavHostFragment.findNavController(MoreFragment.this).navigate(R.id.action_to_nutritious_fragment,bundle4);
                } else if (position == 6) {
                    NavHostFragment.findNavController(MoreFragment.this).navigate(R.id.action_to_video_fragment);
                } else if (position == 17) {

                    NavHostFragment.findNavController(MoreFragment.this).navigate(R.id.action_to_person_list_message_fragment);
                }
                else if (position == 18) {
                    NavHostFragment.findNavController(MoreFragment.this).navigate(R.id.action_to_my_reminder_fragment);
                }
                else {
                    DialogUtils.showMyToast(getActivity(), "暂未开放\n敬请期待");
                }

            }
        });

        recly_view.setAdapter(moreAdapter);
        recly_view.setLayoutManager(new GridLayoutManager(getActivity(), 4));

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(MoreFragment.this).navigate(R.id.action_out_to_more);
            }
        });
    }

}
