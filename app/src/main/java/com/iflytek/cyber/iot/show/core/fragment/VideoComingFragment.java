package com.iflytek.cyber.iot.show.core.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.video.AudioUtil;
import com.example.video.MainActivity;
import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.TRTCMainActivity;
import com.iflytek.cyber.iot.show.core.adapter.VideoAdapter;
import com.iflytek.cyber.iot.show.core.chat.ChatDetailMessage;
import com.iflytek.cyber.iot.show.core.chat.SessionParamBean;
import com.iflytek.cyber.iot.show.core.chat.event.ChatEvent;
import com.iflytek.cyber.iot.show.core.chat.manager.VideoManager;
import com.iflytek.cyber.iot.show.core.data.local.LoginSession;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.BaseResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.JoinGroupResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.VideoUserBean;
import com.iflytek.cyber.iot.show.core.impl.SpeechRecognizer.SpeechRecognizerHandler;
import com.iflytek.cyber.iot.show.core.utils.Constant;
import com.iflytek.cyber.iot.show.core.utils.RoomUtils;
import com.iflytek.cyber.iot.show.core.utils.SharedPreUtils;
import com.iflytek.cyber.iot.show.core.utils.ToastUtil;
import com.tencent.trtc.TRTCCloudDef;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Logger;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.navigation.fragment.NavHostFragment;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class VideoComingFragment extends BaseFragment {
    private final static int REQ_PERMISSION_CODE = 0x1000;

    LinearLayout ll_finsh;
    private View view;
    private View hangupView;
    private View comingView;
    private SpeechRecognizerHandler instance;
    private Ringtone ringtone;
    private int roomId;
    private String name;
    private String img;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private String groupId;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQ_PERMISSION_CODE:
                for (int ret : grantResults) {
                    if (PackageManager.PERMISSION_GRANTED != ret) {
                        Toast.makeText(getActivity(), "用户没有允许需要的权限，使用可能会受到限制！", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            default:
                break;
        }
    }

    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> permissions = new ArrayList<>();
            if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO)) {
                permissions.add(Manifest.permission.RECORD_AUDIO);
            }

            if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)) {
                permissions.add(Manifest.permission.CAMERA);
            }
            if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }

            if (permissions.size() != 0) {
                requestPermissions((String[]) permissions.toArray(new String[0]),
                        REQ_PERMISSION_CODE);
                return false;
            }
        }

        return true;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_video_remind, container, false);
            initView();
        }

        return view;
    }


    private void initView() {
        EventBus.getDefault().register(this);

        if (getArguments() != null) {
            try {
                roomId = Integer.parseInt(getArguments().getString("roomId"));
            } catch (Exception e) {
                ToastUtil.showLongToastCenter("房间号格式不对，无法进入房间");
                exitPage();
            }

            name = getArguments().getString("name");
            img = getArguments().getString("img");
            groupId = getArguments().getString("groupId");
        } else {
            ToastUtil.showLongToastCenter("没有参数，找不到房间");
            exitPage();
        }

        Uri defaultRingtoneUri = RingtoneManager.getActualDefaultRingtoneUri(getContext(), RingtoneManager.TYPE_RINGTONE);
        ringtone = RingtoneManager.getRingtone(getContext(), defaultRingtoneUri);
        ringtone.play();

        TextView tv_handover = view.findViewById(R.id.tv_handover);
        TextView tv_handup = view.findViewById(R.id.tv_handup);
        ImageView iv_from_img = view.findViewById(R.id.iv_from_img);
        TextView tv_name = view.findViewById(R.id.tv_name);

        hangupView = view.findViewById(R.id.cl_hang_up);
        comingView = view.findViewById(R.id.fl_coming);

        comingView.setVisibility(View.VISIBLE);
        hangupView.setVisibility(View.GONE);

        TextView tvClose = view.findViewById(R.id.tv_close);
        ImageView ivFromAvatar = view.findViewById(R.id.iv_from_avatar);
        TextView tvFromName = view.findViewById(R.id.tv_from_name);
        ImageView ivSend = view.findViewById(R.id.iv_send);

        Glide.with(this).load(img).into(iv_from_img);
        Glide.with(this).load(img).into(ivFromAvatar);
        tv_name.setText(name);
        tvFromName.setText(name);
        tv_handover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //发送挂断消息
                HashMap<String, String> map = new HashMap<>();
                map.put("toName", name);
                map.put("video_status", "2");
                VideoManager.getInstance().initiateVideoCall(getLauncher(),
                        roomId, groupId, map, new VideoManager.RequestCallBack<JoinGroupResponse>() {
                            @Override
                            public void onSuccess(JoinGroupResponse joinGroupResponse) {
                                exitPage();
                            }

                            @Override
                            public void onFailed(String msg) {
                                ToastUtil.showLongToastCenter("挂断失败，请重试" + msg);
                            }
                        });

            }
        });

        tv_handup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onJoinRoom(roomId);
            }
        });

        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().unregister(this);
                NavHostFragment.findNavController(VideoComingFragment.this).navigateUp();
            }
        });

        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, String> map = new HashMap<>();
                map.put("groupName", name);
                map.put("active", "true");
                map.put("img", img);
                map.put("video_status", "0");
                map.put("toName", name);
                VideoManager.getInstance().initiateVideoCall(getLauncher(), RoomUtils.getRoomId(), groupId, map);
            }
        });

        checkPermission();
        this.instance = SpeechRecognizerHandler.getInstance(getContext(), null, false, false, "");
    }

    public void exitPage() {
        EventBus.getDefault().unregister(this);
        ringtone.stop();
        NavHostFragment.findNavController(VideoComingFragment.this).navigateUp();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleEvent(ChatEvent chatEvent) {
        if (chatEvent.getMessageType() == ChatEvent.EVENT_VIDEO_DROPPED) {
            if (hangupView != null && comingView != null) {
                comingView.setVisibility(View.GONE);
                hangupView.setVisibility(View.VISIBLE);
            } else {
                exitPage();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        instance.startAudioInput();
    }

    private void onJoinRoom(final int roomId) {
        instance.stopAudioInput();
        exitPage();
        final Intent intent = new Intent(getActivity(), TRTCMainActivity.class);
        String userSig = LoginSession.INSTANCE.getSUserSig();
        String userId = LoginSession.INSTANCE.getSUserId();
        intent.putExtra("roomId", roomId);
        intent.putExtra("userId", userId);
        intent.putExtra("fromId", groupId);
        intent.putExtra("AppScene", TRTCCloudDef.TRTC_APP_SCENE_VIDEOCALL);
        intent.putExtra("customVideoCapture", false);
        intent.putExtra("videoFile", false);
        //（1） 从控制台获取的 json 文件中，简单获取几组已经提前计算好的 userid 和 usersig
        intent.putExtra("sdkAppId", Constant.APP_ID);
        intent.putExtra("userSig", userSig);
        intent.putExtra("groupId", groupId);
        intent.putExtra("img", "");
        intent.putExtra("name", name);
        startActivity(intent);
    }

}
