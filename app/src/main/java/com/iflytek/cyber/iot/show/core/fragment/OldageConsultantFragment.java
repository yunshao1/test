package com.iflytek.cyber.iot.show.core.fragment;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.adapter.OnlineInterrogationAdapter;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.ConsultBean;
import com.iflytek.cyber.iot.show.core.utils.DialogUtils;

import java.io.IOException;

import androidx.navigation.fragment.NavHostFragment;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * 养老顾问
 */
public class OldageConsultantFragment extends BaseFragment implements View.OnTouchListener {
    LinearLayout ll_finsh;
    private View view;
    private OnlineInterrogationAdapter adapter;
    private RecyclerView recly_view;
    private MediaPlayer mediaPlayer;
//    private ImageView im_ly;//录音
    private RelativeLayout re_top;
    private PopupWindow popupWindow;
    private TextView tv_title;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_online_interogation, container, false);
        }

        initView();
        load();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = new MediaPlayer();
        }
    }

    private void load() {

        NetHandler.Companion.getInst().getAdviseByGrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ConsultBean>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(ConsultBean listResponse) {
                        if ("success".equals(listResponse.getMessage())) {
                            setData(listResponse);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void setData(ConsultBean listResponse) {
        if (listResponse == null)
            return;
        adapter.setData(listResponse.getResult());

    }


    private void initView() {
        mediaPlayer = new MediaPlayer();
        ll_finsh = view.findViewById(R.id.ll_finsh);
        recly_view = view.findViewById(R.id.recycler);
//        im_ly = view.findViewById(R.id.im_ly);
        re_top = view.findViewById(R.id.re_top);
        tv_title = view.findViewById(R.id.tv_title);
        tv_title.setText("养老顾问");
//        im_ly.setOnTouchListener(this);
        adapter = new OnlineInterrogationAdapter(getActivity());
        recly_view.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        recly_view.setAdapter(adapter);
        ll_finsh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(OldageConsultantFragment.this).navigateUp();

            }
        });
        adapter.setOnItemClickListener(new OnlineInterrogationAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String url) {
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = new MediaPlayer();
                    try {
                        mediaPlayer.setDataSource(url);
                        mediaPlayer.prepare();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    mediaPlayer = new MediaPlayer();
                    try {
                        mediaPlayer.setDataSource(url);
                        mediaPlayer.prepare();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                mediaPlayer.start();
            }
        });
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.im_ly:
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    popupWindow = DialogUtils.showToalkPop(getActivity(), re_top);
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (popupWindow != null && popupWindow.isShowing()) {
                        popupWindow.dismiss();
                    }
                }
                break;
        }

        return true;
    }
}
