package com.iflytek.cyber.iot.show.core.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.ActionResponse;
import com.iflytek.cyber.iot.show.core.data.net.response.CommunityBean;
import com.iflytek.cyber.iot.show.core.data.net.response.Response;
import com.iflytek.cyber.iot.show.core.widget.recyclerview.AlphaTransformer;
import com.iflytek.cyber.iot.show.core.widget.recyclerview.FlingRecycleView;
import com.iflytek.cyber.iot.show.core.widget.recyclerview.GalleryLayoutManager;
import com.iflytek.cyber.iot.show.core.widget.recyclerview.LinearItemDecoration;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.navigation.fragment.NavHostFragment;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class CommunityFragment extends BaseFragment {

    TextView mBackTv;

    FlingRecycleView mRecycler;

    private View mContentView;
    private CommunityAdapter mAdapter;
    private int mPosition;

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mContentView == null) {
            mContentView = inflater.inflate(R.layout.fragment_community, container, false);
            Log.i("Cloud", "onCreateView mContentView == null");
            initView();
        }

        Log.i("Cloud", "onCreateView ====");

        return mContentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.i("Cloud", "onViewCreated ====");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.i("Cloud", "onActivityCreated ====");

        load("");
    }

    @Override
    public void onStart() {
        super.onStart();

        Log.i("Cloud", "onStart ====");
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.i("Cloud", "onResume ====");
    }

    private void initView() {
        mBackTv = mContentView.findViewById(R.id.tv_back);
        mBackTv.setText("社区邻里");
        mBackTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(CommunityFragment.this).navigateUp();
            }
        });

        mRecycler = mContentView.findViewById(R.id.recycler);

        mRecycler.setFlingAble(false);
        GalleryLayoutManager layoutManager = new GalleryLayoutManager(GalleryLayoutManager.HORIZONTAL);
        layoutManager.attach(mRecycler, 0);
        layoutManager.setOnItemSelectedListener(new GalleryLayoutManager.OnItemSelectedListener() {
            @Override
            public void onItemSelected(RecyclerView recyclerView, View item, int position) {
                Log.i("Cloud", "onItemSelected====" + position);
                mPosition = position;
                CommunityBean bean = mAdapter.getDatas().get(position);
                playVoice(bean.readTxt);
            }
        });
        layoutManager.setItemTransformer(new AlphaTransformer());
        LinearItemDecoration divider = new LinearItemDecoration.Builder(mContentView.getContext())
                .setOrientation(LinearLayoutManager.HORIZONTAL)
                .setSpan(60f)
//                .setPadding(R.dimen.line_width)
//                .setLeftPadding(48f)
//                .setRightPadding(24f)
                .setColorResource(android.R.color.transparent)
                .setShowLastLine(true)
                .build();
        mRecycler.addItemDecoration(divider);

        mAdapter = new CommunityAdapter(mContentView.getContext(), R.layout.item_community, new ArrayList<CommunityBean>());
        mAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                if (view.getId() != R.id.tv_bottom) {
                    Log.i("Cloud", "其他部位被点击");
                    //item的点击事件不处理
                    return;
                }

                //点击了报名
                if (mAdapter.getDatas() == null || mAdapter.getDatas().size() < 1) return;

                if (mPosition != position) {
                    mPosition = position;
                }

                Log.i("Cloud", "R.id.tv_bottom被电击");
                submitAction("");
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
        mRecycler.setAdapter(mAdapter);
    }

    public void load(final String keyword) {

        NetHandler.Companion.getInst().fetchCommunity(keyword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<List<CommunityBean>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mCompositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Response<List<CommunityBean>> communityBeans) {
                        if (communityBeans != null && communityBeans.list != null) {
                            if (!TextUtils.isEmpty(keyword)) {
                                playVoice("以下是你要找的内容");
                            }

                            if (communityBeans.list != null && communityBeans.list.size() > 0) {
                                mAdapter.updateList(communityBeans.list);
                                setHideBottomBar(true);
                                setTipText(communityBeans.tryTxt, R.style.TextWhiteFont);
                            } else {
                                if (!TextUtils.isEmpty(keyword)) {
                                    playVoice("找不到你要的内容");
                                }
                            }
                        } else {
                            Toast.makeText(getLauncher(), communityBeans.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Cloud", "onError::"+e.getMessage());
                        Toast.makeText(getLauncher(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void playVoice(String speakText) {
        if (!TextUtils.isEmpty(speakText)) {
            NetHandler.Companion.getInst().playVoice(speakText);
        }
    }

    public void submitAction(final String input) {
        final CommunityBean bean = mAdapter.getDatas().get(mPosition);
        if (bean == null) {
            return;
        }
        Map<String, String> map = new HashMap<>();

        mCompositeDisposable.add(
                NetHandler.Companion.getInst().submitAction(bean.id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<ActionResponse>() {
                            @Override
                            public void accept(ActionResponse actionResponse) throws Exception {
                                if ("success".equals(actionResponse.getMessage())) {
                                    playVoice("报名成功");
                                    Bundle bundle = new Bundle();
                                    bundle.putString("title", TextUtils.isEmpty(input) ? bean.bottomTxt : input);
                                    bundle.putString("content", "好的，报名成功");
                                    NavHostFragment.findNavController(CommunityFragment.this).navigate(R.id.action_to_apply_fragment, bundle);
                                } else {
                                    Log.i("Cloud", actionResponse.getMessage());
//                                    Toast.makeText(getLauncher(), "报名失败，"+ actionResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                Log.i("Cloud", "onError::"+ throwable.getMessage());
//                                Toast.makeText(getLauncher(), "网络请求失败，导致报名失败，请重新"+ throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

//        mCompositeDisposable.dispose();
    }
}
