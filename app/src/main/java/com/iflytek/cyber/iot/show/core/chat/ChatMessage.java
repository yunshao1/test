package com.iflytek.cyber.iot.show.core.chat;

import com.iflytek.cyber.iot.show.core.utils.GsonUtil;

/**
 * @author cloud.wang
 * @date 2019-07-20
 * @Description: TODO
 */
public class ChatMessage {

    /**
     * Id : aa9023059aa0-8432-834e-68c9-15e1883f
     * GroupId : AppElderly.SOS_groupaa7320e90032-b574-a045-21ba-69bdb2ee-Chat
     * Group : aa7320e90032-b574-a045-21ba-69bdb2ee
     * AppKey : Elderly.SOS
     * GroupType : 2
     * FromId : 6190
     * FromName : lyj
     * MsgType : txt
     * MsgContent : {\"msgType\":\"txt\",\"msgContent\":\"Hello World!!!\",\"iconImg\":\"http://elderlyservice.shfusion.com/Image/newTow/avatar1@2x.jpg\",\"name\":\"Mobile\",\"nickname\":\"客户端开发\"}
     * SendTime : 2019-07-20T16:19:16.9824147+08:00
     */
    private String id;
    private String toId;
    private String toName;
    private String type;
    private String groupId;
    private String group;
    private String appKey;
    private int groupType;
    private String fromId;
    private String fromName;
    /**
     * txt   文本   img   图片   audio   音频   video   视频
     */
    private String msgType;
    private String msgContent;
    private String sendTime;
    private long sendTimeSeconds;
    private ChatDetailMessage detailMessage;

    public ChatDetailMessage getDetailMessage() {
        return GsonUtil.INSTANCE.convertChatDetailMessage(msgContent);
    }

    public void setDetailMessage(ChatDetailMessage detailMessage) {
        this.detailMessage = detailMessage;
    }

    public long getSendTimeSeconds() {
        return sendTimeSeconds;
    }

    public void setSendTimeSeconds(long sendTimeSeconds) {
        this.sendTimeSeconds = sendTimeSeconds;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public int getGroupType() {
        return groupType;
    }

    public void setGroupType(int groupType) {
        this.groupType = groupType;
    }

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ChatMessage{" +
                "id='" + id + '\'' +
                ", toId='" + toId + '\'' +
                ", toName='" + toName + '\'' +
                ", type='" + type + '\'' +
                ", groupId='" + groupId + '\'' +
                ", group='" + group + '\'' +
                ", appKey='" + appKey + '\'' +
                ", groupType=" + groupType +
                ", fromId='" + fromId + '\'' +
                ", fromName='" + fromName + '\'' +
                ", msgType='" + msgType + '\'' +
                ", msgContent='" + msgContent + '\'' +
                ", sendTime='" + sendTime + '\'' +
                ", sendTimeSeconds=" + sendTimeSeconds +
                ", detailMessage=" + detailMessage +
                '}';
    }
}
