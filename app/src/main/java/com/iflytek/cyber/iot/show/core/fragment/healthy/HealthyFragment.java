package com.iflytek.cyber.iot.show.core.fragment.healthy;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.AdBeanResonse;
import com.iflytek.cyber.iot.show.core.data.net.response.RealTimeByGroupBena;
import com.iflytek.cyber.iot.show.core.fragment.BaseFragment;
import com.iflytek.cyber.iot.show.core.utils.Constant;
import com.iflytek.cyber.iot.show.core.utils.DateUtils;
import com.iflytek.cyber.iot.show.core.utils.GlideUtil;

import java.util.List;

import androidx.navigation.fragment.NavHostFragment;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * 健康小蜜
 */
public class HealthyFragment extends BaseFragment implements View.OnClickListener {
    LinearLayout ll_finsh;

    private View view;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private TextView tv_bushu_value;
    private TextView tv_bushu_time;
    private TextView tv_xinlv_value;
    private TextView tv_xinlv_time;
    private TextView tv_xueya_value;
    private TextView tv_xueya_time;
    private TextView tv_xuetang_value;
    private TextView tv_xuetang_time;

    private LinearLayout ll_zxwz;
    private LinearLayout ll_sqyy;
    private LinearLayout ll_bushu;
    private LinearLayout ll_view1;
    private LinearLayout ll_view2;
    private LinearLayout ll_view3;

    private LinearLayout ll_health_report;//健康报告

    private TextView eat_y;


    private ImageView image2, image1;
    private TextView tv_title1, tv_title2, tv_subtitle1, tv_subtitle2;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_healthy, container, false);
            initView();
        }
        return view;
    }

    private void load() {

        NetHandler.Companion.getInst().getRealTimeByGrop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RealTimeByGroupBena>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        mCompositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(RealTimeByGroupBena listResponse) {
                        if ("success".equals(listResponse.getMessage())) {
                            setData(listResponse);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private String getStr() {
        if (Constant.rmindList == null)
            return "";
        String str = "";
        String min ="";
        for (int i = 0; i < Constant.rmindList.size(); i++) {
            if ("1".equals(Constant.rmindList.get(i).getType())) {
                str = Constant.rmindList.get(i).getText();
                min = Constant.rmindList.get(i).getHour()+":"+Constant.rmindList.get(i).getMinute();
                if (TextUtils.isEmpty(str)){
                    str ="语音提醒";
                }
                break;
            }
        }
        return min+str;
    }

    private void setData(RealTimeByGroupBena listResponse) {
        if (listResponse.getList() == null)
            return;

        eat_y.setText("用药提醒\n" + getStr());

        List<RealTimeByGroupBena.ListBean.IndicatorDataBean> list = listResponse.getList().getIndicatorData();
        if (list==null)
            return;
        for (int i = 0; i < list.size(); i++) {
            RealTimeByGroupBena.ListBean.IndicatorDataBean byGroupBena = list.get(i);
            if ("1".equals(list.get(i).getSort())) {
                tv_xinlv_value.setText(byGroupBena.getValue());
                tv_xinlv_time.setText(DateUtils.getReturnDate(byGroupBena.getCreateTime()));

            }
            if ("2".equals(list.get(i).getSort())) {
                tv_bushu_value.setText(byGroupBena.getValue());
                tv_bushu_time.setText(DateUtils.getReturnDate(byGroupBena.getCreateTime()));
            }
            if ("3".equals(list.get(i).getSort())) {
                try {
                    tv_xueya_value.setText(byGroupBena.getItems().get(0).getValue() + "/" + byGroupBena.getItems().get(1).getValue());
                } catch (Exception e) {
                }
                tv_xueya_time.setText(DateUtils.getReturnDate(byGroupBena.getCreateTime()));
            }
            if ("4".equals(list.get(i).getSort())) {
                tv_xuetang_value.setText(byGroupBena.getValue());
                tv_xuetang_time.setText(DateUtils.getReturnDate(byGroupBena.getCreateTime()));
            }

        }


    }
    @Override
    public void onActivityCreated(@org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHideBottomBar(true);
        setTipText("试试，“蓝小飞,我今天走多少步了”", R.style.TextBlackFont);

    }

    private void initView() {
        ll_finsh = view.findViewById(R.id.ll_finsh);

        tv_bushu_value = view.findViewById(R.id.tv_bushu_value);
        tv_bushu_time = view.findViewById(R.id.tv_bushu_time);

        tv_xinlv_value = view.findViewById(R.id.tv_xinlv_value);
        tv_xinlv_time = view.findViewById(R.id.tv_xinlv_time);

        tv_xueya_value = view.findViewById(R.id.tv_xueya_value);
        tv_xueya_time = view.findViewById(R.id.tv_xueya_time);

        tv_xuetang_value = view.findViewById(R.id.tv_xuetang_value);
        tv_xuetang_time = view.findViewById(R.id.tv_xuetang_time);
        eat_y = view.findViewById(R.id.eat_y);
        ll_health_report = view.findViewById(R.id.ll_health_report);


        ll_zxwz = view.findViewById(R.id.ll_zxwz);
        ll_sqyy = view.findViewById(R.id.ll_sqyy);
        ll_bushu = view.findViewById(R.id.ll_bushu);
        ll_view1 = view.findViewById(R.id.ll_view1);
        ll_view2 = view.findViewById(R.id.ll_view2);
        ll_view3 = view.findViewById(R.id.ll_view3);


        tv_title1 = view.findViewById(R.id.tv_title1);
        tv_title2 = view.findViewById(R.id.tv_title2);

        image2 = view.findViewById(R.id.image2);
        image1 = view.findViewById(R.id.image1);

        tv_subtitle1 = view.findViewById(R.id.tv_subtitle1);
        tv_subtitle2 = view.findViewById(R.id.tv_subtitle2);


        ll_zxwz.setOnClickListener(this);
        ll_sqyy.setOnClickListener(this);
        ll_bushu.setOnClickListener(this);
        ll_view1.setOnClickListener(this);
        ll_view2.setOnClickListener(this);
        ll_view3.setOnClickListener(this);




        ll_health_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(HealthyFragment.this).navigate(R.id.action_to_healthy_report_fragment);
            }
        });
        ll_finsh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(HealthyFragment.this).navigateUp();

            }
        });
        load();
        loadAd("H-001", "http://speaker.api.shfusion.com/api/Ad/Get");
    }

    private void loadAd(String code, String url) {
        if (TextUtils.isEmpty(url))
            return;
        NetHandler.Companion.getInst().getAd(url, code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AdBeanResonse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(AdBeanResonse listResponse) {
                        if (listResponse != null && listResponse.getList() != null && listResponse.getList().size() > 0) {
                            tv_title1.setText(listResponse.getList().get(0).getTitle());
                            tv_subtitle1.setText(listResponse.getList().get(0).getContent());
                            GlideUtil.loadImage(getActivity(), listResponse.getList().get(0).getImg(), 0, image1);
                            if (listResponse.getList().size() > 1) {
                                tv_title2.setText(listResponse.getList().get(1).getTitle());
                                tv_subtitle2.setText(listResponse.getList().get(1).getContent());
                                GlideUtil.loadImage(getActivity(), listResponse.getList().get(0).getImg(), 0, image2);
                            }
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_zxwz:
                NavHostFragment.findNavController(HealthyFragment.this).navigate(R.id.action_to_online_fragment);

                break;
            case R.id.ll_sqyy:
                Bundle bundle = new Bundle();
                bundle.putString("title","社区预约");
                bundle.putString("url", Constant.URL_SQYY);
                NavHostFragment.findNavController(HealthyFragment.this).navigate(R.id.action_to_webview, bundle);

                break;
            case R.id.ll_bushu:
                Bundle bundle1 = new Bundle();
                bundle1.putString("title", "健康数据");
                bundle1.putString("url", Constant.URL_BUSHU);
                NavHostFragment.findNavController(HealthyFragment.this).navigate(R.id.action_to_webview, bundle1);

                break;
            case R.id.ll_view1:
                Bundle bundle2 = new Bundle();
                bundle2.putString("title", "健康数据");
                bundle2.putString("url", Constant.URL_XINLV);
                NavHostFragment.findNavController(HealthyFragment.this).navigate(R.id.action_to_webview, bundle2);

                break;
            case R.id.ll_view2:
                Bundle bundle3 = new Bundle();
                bundle3.putString("title", "健康数据");
                bundle3.putString("url", Constant.URL_XUEYA);
                NavHostFragment.findNavController(HealthyFragment.this).navigate(R.id.action_to_webview, bundle3);

                break;
            case R.id.ll_view3:
                Bundle bundle4 = new Bundle();
                bundle4.putString("title", "健康数据");
                bundle4.putString("url", Constant.URL_XUETANG);
                NavHostFragment.findNavController(HealthyFragment.this).navigate(R.id.action_to_webview, bundle4);

                break;
        }
    }
}
