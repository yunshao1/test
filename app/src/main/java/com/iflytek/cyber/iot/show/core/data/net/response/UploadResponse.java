package com.iflytek.cyber.iot.show.core.data.net.response;

import android.text.TextUtils;

/**
 * @author cloud.wang
 * @date 2019/6/9
 * @Description: TODO
 */
public class UploadResponse {

    private String msg;
    private ResultBean result;
    private String result_code;

    public boolean isSuccess() {
        return TextUtils.equals("200", result_code);
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getResult_code() {
        return result_code;
    }

    public void setResult_code(String result_code) {
        this.result_code = result_code;
    }

    public static class ResultBean {
        /**
         * name : 账号.txt
         * path : /upload/msg/8324f4b416044629acb46c0c29aa9940.txt
         * url : http://common.api.shfusion.com/upload/msg/8324f4b416044629acb46c0c29aa9940.txt
         */
        private String name;
        private String path;
        private String url;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
