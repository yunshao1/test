package com.iflytek.cyber.iot.show.core.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * @author cloud.wang
 * @date 2019/6/1
 * @Description: TODO
 */
class CustomDirective : Serializable {

    //{\"IntentType\":\"navigate\",\"Type\":1,\"Input\":\"打开服务城\",\"Output\":\"serviceCityPage\"}

    @SerializedName("IntentType")
    lateinit var intentType: String //跳转意图

    @SerializedName("Type")
    lateinit var type: String   //1：关键词或者页面标识，0：音箱回复的播放的文本

    @SerializedName("Input")
    lateinit var input: String  //所说文本

    @SerializedName("Output")
    lateinit var output: String //关键字/页面跳转标识，音箱回复的播放的文本

    override fun toString(): String {
        return "CustomDirective(intentType='$intentType', type='$type', input='$input', output='$output')"
    }

}