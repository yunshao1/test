package com.iflytek.cyber.iot.show.core.data.net.service

import com.iflytek.cyber.iot.show.core.data.net.response.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Kaka on 19/1/10.
 */
interface NetService {

    @POST("/api/Account/Login")
    @Headers("Content-Type: application/json")
    fun login(@Body requestBody: RequestBody): Observable<LoginBean>


    @GET("/api/Neighbor/Get")
    fun fetchCommunityContent(@Query("RegionCode") code: String,
                              @Query("PageSize") pageSize: Int,
                              @Query("PageNo") pageNo: Int,
                              @Query("BeginTime") BeginTime: String,
                              @Query("EndTime") EndTime: String,
                              @Query("Keyword") Keyword: String): Observable<Response<List<CommunityBean>>>

    @GET("/api/Health/GetHealth")
    fun getRealTimeByGrop(): Observable<RealTimeByGroupBena>


    @GET("/api/Ask/Get")
    fun getAskHealthByGrop(): Observable<AskHealthBean>

    @GET("/api/Order/Get")
    fun getMyOrderyGrop(
                        @Query("keyword")Keyword: String,
                        @Query("pageSize") PageSize: Int,
                        @Query("pageNo")PageNo: Int): Observable<MyOrederBean>

    @GET("/neighbor/api/Service/GetService")
    fun getService(@Query("type") Type: String,
                   @Query("keyword") Keyword: String,
                   @Query("pageSize") PageSize: Int,
                   @Query("pageNo") PageNo: Int): Observable<ServiceBean>


    @POST
    fun getClassify(@Url url: String, @Body requestBody: RequestBody): Observable<ClassifyBean>

//    @POST("/neighbor/api/Org/Action")
//    @Headers("Content-Type: application/json")
//    @FormUrlEncoded
//    fun submitAction(@Field("id") id: String, @Field("elderId") elderId: String,
//                     @Field("actionName") actionName: String): Observable<ActionResponse>

    @POST("/api/Neighbor/Action")
    @Headers("Content-Type: application/json")
    fun submitAction(@Body requestBody: RequestBody): Observable<ActionResponse>

    @POST("/api/Service/Action")
    @Headers("Content-Type: application/json")
    fun submitServiceAction2(@Body requestBody: RequestBody): Observable<ActionResponse>

    @POST("/neighbor/api/Order/Order")
    @Headers("Content-Type: application/json")
    fun submitOrderAction3(@Body requestBody: RequestBody): Observable<ActionResponse>

    @GET("/api/Service/GetChildServiceTree")
    fun getByClassifyId(@Query("typeCode") Type: String,
                        @Query("keyword") Keyword: String,
                        @Query("pageSize") PageSize: Int,
                        @Query("pageNo") PageNo: Int): Observable<ByClassifyIdBean>


    @POST("http://https.shfusion.com/api/Broadcast/Broadcast")
    @Headers("Content-Type: application/json")
    fun playVoice(@Body requestBody: RequestBody): Call<RequestBody>

    @GET("/neighbor/api/Consult/Get")
    fun getConsultByGrop(@Query("elderId") elderId: String,
                         @Query("pageSize") PageSize: Int,
                         @Query("pageNo") PageNo: Int): Observable<ConsultBean>

    @GET("/neighbor/api/Advise/Get")
    fun getAdviseByGrop(@Query("elderId") elderId: String,
                        @Query("pageSize") PageSize: Int,
                        @Query("pageNo") PageNo: Int): Observable<ConsultBean>

    @GET("/api/Health/GetBriefReports")
    fun getFormsBriefly(): Observable<HealthReportBean>

    @GET("/api/SOS/Sos")
    fun sendSOS(@Query("DeviceId") elderId: String): Observable<Response<Empty>>

    @POST
    fun getService2(@Url url: String, @Body body: RequestBody): Observable<ServiceBean>

    @POST
    fun getService3(@Url url: String, @Body body: RequestBody): Observable<NutritiousMealsResonse>

    @POST
    fun getService4(@Url url: String, @Body body: RequestBody): Observable<RepairResponse>

    @POST("/api/Service/GetService")
    fun getPhDetail(@Body body: RequestBody): Observable<PharmacyDetailResponse>

    @GET("/neighbor/api/Service/GetBMWXService")
    fun getService7(
            @Query("keyword") Keyword: String,
            @Query("pageSize") PageSize: Int,
            @Query("pageNo") PageNo: Int): Observable<ServiceBean>

    @GET("/neighbor/api/Service/GetSLLXService")
    fun getService8(
            @Query("keyword") Keyword: String,
            @Query("pageSize") PageSize: Int,
            @Query("pageNo") PageNo: Int): Observable<ServiceBean>

    @GET("/neighbor/api/Service/GetJKSSService")
    fun getService9(
            @Query("keyword") Keyword: String,
            @Query("pageSize") PageSize: Int,
            @Query("pageNo") PageNo: Int): Observable<ServiceBean>


    @POST("/neighbor/api/Consult/SendConsultAsync")
    @Headers("Content-Type: application/json")
    fun sendConsultAsync(@Body requestBody: RequestBody): Observable<OnlineResBean>

    /**
     * 修改用户基本信息，同时支持上传头像和携带String参数
     * @param params    String参数集合
     * @param file  头像文件
     * @param token 登录/注册成功后的Token
     * @return
     */
    //设置用户信息,post表单上传
    @POST("/api/Common/Upload")
    @Multipart
    fun uploadVoice(@Part file: MultipartBody.Part): Observable<UploadResponse>

    @GET("/api/Service/GetPage")
    fun getPage(@Query("AppCode") appCode: String, @Query("PageCode") pageCode: String): Observable<ServiceNewBean>

    @GET
    fun getAd(@Url url: String, @Query("code") code: String): Observable<AdBeanResonse>

    @GET("/api/Remind/Get")
    fun getReminder(): Observable<ReminderResponse>

    @GET("/api/Remind/Close")
    fun closeReminder(@Query("id") id: String): Observable<CloseReminder>

    @GET("/api/Remind/Delete")
    fun deleteReminder(@Query("id") id: String): Observable<String>

    @POST("/api/Remind/Create")
    @Headers("Content-Type: application/json")
    fun creatRemind(@Body requestBody: RequestBody): Observable<CreatRemindResponse>

    @GET("/api/LeaveMessage/GetMessage")
    fun getMessage(@QueryMap map: Map<String, String>): Observable<GetMessageResponse>

    @POST("/api/LeaveMessage/Send")
    @Headers("Content-Type: application/json")
    fun sendMessage(@Body requestBody: RequestBody): Observable<SendMessageByLiuYanRespoonse>

    @POST("/api/LeaveMessage/Readed")
    @Headers("Content-Type: application/json")
    fun setReaded(@Body requestBody: RequestBody): Observable<CreatRemindResponse>

    @GET("/api/LeaveMessage/GetFamilyMessages")
    fun getFamilyMessages(): Observable<PersonListMessageResponse>

    @GET("/api/LeaveMessage/GetUnreadMessageAbstract")
    fun getUnreadMessageAbstract(): Observable<HomeUnderReadMessageResponse>
}
