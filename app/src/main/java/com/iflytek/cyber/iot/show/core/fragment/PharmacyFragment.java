package com.iflytek.cyber.iot.show.core.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.adapter.PharmacyAdapter;
import com.iflytek.cyber.iot.show.core.data.net.NetHandler;
import com.iflytek.cyber.iot.show.core.data.net.response.ClassifyBean;
import com.iflytek.cyber.iot.show.core.utils.DialogUtils;

import java.util.HashMap;
import java.util.Map;

import androidx.navigation.fragment.NavHostFragment;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * 周边药房
 */
public class PharmacyFragment extends BaseFragment {
    LinearLayout ll_finsh;
    private RecyclerView recyclerView;
    private PharmacyAdapter adapter;
    private TextView tv_content;
    private String code = "";
    private String title = "";
    private String apiUrl;
    private View view;
    private TextView tv_title;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_pharmacy, container, false);
            initView();
        }
        return view;
    }

    private void load() {

        Map<String, String> map = new HashMap<>();
        map.put("categoryCode", code);
        map.put("pageSize", "20");
        map.put("pageNo", "1");
        map.put("keyword", "");
        NetHandler.Companion.getInst().getClassify(apiUrl,map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ClassifyBean>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(ClassifyBean listResponse) {
                        if ("success".equals(listResponse.getMessage())) {
                            setData(listResponse);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void setData(ClassifyBean listResponse) {
        if (listResponse == null)
            return;
        if (listResponse.getTryTxt() != null) {
            setHideBottomBar(true);
            setTipText(listResponse.getTryTxt(), R.style.TextBlackFontAndBg);
        }
        adapter.setData(listResponse.getList());

    }


    private void initView() {
        ll_finsh = view.findViewById(R.id.ll_finsh);
        recyclerView = view.findViewById(R.id.recyclerView);
        tv_content = view.findViewById(R.id.tv_content);
        tv_title = view.findViewById(R.id.tv_title);
        adapter = new PharmacyAdapter(getActivity());
        code = getArguments().getString("code");
        title = getArguments().getString("title");
        apiUrl = getArguments().getString("apiUrl");
        tv_title.setText(title);
        load();

        adapter.setOnItemClickListener(new PharmacyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ClassifyBean.ListBeanX.ListBean bean) {
                    Bundle bundle = new Bundle();
                    bundle.putString("type", bean.getCode());
                    bundle.putString("title", bean.getName());
                    NavHostFragment.findNavController(PharmacyFragment.this).navigate(R.id.pharmacy_detail_fragment,bundle );
            }
        });
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        ll_finsh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(PharmacyFragment.this).navigateUp();

            }
        });
    }

}
