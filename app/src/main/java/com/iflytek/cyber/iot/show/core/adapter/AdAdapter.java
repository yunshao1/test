package com.iflytek.cyber.iot.show.core.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.response.AdBeanResonse;
import com.iflytek.cyber.iot.show.core.data.net.response.ServiceNewBean;
import com.iflytek.cyber.iot.show.core.utils.GlideUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 广告适配器
 */
public class AdAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<AdBeanResonse.ListBean> list = new ArrayList<>();
    private int type = 0;//0、1、2、3、4、5、6、7


    public AdAdapter(Context context) {
        this.context = context;
    }

    public int getType() {
        return type;
    }

    public AdAdapter setType(int type) {
        this.type = type;
        return this;
    }

    public void setData(List<AdBeanResonse.ListBean> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();

    }

    public List<AdBeanResonse.ListBean> getList() {
        return list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        RecyclerView.ViewHolder vHodler = null;
        View view = LayoutInflater.from(context).inflate(R.layout.item_ad, viewGroup, false);
        vHodler = new AdAdapter.VHodler(view);
        return vHodler;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vHodler, final int position) {
        ((VHodler) vHodler).tv_title.setText(list.get(position).getTitle());
        ((VHodler) vHodler).tv_subtitle.setText(list.get(position).getContent());
        GlideUtil.loadImage(context, list.get(position).getImg(), 0, ((VHodler) vHodler).image);
       if (position==0){
           ((VHodler) vHodler).ll_parent.setBackgroundResource(R.drawable.shape_corner_4_color_f4e7e1);
       }else {
           ((VHodler) vHodler).ll_parent.setBackgroundResource(R.drawable.shape_corner_4_color_e2edea);
       }

        ((VHodler) vHodler).ll_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener!=null){
                    onItemClickListener.onItemClick(position);
                }
            }
        });


    }

    @Override
    public int getItemViewType(int position) {
        return type;
    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    public class VHodler extends RecyclerView.ViewHolder {
        public LinearLayout ll_parent;
        public ImageView image;
        public TextView tv_title;
        public TextView tv_subtitle;

        public VHodler(@NonNull View itemView) {
            super(itemView);
            ll_parent = itemView.findViewById(R.id.ll_parent);
            image = itemView.findViewById(R.id.image);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_subtitle = itemView.findViewById(R.id.tv_subtitle);

        }
    }
    public interface OnItemClickListener {
        void onItemClick(int pos);
    }

    public OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
