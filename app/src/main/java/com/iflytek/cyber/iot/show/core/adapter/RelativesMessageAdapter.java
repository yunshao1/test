package com.iflytek.cyber.iot.show.core.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.response.GetMessageResponse;
import com.iflytek.cyber.iot.show.core.utils.GlideUtil;

import java.util.ArrayList;
import java.util.List;

public class RelativesMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<GetMessageResponse.ListBean> list;
    private Context context;


    public RelativesMessageAdapter(Context context) {
        this.context = context;
    }

    public List<GetMessageResponse.ListBean> getList() {
        if (list == null) {
            list = new ArrayList<>();
        }
        return list;
    }

    public void setData(List<GetMessageResponse.ListBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void addData(GetMessageResponse.ListBean bean) {
        this.list.add(bean);
        notifyDataSetChanged();
    }


    @Override
    public int getItemViewType(int position) {
        if (list != null && list.size() > 0) {
            if (list.get(position).isIsMine()) {
                position = 1;
            } else {
                position = 0;
            }

        } else {
            position = -1;
        }
        return position;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        RecyclerView.ViewHolder vHodler = null;
        switch (position) {
            case 0:
                vHodler = new VHodlerLeft(LayoutInflater.from(context).inflate(R.layout.item_message_left, viewGroup, false));
                break;
            case 1:
                vHodler = new VHodlerRight(LayoutInflater.from(context).inflate(R.layout.item_message_right, viewGroup, false));
                break;
        }
        return vHodler;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder vHodler, final int position) {
        if (vHodler instanceof VHodlerLeft) {
            if (TextUtils.isEmpty(list.get(position).getAudioUrl())) {   //文字
                ((VHodlerLeft) vHodler).content.setVisibility(View.VISIBLE);
                ((VHodlerLeft) vHodler).image_laba_left.setVisibility(View.GONE);
                ((VHodlerLeft) vHodler).imread_txt.setVisibility(View.VISIBLE);
                ((VHodlerLeft) vHodler).imread_txt.setOnClickListener(v -> {

                });
                ((VHodlerLeft) vHodler).tv_video_seconds.setText("");
                ((VHodlerLeft) vHodler).imread_txt.setOnClickListener(v -> {
                    if (onReadClickListener!=null){
                        onReadClickListener.onReadClick(list.get(position).getMsgContent());
                    }
                });
                ((VHodlerLeft) vHodler).content.setText(list.get(position).getMsgContent());
            } else {
                ((VHodlerLeft) vHodler).imread_txt.setVisibility(View.GONE);
                ((VHodlerLeft) vHodler).image_laba_left.setVisibility(View.VISIBLE);
                ((VHodlerLeft) vHodler).content.setVisibility(View.GONE);
                ((VHodlerLeft) vHodler).tv_video_seconds.setText(list.get(position).getAudioSecond() + " ''");
            }
            if (list.get(position).isIsAudio()) {//音频
                ((VHodlerLeft) vHodler).ll_bg.setOnClickListener(v -> {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(list.get(position).getAudioUrl());
                    }
                });
            }
            GlideUtil.load(context, list.get(position).getFromUserImg(), 0, ((VHodlerLeft) vHodler).image_head_left);
            ((VHodlerLeft) vHodler).image_laba_left.setImageResource(R.drawable.icon_laba_left);
        } else if (vHodler instanceof VHodlerRight) {
            GlideUtil.load(context, list.get(position).getFromUserImg(), 0, ((VHodlerRight) vHodler).image_head_left);
            ((VHodlerRight) vHodler).image_laba_left.setImageResource(R.drawable.icon_laba_right);
            if (!list.get(position).isIsAudio()) {//文字
                ((VHodlerRight) vHodler).content.setVisibility(View.VISIBLE);
                ((VHodlerRight) vHodler).image_laba_left.setVisibility(View.GONE);
                ((VHodlerRight) vHodler).content.setText(list.get(position).getMsgContent());
                ((VHodlerRight) vHodler).tv_video_right_seconds.setText("");
            } else {
                ((VHodlerRight) vHodler).image_laba_left.setVisibility(View.VISIBLE);
                ((VHodlerRight) vHodler).content.setVisibility(View.GONE);
                ((VHodlerRight) vHodler).tv_video_right_seconds.setText(list.get(position).getAudioSecond() + " ''");
            }
            if (list.get(position).isIsRead()) {
                ((VHodlerRight) vHodler).tv_isread.setText("已读");
            } else {
                ((VHodlerRight) vHodler).tv_isread.setText("未读");
            }
            if (list.get(position).isIsAudio()) {//音频
                ((VHodlerRight) vHodler).ll_bg.setOnClickListener(v -> {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(list.get(position).getAudioUrl());
                    }
                });
            }
        }

    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        else
            return list.size();

    }

    public class VHodlerLeft extends RecyclerView.ViewHolder {
        public ImageView image_laba_left, imread_txt;
        public ImageView image_head_left;
        public TextView content, tv_video_seconds;
        public LinearLayout ll_bg;

        public VHodlerLeft(@NonNull View itemView) {
            super(itemView);
            content = itemView.findViewById(R.id.content);
            image_laba_left = itemView.findViewById(R.id.image_laba_left);
            image_head_left = itemView.findViewById(R.id.image_head_left);
            imread_txt = itemView.findViewById(R.id.read_txt);
            tv_video_seconds = itemView.findViewById(R.id.tv_video_seconds);
            ll_bg = itemView.findViewById(R.id.ll_bg);


        }
    }


    public class VHodlerRight extends RecyclerView.ViewHolder {
        public ImageView image_laba_left;
        public ImageView image_head_left;
        public TextView content;
        public LinearLayout ll_bg;
        public TextView tv_video_right_seconds, tv_isread;


        public VHodlerRight(@NonNull View itemView) {
            super(itemView);
            content = itemView.findViewById(R.id.content);
            image_laba_left = itemView.findViewById(R.id.image_laba_left);
            image_head_left = itemView.findViewById(R.id.image_head_left);
            ll_bg = itemView.findViewById(R.id.ll_bg);
            tv_video_right_seconds = itemView.findViewById(R.id.tv_video_right_seconds);
            tv_isread = itemView.findViewById(R.id.tv_isread);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(String url);
    }

    public OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public interface OnReadClickListener {
        void onReadClick(String url);
    }

    public OnReadClickListener onReadClickListener;

    public void setOnReadClickListener(OnReadClickListener onReadClickListener) {
        this.onReadClickListener = onReadClickListener;
    }
}
