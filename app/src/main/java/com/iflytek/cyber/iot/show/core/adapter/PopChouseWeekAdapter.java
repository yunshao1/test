package com.iflytek.cyber.iot.show.core.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;

import java.util.HashMap;
import java.util.Map;

public class PopChouseWeekAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private String[] list = {"只限当日", "每周一", "每周二", "每周三", "每周四", "每周五", "每周六", "每周日"};
    private Map<Integer, Boolean> map = new HashMap<>();


    public PopChouseWeekAdapter(Context context) {
        this.context = context;
        for (int i = 0; i < list.length; i++) {
            map.put(i, false);
        }
    }

    public String[] getList() {
        return list;
    }

    public Map<Integer, Boolean> getMap() {
        return map;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        RecyclerView.ViewHolder vHodler = null;
        View view = LayoutInflater.from(context).inflate(R.layout.item_pop_chouse_week, viewGroup, false);
        vHodler = new VHodler(view);
        return vHodler;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vHodler, final int position) {
        ((VHodler) vHodler).tv_title.setText(list[position]);
        if (map.get(position)) {
            ((VHodler) vHodler).im_chouse.setVisibility(View.VISIBLE);
            ((VHodler) vHodler).tv_title.setTextColor(context.getResources().getColor(R.color.color_2cc898b));
        } else {
            ((VHodler) vHodler).im_chouse.setVisibility(View.INVISIBLE);
            ((VHodler) vHodler).tv_title.setTextColor(context.getResources().getColor(R.color.color_666));
        }
        ((VHodler) vHodler).ll_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(position);
                }

            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.length;
    }

    public class VHodler extends RecyclerView.ViewHolder {
        public ImageView im_chouse;
        public TextView tv_title;
        public LinearLayout ll_parent;

        public VHodler(@NonNull View itemView) {
            super(itemView);
            im_chouse = itemView.findViewById(R.id.im_chouse);
            tv_title = itemView.findViewById(R.id.tv_title);
            ll_parent = itemView.findViewById(R.id.ll_parent);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }

    public OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
