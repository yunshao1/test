package com.iflytek.cyber.iot.show.core.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iflytek.cyber.iot.show.core.R;
import com.iflytek.cyber.iot.show.core.data.net.response.ReminderResponse;
import com.iflytek.cyber.iot.show.core.utils.Constant;
import com.iflytek.cyber.iot.show.core.utils.DateUtils;

import java.util.Date;
import java.util.List;

public class MyReminderAdapter extends RecyclerView.Adapter<MyReminderAdapter.VHodler> {

    private Context context;
    private List<ReminderResponse.ListBean> list;
    private int type = 0;//1删除

    public List<ReminderResponse.ListBean> getList() {
        return list;
    }

    public MyReminderAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<ReminderResponse.ListBean> list) {
        this.list = list;
        deleteNotOpenData();
        Constant.rmindList = list;
        notifyDataSetChanged();
    }
    private void deleteNotOpenData(){
        if (list==null)
            return;
        for (int i=list.size()-1;i>=0;i--){
            if (!list.get(i).isSend()){//false已经关闭
                list.remove(i);
            }
        }
    }

    public void setType(int type) {
        this.type = type;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public MyReminderAdapter.VHodler onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        MyReminderAdapter.VHodler vHodler = null;
        vHodler = new MyReminderAdapter.VHodler(LayoutInflater.from(context).inflate(R.layout.item_my_reminder, viewGroup, false));
        vHodler.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        return vHodler;
    }

    @Override
    public void onBindViewHolder(@NonNull MyReminderAdapter.VHodler vHodler, final int position) {
        String time = "";
        String wewkDay = DateUtils.getWeekOfIntDate(new Date(), list.get(position).getWeek());
        int hour = list.get(position).getHour();
        int min = list.get(position).getMinute();
        if (TextUtils.isEmpty(wewkDay)){
            wewkDay="只限当日";
        }
        if (hour < 10) {
            time = wewkDay + " 0" + hour + ":";
        } else {
            time = wewkDay + " " + hour + ":";
        }
        if (min < 10) {
            time = time + " 0" + min;
        } else {
            time = time + " " + min;
        }
        vHodler.tv_time.setText(time);
        String voiceUrl = list.get(position).getAudioUrl();
        if (TextUtils.isEmpty(voiceUrl)) {
            vHodler.image.setVisibility(View.GONE);
            vHodler.tv_content.setVisibility(View.VISIBLE);
        } else {
            vHodler.image.setVisibility(View.VISIBLE);
            vHodler.tv_content.setVisibility(View.GONE);
        }
        vHodler.image.setOnClickListener(v -> {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(voiceUrl, position);
            }
        });
        vHodler.image_edit.setOnClickListener(v -> {
            if (onItemDeleteClickListener != null) {
                onItemDeleteClickListener.onItemDeleteClick(position);
            }
        });
        vHodler.tv_content.setText(list.get(position).getText());
        if (type == 0) {
            vHodler.image_edit.setVisibility(View.GONE);
        } else {
            vHodler.image_edit.setVisibility(View.VISIBLE);
        }
        if (list.get(position).IsPlaying()) {
            vHodler.image.setImageResource(R.drawable.ic_my_reminder_pouse);
        } else {
            vHodler.image.setImageResource(R.drawable.ic_my_reminder_paly);

        }
    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    public class VHodler extends RecyclerView.ViewHolder {
        public TextView tv_time;
        public ImageView image;
        public TextView tv_content;
        public ImageView image_edit;

        public VHodler(@NonNull View itemView) {
            super(itemView);
            tv_time = itemView.findViewById(R.id.tv_time);
            image = itemView.findViewById(R.id.image);
            tv_content = itemView.findViewById(R.id.tv_content);
            image_edit = itemView.findViewById(R.id.image_edit);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(String url, int pos);
    }

    public OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public interface OnItemDeleteClickListener {
        void onItemDeleteClick(int pos);
    }

    public OnItemDeleteClickListener onItemDeleteClickListener;

    public void setOnItemDeleteClickListener(OnItemDeleteClickListener onItemDeleteClickListener) {
        this.onItemDeleteClickListener = onItemDeleteClickListener;
    }

}
