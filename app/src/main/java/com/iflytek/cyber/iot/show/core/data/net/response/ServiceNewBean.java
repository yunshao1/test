package com.iflytek.cyber.iot.show.core.data.net.response;

import java.util.List;

public class ServiceNewBean {

    private int success;
    private String code;
    private String message;
    private String dataString;
    private DataBean data;
    private int state;
    private String signature;
    private String signatureAlgorithm;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDataString() {
        return dataString;
    }

    public void setDataString(String dataString) {
        this.dataString = dataString;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    public void setSignatureAlgorithm(String signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }

    public static class DataBean {

        private int id;
        private int appId;
        private String code;
        private String name;
        private String remark;
        private boolean visible;
        private List<PanelBean> panel;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getAppId() {
            return appId;
        }

        public void setAppId(int appId) {
            this.appId = appId;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public boolean isVisible() {
            return visible;
        }

        public void setVisible(boolean visible) {
            this.visible = visible;
        }

        public List<PanelBean> getPanel() {
            return panel;
        }

        public void setPanel(List<PanelBean> panel) {
            this.panel = panel;
        }

        public static class PanelBean {

            private String code;
            private int type;
            private String apiUrl;
            private int sort;
            private boolean visible;
            private List<PlugBean> plug;

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public String getApiUrl() {
                return apiUrl;
            }

            public void setApiUrl(String apiUrl) {
                this.apiUrl = apiUrl;
            }

            public int getSort() {
                return sort;
            }

            public void setSort(int sort) {
                this.sort = sort;
            }

            public boolean isVisible() {
                return visible;
            }

            public void setVisible(boolean visible) {
                this.visible = visible;
            }

            public List<PlugBean> getPlug() {
                return plug;
            }

            public void setPlug(List<PlugBean> plug) {
                this.plug = plug;
            }

            public static class PlugBean {
                /**
                 * code : T-001
                 * openType : 0
                 * name : 促销专场
                 * img : http://resource.shfusion.com/image/service/slices/促销专场.png
                 * enable : false
                 * visible : true
                 */

                private String code;
                private String openType;
                private String name;
                private String img;
                private boolean enable;
                private boolean visible;

                public String getCode() {
                    return code;
                }

                public void setCode(String code) {
                    this.code = code;
                }

                public String getOpenType() {
                    return openType;
                }

                public void setOpenType(String openType) {
                    this.openType = openType;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getImg() {
                    return img;
                }

                public void setImg(String img) {
                    this.img = img;
                }

                public boolean isEnable() {
                    return enable;
                }

                public void setEnable(boolean enable) {
                    this.enable = enable;
                }

                public boolean isVisible() {
                    return visible;
                }

                public void setVisible(boolean visible) {
                    this.visible = visible;
                }
            }
        }
    }
}
