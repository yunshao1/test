package com.cloud.box.chat;

import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;

import io.reactivex.Single;

/**
 * @author cloud.wang
 * @date 2019-07-13
 * @Description: TODO
 */
public class Chat {

    public static void main(String[] args) throws Exception {
//        System.out.println("Enter the URL of the SignalR Chat you want to join");
//        Scanner reader = new Scanner(System.in);  // Reading from System.in
//        String input;
//        input = reader.nextLine();

        String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBY2NvdW50Ijoie1wiSWRcIjpcIjYxOTBcIixcIk5hbWVcIjpcImx5alwiLFwiQWNjb3VudFR5cGVcIjowLFwiUmVsYXRlXCI6XCI2MTlcIixcIkFwcENvZGVcIjowLFwiUmVhbE5hbWVcIjpcIuWImOS7peeOllwifSIsImV4cCI6MTU2MzA4MjA4OSwiaXNzIjoibWUiLCJhdWQiOiJ5b3UifQ.v2OtnNsJaH-QIEjJYvu5k55gJo7fmAZCsSE6fsDNRnQ";
//        String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBY2NvdW50Ijoie1wiSWRcIjpcIkExRTE5QzZFLUQ1MTgtNDlBMS1BNzMzLTc1MzIyODA3RDJCMVwiLFwiTmFtZVwiOlwiVXNlclwiLFwiQWNjb3VudFR5cGVcIjowLFwiUmVsYXRlXCI6XCIxXCIsXCJSZWFsTmFtZVwiOlwi5rWL6K-V6LSm5Y-3XCIsXCJBcHBDb2RlXCI6MH0iLCJleHAiOjE1NjMxMDAzNDgsImlzcyI6Im1lIiwiYXVkIjoieW91In0.GatcQ_r8IXDGxxm39NRal9pn218MRTEYxjABoLRKIG8";
//        String url = "http://messagehub.api.shfusion.com/messageHub?access_token="+token;
        String url = "http://messagehub.api.shfusion.com/messageHub";
//        String url = "http://192.168.1.107:5000/messageHub";

        HubConnection hubConnection = HubConnectionBuilder.create(url)
                .shouldSkipNegotiate(true)
                .withHeader("authorization", "Bearer "+token)
                .withAccessTokenProvider(Single.defer(() ->
                    Single.just(token)
                )).build();

        hubConnection.on("Send", (message) -> {
            System.out.println("New Message: " + message);
        }, String.class);

        //This is a blocking call
        hubConnection.start().blockingAwait();

//        while (!input.equals("leave")){
//            input = reader.nextLine();
//            hubConnection.send("Send", input);
//        }

        System.out.println("---------------");

        hubConnection.stop();
    }
}
