package com.cloud.box.chat;

import java.util.List;

/**
 * @author cloud.wang
 * @date 2019-07-20
 * @Description: TODO
 */
public class Test {


    private List<List<EeBean>> ee;

    public List<List<EeBean>> getEe() {
        return ee;
    }

    public void setEe(List<List<EeBean>> ee) {
        this.ee = ee;
    }

    public static class EeBean {
        /**
         * test : hh
         */

        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }
    }
}
