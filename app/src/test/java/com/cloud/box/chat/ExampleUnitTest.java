package com.cloud.box.chat;

//import org.junit.Test;
//
//import static org.junit.Assert.*;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.iflytek.cyber.iot.show.core.chat.ChatMessage;
import com.iflytek.cyber.iot.show.core.chat.ChatResponse;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

//    @Test
//    public void addition_isCorrect() {
//        assertEquals(4, 2 + 2);
//    }

    public static void main(String[] args) throws Exception {

//        HubMessageType messageType = null;
        String str = "{\"type\":1,\"target\":\"OnGroupMessages\",\"arguments\":[[{\"id\":\"aa90277c39e7-9307-5e4d-41a9-fd363fca\"},{\"id\":\"aa90277c39e7-9307-5e4d-41a9-fd363fca\"}]]}";
        String invocationId = null;
        String target = null;
        String error = null;
        ArrayList<Object> arguments = null;
        JsonArray argumentsToken = null;
        Object result = null;
        Exception argumentBindingException = null;
        JsonElement resultToken = null;
        JsonReader reader = new JsonReader(new StringReader(str));
        reader.beginObject();

        do {
            String name = reader.nextName();
            switch (name) {
                case "type":
                    System.out.println("type=="+reader.nextInt());
                    break;
                case "invocationId":
                    invocationId = reader.nextString();
                    System.out.println("invocationId=="+invocationId);
                    break;
                case "target":
                    target = reader.nextString();
                    System.out.println("target=="+target);
                    break;
                case "error":
                    error = reader.nextString();
                    break;
                case "result":
//                    if (invocationId == null) {
//                        resultToken = jsonParser.parse(reader);
//                    } else {
//                        result = gson.fromJson(reader, binder.getReturnType(invocationId));
//                    }
                    break;
                case "item":
                    reader.skipValue();
                    break;
                case "arguments":
                    System.out.println("arguments==");
                    if (target != null) {
                        boolean startedArray = false;
                        try {
//                            List<Class<?>> types = binder.getParameterTypes(target);
                            startedArray = true;
                            arguments = bindArguments(reader);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            argumentBindingException = ex;

                            // Could be at any point in argument array JSON when an error is thrown
                            // Read until the end of the argument JSON array
                            if (!startedArray) {
                                reader.beginArray();
                            }
                            while (reader.hasNext()) {
                                reader.skipValue();
                            }
                            if (reader.peek() == JsonToken.END_ARRAY) {
                                reader.endArray();
                            }
                        }

                        if (arguments != null) {
                            for (Object obj: arguments) {
                                System.out.println("arguments=="+ obj.toString());
                            }
                        }
                    } else {
                        System.out.println("arguments no  ==");
//                        argumentsToken = (JsonArray)jsonParser.parse(reader);
                    }
                    break;
                case "headers":
                    throw new RuntimeException("Headers not implemented yet.");
                default:
                    // Skip unknown property, allows new clients to still work with old protocols
                    reader.skipValue();
                    break;
            }
        } while (reader.hasNext());

        reader.endObject();
        reader.close();

    }

    private static ArrayList<Object> bindArguments(JsonReader reader) throws IOException {
        Gson gson = new Gson();
        reader.beginArray();
//        int paramCount = paramTypes.size();
        int argCount = 0;
        ArrayList<Object> arguments = new ArrayList<>();
        while (reader.peek() != JsonToken.END_ARRAY) {
//            if (argCount < paramCount) {
            Type founderListType = new TypeToken<ArrayList<ChatMessage>>(){}.getType();
            arguments.add(gson.fromJson(reader, founderListType));
//            arguments.add(gson.fromJson(reader, ChatMessage[].class));
//            } else {
//                reader.skipValue();
//            }
            argCount++;
        }

//        if (paramCount != argCount) {
//            throw new RuntimeException(String.format("Invocation provides %d argument(s) but target expects %d.", argCount, paramCount));
//        }

        // Do this at the very end, because if we throw for any reason above, we catch at the call site
        // And manually consume the rest of the array, if we called endArray before throwing the RuntimeException
        // Then we can't correctly consume the rest of the json object
        reader.endArray();

        return arguments;
    }

}