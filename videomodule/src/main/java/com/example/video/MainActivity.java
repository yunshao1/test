package com.example.video;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

import com.tencent.trtc.TRTCCloudDef;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private String mCurrentUser;
    private static final  int mRoomId =123;
    private String mCurrentSig;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkPermission();
        setContentView(R.layout.activity_main);
        findViewById(R.id.enter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentSig=mSig;
                mCurrentUser=mUserid;
                onJoinRoom(mRoomId);
            }
        });
        findViewById(R.id.enter2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentSig=mSig2;
                mCurrentUser=mUserid2;
                onJoinRoom(mRoomId);
            }
        }); findViewById(R.id.enter3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentSig=mSig3;
                mCurrentUser=mUserid3;
                onJoinRoom(mRoomId);
            }
        });

        Toast.makeText(this,(isPad(this)?"是平板":"不是平板"),Toast.LENGTH_SHORT).show();
    }
    public  boolean isPad(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }
    private void onJoinRoom(final int roomId) {
        final Intent intent = new Intent(this, TRTCMainActivity.class);
        intent.putExtra("roomId", roomId);
        intent.putExtra("userId", mCurrentUser);
        intent.putExtra("AppScene", TRTCCloudDef.TRTC_APP_SCENE_VIDEOCALL);
        intent.putExtra("customVideoCapture", false);
        intent.putExtra("videoFile", false);
        //（1） 从控制台获取的 json 文件中，简单获取几组已经提前计算好的 userid 和 usersig
        intent.putExtra("sdkAppId", 1400230639);
        intent.putExtra("userSig", mCurrentSig);
        startActivity(intent);
    }

    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> permissions = new ArrayList<>();
            if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA)) {
                permissions.add(Manifest.permission.CAMERA);
            }
            if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECORD_AUDIO)) {
                permissions.add(Manifest.permission.RECORD_AUDIO);
            }
            if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (permissions.size() != 0) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        (String[]) permissions.toArray(new String[0]),
                        REQ_PERMISSION_CODE);
                return false;
            }
        }

        return true;
    }
    private final static int REQ_PERMISSION_CODE = 0x1000;
    private final static String mSig = "eJxlj8FOg0AQQO98BeGqscvCruCNLm1EwbSWWullg7DUsRFwWVrE*O8qNpHEub6XmTcfmq7rRhyuLtIsq9pScfVeC0O-0g1knP-Buoacp4pbMv8HRVeDFDwtlJADNAkhGKGxA7koFRRwMjbiiSupMo7wSGryPR8u-W6xEcIWopY7VmA3wGiWsGDJFl0DHY0d70YlfjUJpQ-LhQyYeGCmpwRd99P2ejZ-tKNj8OxF8f3UKuLscHbnmvtwhSeJvXWdfovaFz*87Tdx8ub2bL2bj6sUvIrTWxTTS4eQcdBByAaqchAwMon5nfwzhvapfQFCEl41";
    private final static String mSig2 = "eJxlj11PgzAYhe-5FU1vZ1wpK2Mmu2C46AzELU4BbxqkxTWEj5XyafzvKi6RxPf2eXLOeT80AAA8uk-XURwXda6o6ksOwQ2ACF79wbIUjEaKGpL9g7wrheQ0ShSXI9QJIRihqSMYz5VIxMXw*RtVUsUU6ROpYikdm35TFghhA5nGaqqI9xF624Ozu2vrl4eYbImfZiQzh*Uta-fnXTCsjK5O**ekPVuBfZ-aXmSLzcKpdTnMrCDF7qvPTvNj6IbFoxX2jaxOs7m5SQzudKrxDuv1pFKJjF-eMrG5tAiZDmq4rESRjwJGOtG-J-8c1D61L9JIX34_";
    private final static String mSig3 = "eJxlj11PgzAYhe-5FQ23GFNgLZuJF3PDDHQOw8aiNw0fhRUZq6Uwxey-T3GJTXxvnyfnnPdLAwDo68fwOk7TQ1tLIj851cEN0KF*9Qc5ZxmJJbFF9g-SD84EJXEuqRigiRCyIFQdltFaspxdjC1NiBQyJdBWpCZ7I0PTb8oIQsuG2J6oCisGuHQ3M*95tg6mht*3O4w282Q7XvZw8sBfYeUviqNTtyUqOlku7voRnXpF2DYdCmicRMG8dI08WB3DxKmiyLh-f1rtfIa597JP86pyb5VKyfb08ha2sDNGSB3UUdGwQz0IFjSR*T3553TtpJ0Bth1fiw__";
    private final static String mUserid="Web_trtc_02";
    private final static String mUserid2="Web_trtc_01";
    private final static String mUserid3="Web_trtc_03";
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQ_PERMISSION_CODE:
                for (int ret : grantResults) {
                    if (PackageManager.PERMISSION_GRANTED != ret) {
                        Toast.makeText(MainActivity.this, "用户没有允许需要的权限，使用可能会受到限制！", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            default:
                break;
        }
    }
}
