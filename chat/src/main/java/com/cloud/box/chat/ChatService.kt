package com.cloud.box.chat

import android.app.IntentService
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.microsoft.signalr.HubConnectionBuilder
import com.microsoft.signalr.HubConnection
import io.reactivex.Observer
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * @author cloud.wang
 * @date 2019-07-13
 * @Description: TODO
 */
class ChatService: IntentService("ChatService") {

    private val TAG = "ChatService"
    private var mConnection: HubConnection? = null

    override fun onCreate() {
        super.onCreate()

        //可用本地广播的形式，和其他组建通信
//        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
    }

    override fun onBind(intent: Intent): IBinder? {
        return binder
    }

    private val binder = EngineBinder()

    open inner class EngineBinder : Binder() {
        fun getService(): ChatService {
            return this@ChatService
        }
    }

    override fun onHandleIntent(intent: Intent?) {
        Log.i(TAG, "onHandleIntent: 启动Service")
        val url = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBY2NvdW50Ijoie1wiSWRcIjpcIjYxOTBcIixcIk5hbWVcIjpcImx5alwiLFwiQWNjb3VudFR5cGVcIjowLFwiUmVsYXRlXCI6XCI2MTlcIixcIkFwcENvZGVcIjowLFwiUmVhbE5hbWVcIjpcIuWImOS7peeOllwifSIsImV4cCI6MTU2MzA4MjA4OSwiaXNzIjoibWUiLCJhdWQiOiJ5b3UifQ.v2OtnNsJaH-QIEjJYvu5k55gJo7fmAZCsSE6fsDNRnQ";
        if (mConnection == null) {
            Log.i(TAG, "onHandleIntent: 初始化Connection")
            mConnection = HubConnectionBuilder.create("http://messagehub.api.shfusion.com/messageHub?access_token=$url")
                    .withAccessTokenProvider(Single.defer{
                        Single.just("")
                    }).build()
        }

        //GetGroupMessagesAsync(String group,string AppKey,int groupType)
//        mConnection?.on("GetGroupMessagesAsync",
//                { group, appKey, groupType -> println("New Message: $group") },
//                String::class.java, String::class.java, Int::class.java)


//        mConnection?.on("Send", { message -> println("New Message: $message") }, String::class.java)

        mConnection?.on("setOnGroupMessagesListener", { message -> parseResponse(message) }, String::class.java)

        mConnection?.start()?.blockingAwait()

        while (true) {

        }

        Log.i(TAG, "onHandleIntent: 是否执行到这里-----")

    }

    /**
     * 创建一个会话
     * @param group
     * @param appKey  写死：Elderly.SOS
     * @param groupType 写死：2
     */
    public fun createSession(group: String, appKey: String, groupType: Int): Boolean {
        when (mConnection != null) {
            true -> {
                mConnection?.send("GetGroupMessagesAsync", group, appKey, groupType)
                return true
            }

            false -> return false
        }
    }

    private fun parseResponse(message: Any) {
        Log.i(TAG, "New Message: $message")
    }
}